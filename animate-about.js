function staticAnimation() {
    /*
    gsap.set(".paris", {
        scale: 1.5,
        translateY: 0,
        opacity: 0,
        overwrite: "auto",
    });
    */
    gsap.set("#item-text-0", {
        translateX: -100,
        translateY: 0,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#item-text-1", {
        translateX: 100,
        translateY: 0,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#text-paris", {
        translateX: 0,
        translateY: -100,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#text-cannes", {
        translateX: 0,
        translateY: -100,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#text-madrid", {
        translateX: 0,
        translateY: -100,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#text-dubai", {
        translateX: 0,
        translateY: -100,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#text-tana", {
        translateX: 0,
        translateY: -100,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.to(
        ".paris",
        {
            scale: 1,
            translateY: 0,
            opacity: 1,
            duration: 1,
            ease: "power2.inOut",
            overwrite: "auto",
        },
        "+=0.1"
    );

    gsap.to(
        "#text-paris",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.2,
            ease: "power3.out",
            overwrite: "auto",
        },
        ">"
    );
    gsap.to(
        "#text-cannes",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.3,
            ease: "power3.out",
            overwrite: "auto",
        },
        "-=0.1"
    );
    gsap.to(
        "#text-madrid",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.3,
            ease: "power3.out",
            overwrite: "auto",
        },
        "-=0.1"
    );
    gsap.to(
        "#text-dubai",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.4,
            ease: "power3.out",
            overwrite: "auto",
        },
        "-=0.1"
    );
    gsap.to(
        "#text-tana",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.4,
            ease: "power3.out",
            overwrite: "auto",
        },
        "-=0.1"
    );
    gsap.to(
        "#item-text-0",
        {
            translateX: -0,
            translateY: 0,
            opacity: 1,
            duration: 0.4,
            ease: "power3.out",
            overwrite: "auto",
        },
        "<="
    );
    gsap.to(
        "#item-text-1",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.5,
            ease: "power3.out",
            overwrite: "auto",
        },
        "<="
    );
}

function isInViewport(element, arg) {
    const rect = element.getBoundingClientRect();

    return (
        rect.top + arg <=
        (window.innerHeight || document.documentElement.clientHeight)
    );
}
function isInViewportB(element, arg) {
    const rect = element.getBoundingClientRect();
    return (
        rect.bottom + arg <=
        (window.innerHeight || document.documentElement.clientHeight)
    );
}
function init(els) {
    els.forEach((el) => {
        gsap.set(el.class, {
            onStart: () => {
                el.on = true;
            },
            onComplete: () => {
                el.on = false;
            },

            translateY: 0,
            translateX: el.id % 2 == 0 ? -200 : 200,
            opacity: 0,
            overwrite: true,
        });
    });
}

function initImg(els) {
    els.forEach((el) => {
        gsap.set(el.class, {
            onStart: () => {
                el.on = true;
            },
            onComplete: () => {
                el.on = false;
            },
            translateY: 0,
            translateX: -500,
            scale: 1,
            opacity: 0,
            overwrite: true,
        });
    });
}
function animate(els) {
    els.forEach((el) => {
        if (!el.on) {
            if (isInViewport(el.el, -50)) {
                gsap.to(el.class, {
                    start: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = true;
                    },

                    translateY: 0,
                    translateX: 0,
                    opacity: 1,
                    duration: 1,
                    ease: "power4.out",
                    overwrite: true,
                });
            } else {
                gsap.to(el.class, {
                    onStart: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = false;
                    },
                    translateY: 0,
                    translateX: el.id % 2 == 0 ? -200 : 200,

                    opacity: 0,
                    duration: 0.5,
                    ease: "power4.inOut",
                    overwrite: true,
                });
            }
        }
    });
}

function animateImg(els) {
    els.forEach((el) => {
        if (!el.on) {
            if (isInViewport(el.el, 100)) {
                gsap.to(el.class, {
                    start: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = true;
                    },

                    translateY: 0,
                    translateX: 0,
                    scale: 1,
                    opacity: 1,
                    duration: 1,
                    ease: "power4.inOut",
                    overwrite: true,
                });
            } else {
                gsap.to(el.class, {
                    onStart: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = false;
                    },
                    translateY: 0,
                    translateX: -500,
                    scale: 1,
                    opacity: 0,
                    duration: 0.5,
                    ease: "elastic",
                    overwrite: true,
                });
            }
        }
    });
}

function main() {
    staticAnimation();

    //trigger();

    const querrySelect = [2, 3, 4];

    const querrySelectImg = [];
    let els = [];

    let imgs = [];

    for (const index of querrySelect) {
        els.push({
            el: document.querySelector(`#item-text-${index}`),
            class: `#item-text-${index}`,
            on: false,
            id: index,
        });
    }

    for (const index of querrySelectImg) {
        imgs.push({
            el: document.querySelector(`#plexel-${index}`),
            class: `#plexel-${index}`,
            on: false,
            id: index,
        });
    }
    init(els);
    initImg(imgs);

    window.onscroll = function () {
        animate(els);
        animateImg(imgs);
    };
}
main();
