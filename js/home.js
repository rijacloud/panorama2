(function () {
    document.querySelectorAll(".nav-tabs .nav-link").forEach(function (item) {
        item.addEventListener("click", function (e) {
            e.preventDefault();
            if (this.classList.contains("active") === false) {
                if (document.querySelector(".nav-tabs .nav-link.active")) {
                    document
                        .querySelector(".nav-tabs .nav-link.active")
                        .classList.remove("active");
                }
                this.classList.add("active");
                var target = this.getAttribute("target-tab");
                if (document.querySelector(".tab-content .active")) {
                    document
                        .querySelector(".tab-content .active")
                        .classList.remove("active");
                }
                if (document.querySelector(".tab-content .show")) {
                    document
                        .querySelector(".tab-content .show")
                        .classList.remove("show");
                }
                // var newUrl = updateQueryStringParameter(
                //     window.location.href,
                //     "tab",
                //     target
                // );
                // var title = this.getAttribute("page");
                // window.history.pushState("", title);
                document.getElementById(target).classList.add("active");
                document.getElementById(target).classList.add("show");
                document.getElementById("home-").classList.add("hidden");
                document.getElementById("contac").classList.add("hidden");
                if (window.innerWidth > 1120) {
                    document.documentElement.scrollTop = 1000;
                } else if (window.innerWidth <= 800) {
                    document.documentElement.scrollTop = 1350;
                } else {
                    document.documentElement.scrollTop = 600;
                }
            } else {
                document.getElementById("home-").classList.remove("hidden");
                document.getElementById("contac").classList.remove("hidden");
            }
        });
    });

    document.getElementById("tocontac").addEventListener("click", function (e) {
        document.querySelector(".tab-pane.active").classList.remove("active");

        document.getElementById("home-").classList.remove("hidden");
        document.getElementById("contac").classList.remove("hidden");
    });

    document.getElementById("sobree").addEventListener("click", function (e) {
        document.querySelector(".tab-pane.active").classList.remove("active");

        document.getElementById("home-").classList.remove("hidden");
        document.getElementById("contac").classList.remove("hidden");
    });

    // function updateQueryStringParameter(target) {
    //     var refresh =
    //         window.location.protocol +
    //         "//" +
    //         window.location.host +
    //         window.location.pathname +
    //         "?tab=" +
    //         target;
    //     window.history.replaceState({ path: refresh }, "", refresh);
    // }

    var index = 0;
    setInterval(function () {
        var span = document.querySelectorAll(".text-slider");
        if (index++ >= span.length) index = 1;
        else index = index++;
        document
            .querySelector(".text-slider.active")
            .classList.remove("active");
        span[index - 1].classList.add("active");
    }, 2300);
})();
