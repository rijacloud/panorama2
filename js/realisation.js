(function () {
    let up = document.querySelector(".up");
    let down = document.querySelector(".down");
    let current_it = 2;
    let num = 0;
    document
        .querySelectorAll(".expert-list .list button")
        .forEach(function (item) {
            item.addEventListener("click", function () {
                let Class = this.getAttribute("data-tag");
                var active = document.querySelector(
                    ".expert-list .list button.active"
                );
                if (active) {
                    active.classList.remove("active");
                }
                item.classList.add("active");
                document
                    .querySelectorAll(".expert-list .item")
                    .forEach(function (item) {
                        item.classList.remove("visible");
                        item.classList.remove("hidden");
                        let tags = item.getAttribute("data-tag").split(" ");
                        if (tags.includes(Class)) {
                            item.classList.add("visible");
                            num++;
                        } else {
                            item.classList.add("hidden");
                        }
                    });
                if (num == 1) {
                    document.querySelector(
                        ".expert-list .item.visible"
                    ).style.width = "calc(100% / 1)";

                    document.querySelector(
                        ".expert-list .item.visible"
                    ).style.maxHeight = "600px";
                    num = 0;
                } else {
                    document
                        .querySelectorAll(".expert-list .item.visible")
                        .forEach(function (item) {
                            item.style.width = "calc(100% / 2)";
                            item.style.maxHeight = "600px";
                        });
                    num = 0;
                }
            });
        });

    up.addEventListener("click", function () {
        current_it = current_it - 1;

        document.querySelectorAll("#plists .item").forEach(function (item) {
            item.style.width = "calc(100% /" + current_it + ")";
            if (current_it > 2) {
                item.style.maxHeight = "600px";
            }
        });
    });
    down.addEventListener("click", function () {
        current_it = current_it + 1;

        document.querySelectorAll("#plists .item").forEach(function (item) {
            item.style.width = "calc(100% /" + current_it + ")";
            if (current_it <= 2) {
                item.style.maxHeight = "600px";
            }
        });
    });

    // Instance using native lazy loading
    const lazyContent = new LazyLoad({
        use_native: true, // <-- there you go
    });
})();
