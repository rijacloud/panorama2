function staticAnimation() {
    gsap.set("#text-0, #item-text-0, #item-text-1", {
        translateX: -100,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#text-stories", {
        translateX: 0,
        translateY: -100,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#text-brought", {
        translateX: 0,
        translateY: -100,
        opacity: 0,
        overwrite: "auto",
    });
    gsap.set("#text-life", {
        translateX: 0,
        translateY: -100,
        opacity: 0,
        overwrite: "auto",
    });

    gsap.to(
        "#text-stories",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.5,
            ease: "power3.out",
            overwrite: "auto",
        },
        "+=0.3"
    );
    gsap.to(
        "#text-brought",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.7,
            ease: "power3.out",
            overwrite: "auto",
        },
        "<"
    );
    gsap.to(
        "#text-life",
        {
            translateX: 0,
            translateY: 0,
            opacity: 1,
            duration: 0.9,
            ease: "power3.out",
            overwrite: "auto",
        },
        "<"
    );

    gsap.to(
        "#text-0",
        {
            translateX: 0,
            opacity: 1,
            duration: 1.0,
            ease: "power2.inOut",
            overwrite: "auto",
        },
        "<"
    );
    gsap.to(
        "#item-text-0",
        {
            translateX: 0,
            opacity: 1,
            duration: 1.3,
            ease: "power2.inOut",
            overwrite: "auto",
        },
        "<"
    );
    gsap.to(
        "#item-text-1",
        {
            translateX: 0,
            opacity: 1,
            duration: 1.5,
            ease: "power2.inOut",
            overwrite: "auto",
        },
        "<"
    );
}

function isInViewport(element, arg) {
    const rect = element.getBoundingClientRect();

    return (
        rect.top + arg <=
        (window.innerHeight || document.documentElement.clientHeight)
    );
}
function isInViewportB(element, arg) {
    const rect = element.getBoundingClientRect();
    return (
        rect.bottom + arg <=
        (window.innerHeight || document.documentElement.clientHeight)
    );
}
function init(els) {
    els.forEach((el) => {
        gsap.set(el.class, {
            onStart: () => {
                el.on = true;
            },
            onComplete: () => {
                el.on = false;
            },

            translateY: -150,
            translateX: 0,
            opacity: 0,
            overwrite: true,
        });
        gsap.set(el.class + "b", {
            onStart: () => {
                el.on = true;
            },
            onComplete: () => {
                el.on = false;
            },

            translateY: -100,
            translateX: 0,
            opacity: 0,
            overwrite: true,
        });
    });
}

function initImg(els) {
    els.forEach((el) => {
        gsap.set(el.class, {
            onStart: () => {
                el.on = true;
            },
            onComplete: () => {
                el.on = false;
            },
            translateY: 200,
            translateX: 0,
            scale: 1,
            opacity: 0,
            overwrite: true,
        });
    });
}
function initCanvas(arg, items) {
    if (arg == 0) {
        items.forEach((e) => {
            gsap.set(e, {
                filter: "brightness(1) grayscale(0)",
                translateY: 0,
                scale: 1,
                zIndex: 0,
            });
        });
    } else {
        let duration = 0.2;
        items.forEach((e) => {
            gsap.to(e, {
                translateY: 0,
                scale: 1,
                duration: 0.5,
                ease: "power3.out",
                overwrite: "auto",
            });
            gsap.to(
                e,
                {
                    filter: "brightness(1) grayscale(0)",
                    duration: 0.3,
                    ease: "power3.in",
                    overwrite: "auto",
                },
                "<"
            );
            gsap.set(arg, {
                zIndex: 0,
                overwrite: "auto",
            });
            duration += 0.1;
        });
    }
}
function animateCanvas(arg, items) {
    gsap.set(arg, {
        zIndex: 1,
        overwrite: "auto",
    });
    gsap.to(arg, {
        translateY: -6,
        scale: 1.02,
        duration: 0.5,
        ease: "power3.out",
        overwrite: "auto",
    });

    let duration = 0.5;
    items.forEach((e) => {
        if (arg != e) {
            gsap.to(e, {
                filter: "brightness(0.6) grayscale(0.6)",
                duration: duration,
                ease: "power3.out",
                overwrite: false,
            });
            duration += 0.2;
        }
    });
}
function animate(els) {
    els.forEach((el) => {
        if (!el.on) {
            if (isInViewport(el.el, -100)) {
                gsap.to(el.class, {
                    start: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = true;
                    },

                    translateY: 0,
                    translateX: 0,
                    opacity: 1,
                    duration: 1.5,
                    ease: "power4.inOut",
                    overwrite: true,
                });
                gsap.to(el.class + "b", {
                    start: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = true;
                    },

                    translateY: 0,
                    translateX: 0,
                    opacity: 1,
                    duration: 1,
                    ease: "power4.inOut",
                    overwrite: true,
                });
            } else {
                gsap.to(el.class, {
                    onStart: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = false;
                    },
                    translateY: -150,
                    translateX: 0,

                    opacity: 0,
                    duration: 0.5,
                    ease: "power4.inOut",
                    overwrite: true,
                });
                gsap.to(el.class + "b", {
                    onStart: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = false;
                    },
                    translateY: -100,
                    translateX: 0,

                    opacity: 0,
                    duration: 0.5,
                    ease: "power4.inOut",
                    overwrite: true,
                });
            }
        }
    });
}

function animateImg(els) {
    els.forEach((el) => {
        if (!el.on) {
            if (isInViewport(el.el, -250)) {
                gsap.to(el.class, {
                    start: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = true;
                    },

                    translateY: 0,
                    translateX: 0,
                    scale: 1,
                    opacity: 1,
                    duration: 1.0,
                    ease: "power4.out",
                    overwrite: true,
                });
            } else {
                gsap.to(el.class, {
                    onStart: () => {
                        el.on = true;
                    },
                    onComplete: () => {
                        el.on = false;
                    },
                    translateY: 200,
                    translateX: 0,
                    scale: 1,
                    opacity: 0,
                    duration: 0.5,
                    ease: "elastic",
                    overwrite: true,
                });
            }
        }
    });
}

function main() {
    staticAnimation();

    //trigger();

    //const querrySelect = [];

    const querrySelectImg = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    let els = [];
    let imgs = [];
    let cIds = [];

    /* for (const index of querrySelect) {
        els.push({
            el: document.querySelector(`#text-${index}`),
            class: `#text-${index}`,
            on: false,
            id: index,
        });
    } */

    for (const index of querrySelectImg) {
        imgs.push({
            el: document.querySelector(`#bg-${index}`),
            class: `#bg-${index}`,
            on: false,
            id: index,
        });
        cIds.push(`#cg-${index}`);
    }
    //init(els);
    //initImg(imgs);
    //initCanvas(0, cIds);
    /*
    cIds.forEach((e) => {
        const el = document.querySelector(e);
        el.addEventListener("mouseenter", (ev) => {
            animateCanvas(e, cIds);
        });
        el.addEventListener("mouseleave", (ev) => {
            initCanvas(e, cIds);
        });
    });
    */

    window.onscroll = function () {
        //animate(els);
        //animateImg(imgs);
    };
}
main();
