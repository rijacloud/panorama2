<?php

return [
    'page' => 'CREATIVE DIGITAL NATIVE AGENCY',
    'title_1' => 'At wave.art, art and technology go hand in hand.',
    'title_2' => 'Let us bring your story to life!',
    'text_1' => 'Liven up your brand with a fresh perspective
    With our team of creative minds, we are able to deliver the best digital and technology works.',
    'text_2' => 'From A to Z, we have your organization covered. Our team is highly experienced in the field of
    technology and we always work on providing our clients with the best possible results within the
    stipulated time frame.',
    'digital_technologie' => 'DIGITAL ARTIST <br> AND TECHNOLOGY <br> CREATOR',
    'digital_technologie_desc' => "Wave.art is immersed in digital culture and innovation, and is always striving to reach new heights. <br><br>
    Our solutions are based on evolving technologies. Our infrastructure is designed to grow, increasing your ability to compete with your competitors. <br><br>
    Our experienced teams (art directors, designers, developers, graphic artists, scriptwriters, computer graphics artists, 3D animators, motion designers, illustrators and sound designers) bring together technical skills and creative daring to offer you the best digital stories.",
    'branding_contenu' => 'BRANDING <br> & CONTENTS',
    'branding_contenu_desc' => "We're a branding and content powerhouse.
    Wave.art is the perfect partner for businesses who want to create powerful, persuasive
    branding and content. We have a team of experienced creatives and designers who will help you
    communicate your message in a clear and engaging way. <br>
    Let’s take your business to the next level.",
    'salon_virtuel' => 'EXHIBITIONS <br> AND VIRTUAL EVENTS',
    'salon_virtuel_desc' => "Be where they are, without having to be there. 
    Wave.art quickly realised that virtual events would be a major development in the event industry. Our team of experts creates and develops amazing exhibitions to make your virtual events unique immersive experiences.",
    'design_emballage' => 'DESIGN <br> & PACKAGING',
    'design_emballage_desc' => "Helping brands gain market share by delivering effective creative solutions.
    Wave.art is a company of designers and artists who help brands find their creative voice and
    share it with the world. Our team of skilled professionals can help you develop dynamic,
    engaging marketing collateral that will set your brand apart from the competition.
    We specialize in helping our clients create powerful visual communications that capture the
    attention of their target audience and drive results. Our team of experienced professionals
    understands the art of persuasion, and we use our expertise to develop creative solutions that
    help our clients achieve their desired outcomes. With Wave.art on your side, you can be
    confident that your marketing efforts will reach new heights.",

    'culture_media' => 'CULTURE <br> & MEDIA',
    'culture_media_desc' => "Providing the best content in culture and media.
    Wave.art is the top destination for culture and media content. We provide our readers with
    insightful, engaging, and thought-provoking stories that challenge the status quo. Our team of
    talented creators are dedicated to bringing you the best in alternative news, art, and cultural
    commentary. <br>
    We -- bring your story to life",

    'show_virtuel' => 'VIRTUAL FAIRS <br> AND EXHIBITIONS',
    'show_virtuel_desc' => "Our team of experts creates and develops stunning exhibits that will amaze your audience. With
    Wave.art, you can reach a new level of engagement and interaction with your viewers. We Don't
    Just Design Exhibits, We Create Experiences.",

    'realit_augmente' => 'AUGMENTED REALITY <br> APPS',
    'realit_augmente_desc' => "Wave.art develops augmented reality applications using the latest methods and technologies. <br><br> Our applications are designed to explore the world in a new dimension. The virtual world then enters a sequence of real or realistic images.",

    'animation' => '2D & 3D <br> ANIMATIONS',
    'animation_desc' => "Our highly talented artists create compelling animations that capture your brand story and communicate your message with style. <br><br> Whether you need an animated film for your website or social media, Wave.art will make a difference!",

    'visite_immer' => 'IMMERSIVE <br> TOUR',
    'visite_immer_desc' => 'We merge reality and virtual experience for a successful immersive experience. <br><br> Wave.art allows you to glide through a fantasy world and experience something new for a few minutes.',

    'sound_design' => 'SOUND <br> DESIGN',
    'sound_design_desc' => "Because sound design is as important as graphic design, our sound designers develop a whole sound and artistic landscape, implementing a real identity to your projects. ",

    'poesie_virt' => 'VISUAL <br> POETRY',
    'poesie_virt_desc' => "Our digital creations are a blend of art and visual storytelling that captures attention. <br><br> Our artistic approach gives birth to poetic creations capable of aesthetically enchanting an architectural work or the universe of a brand.",

    'support_gif' => 'GIF MEDIA CREATION',
    'support_gif_desc' => "The perfect way to add a little movement and excitement to your digital advertising to generate high levels of engagement. <br><br> Our custom creations are appreciated for their quirky and humorous nature.",

    'jeux_video' => 'PERSONALISED <br> VIDEO GAMES',
    'jeux_video_desc' => 'Communicate differently through video games! Promote your company\'s brand image on social networks, during marketing operations or at trade shows with a custom mobile game. <br><br> Create your event game to improve your digital presence and stand out from your competitors. <br><br>Our gaming division designs and develops simple, easy-to-access, challenging video games that highlight your company\'s values or the quality of your services.',

    'decor_virtuel' => 'VIRTUAL <br> SCENERY',
    'decor_virtuel_desc' => "Our new solution for the creation of original and exclusive 'virtual scenery' content for the creative industries. <br><br> Whatever the mood, style or context, we create hyper-realistic content that will take you from one setting to another instantly.",

    'branding' => 'BRANDING',
    'branding_desc' => "Our creative branding and design studios will help you bring your brand to life. We specialize in
    creating engaging and effective branding that will set your company apart from the competition
    and will propel your business to success!",

    'art_culture' => 'ART & <br> CULTURE',
    'art_culture_desc' => "Art and culture that inspires creativity. Our selection of pieces will help you bring your ideas to
    life, and our team of experts are always available to help you find the perfect piece for your
    project.",

    'identite_visuelle' => 'VISUAL <br> IDENTITY',
    'identite_visuelle_desc' => "Our skilled team of designers will help you buil your visual identity, create a logo that perfectly
    represents your brand an all the assets that accompany your corporate identity.. With our great
    visual identity services, you'll be able to craft a brand that stands out from the competition and
    leaves a lasting impression on customers.",

    'webtoon' => 'DIGITAL COMICS',
    'webtoon_desc' => "Because a picture is worth a thousand words and even more so when it is captivating. <br><br> The talent and multiculturality of our artists and graphic designers allow an innovative, fresh and sparkling approach. A beautiful illustration combined with a good storytelling will give you a real pleasure.",

    'graphic_design' => 'GRAPHIC DESIGN <br> & PACKAGING',
    'graphic_design_desc' => "Creating beautiful design is in our DNA, simply send us your ideas and we'll take care of the rest!
    Our experienced team will work with you to create a stunning design that will wow your
    customers.",

    "motion_design" => 'MOTION DESIGN',
    'motion_design_desc' => "Choose to present your company or your products in a motion design. Very popular to express ideas, activities of a company or tell its legend.<br><br>
    The diversity of our team of motion designers allows us to offer a wide range of graphic styles and designs.",

    'film_animation' => 'ANIMATED FILMS <br> & ARCHITECTURAL VISUALISATION',
    'film_animation_desc' => "The architectural walk is undoubtedly best illustrated by animated films. <br><br>
    Wave.art's team of architects, graphic designers and motion designers create animations that transport viewers to the heart of real estate projects. <br><br>
    They enhance your project with an eye for detail. Our aesthetic, visual and sound work, and our editing methods reflect the volumetry, atmosphere, uses and urban quality. <br><br>
    Associate a photo-realistic perspective as the best business card of your project.",

    'see_more' => 'Read more'
];