<?php

return [
    'pmr_1' => "The alliance of the web, 3D technology and the talent of interior designers.",
    "pmr_2" => "Thanks to a total mastery of these 3 aspects and an assumed passion for decoration and lifestyle, Pimp my Room Company successfully develops services for professionals and individuals, which make the most of the union of technology and creativity.",
    "pano_title" => "PANORAMA <br> STUDIO",
    "pano_sub" => "Panorama Studio, wave.art's branch for the Spanish markets.",
    "pano_desc_1" => "Based in Madrid, the agency has developed a unique ecosystem combining marketing and communication.",
    "pano_desc_2" => "Agency <br>
    Creative <br>
    Comumnication <br>
    Digital",
    "pano_desc_3" => "We intervene at all stages of your project to enhance it while respecting the common thread.",
    "pano_art" => "PANORAMA <br> ART STUDIO",
    "pano_art_desc" => "Panorama Art Studio, a subsidiary of wave.art for the Middle East /Asia / US markets.",
    "pano_art_desc_1" => "Based in Dubai, the creative agency offers virtual stores and other virtual experiences.",
    "pano_art_desc_2" => "Promotional Brand Experiences <br>
    Permanent Retail Experiences <br>
    Trade- Shows & Exhibition Design <br>
    360 Virtual Expériences.",
    "logia_title" => "LOGIA",
    "logia_sub" => "Logia, the creative agency of passionate brands, base in Madagascar..",
    "logia_desc" => "Logia offers a wide range of services. From strategy, creative works, production and distribution of both offline and online campaigns. We are present on all fronts in order to deliver great campaigns that reflect your image",
    "logia_desc_2" => "What makes us at Logia exclusive is our passion and our vision to strive for greatness. We combine creativity, fervor and innovation to offer our clients impactful and offbeat strategies.",
    "isart_desc" => "Is'art, a contemporary art center based in Madagascar, is a place for the exhibition and sale of art.",
    "isart_desc_2" => "Thought as a place of residence of creation for Malagasy and foreign artists, Is Art is also a local scene of diffusion and musical performances and, of diffusion of video art. In short, a place of promotion of the contemporary art any discipline confused.",
    "isart_desc_3" => "It is a real scene of meeting and sharing open to all creative proposals.",
    'voir' => 'Read more',
    'down' => 'Download presentation'
];