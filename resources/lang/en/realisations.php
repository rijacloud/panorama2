<?php

return [
    'title_1' => 'We weave stories with the thread of your ideas, and innovate by creating a timeless legacy through your projects.',
    'title_2' => 'When creative technology meets the senses, your imagination comes alive.',
    'title_3' => 'Dare to experience the incredible through augmented reality or immersive tours. Because we know: your art is a part of you and a part of us.',
    'all' => 'SHOWREAL',
    'salons' => 'VIRTUAL <br> EXHIBITIONS AND EVENTS',
    'animation' => '2D & 3D <br> ANIMATIONS',
    'motion' => 'MOTION <br> DESIGN',
    'poesie' => 'VISUAL <br> POETRY',
    'film' => 'ANIMATED FILMS & <br> ARCHITECTURAL VISUALISATION',
    'decor' => 'VIRTUAL <br> SCENERY',
    'creation' => 'GIF MEDIA <br> CREATION',
    'jeux' => 'CUSTOM <br> VIDEO GAMES',
    'application' => 'AUGMENTED <br> REALITY APPS',
    'bd' => 'DIGITAL <br> COMICS',
    'sound' => 'SOUND <br> DESIGN',
    'visite' => 'IMMERSIVE <br> TOUR',
    'title' => 'Realisations - THE STORIES WE BROUGHT TO LIFE'


];