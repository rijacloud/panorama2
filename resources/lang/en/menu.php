<?php

return [
    'home' => 'HOME',
    'about' => 'ABOUT',
    'expertises' => 'Expertises',
    'realisations' => 'Realizations',
    'group' => 'The Group',
    'contact' => 'Contact'
];