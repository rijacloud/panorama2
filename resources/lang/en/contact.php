<?php

return [
    'title' => 'Contact us',
    'desc' => '<span class="font-true-medium">"Luxury is in every detail." <br>
    -- Hubert de Givenchy </span>
    Let\'s get into the details. <br>
    Let\'s give shape and colour to your projects. <br>
    Let\'s build a timeless legacy. <br>
    Let\'s create the incredible. <br>
    Contact us for more information.',
    'send' => 'SEND',
    'nom' => 'First name (*)',
    'prenom' => 'Last name (*)',
    'email' => 'Email (*)',
    'object' => 'Object',
    'message' => 'Message (*)'
];