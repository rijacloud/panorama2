<?php

return [
    'title' => 'About',
    'about_1' => 'Wave.art is part of a multi-cultural and international ecosystem of creatives and agencies specialising in digital, design, animation and new technologies.',
    'about_2' => 'Wave.art, an admirable synthesis of art, creativity and technology.',
    'tit' => 'DEVOTED TO LUXURY, ART AND CULTURE, WAVE ART IS A DIGITAL AND TECHNOLOGY-BASED CREATIVE AGENCY, BOTH GLOBAL AND INDEPENDENT.',
    'desc_1' => 'Proven technical and creative approaches, knowledge of contemporary art, visual communication, mastery of technologies (3D, VR, animation, gaming ...), combined with the talent of our teams, are the assets that make wave.art an ideal partner.',
    'desc_2' => 'We imagine and design <strong>visual narratives</strong>, <strong>virtual events</strong>, <strong>3D animation films</strong>, <strong>virtual sets or immersive visits</strong>, and <strong>customised video games</strong> that transport your customers into your world.',
    'desc_3' => 'Imbued with different cultures, we take an artistic point of view that transforms your projects into beautiful virtual and digital stories. Our technological know-how propels these stories into the vast field of possibilities.',
    'expertises' => 'OUR EXPERTISE',
    'group' => 'ABOUT THE GROUP'
];