<?php

return [
    'plus' => 'LEARN MORE',
    'title' => 'We bring your story to life',
    'creative' => 'CREATIVE',
    'studio' => 'STUDIO 3D',
    'digital' => 'DIGITAL',
    'v' => 'Virtual Media',
    'a' => 'Animated',
    'r' => 'Real Estate',
    'g' => 'Gaming',
    'n' => 'NFT',
    'm' => 'Motion design',
    's' => 'Sound design',

    'vm' => 'VIRTUAL <br>
    MEDIA',
    'vmp' => 'In a world where media content plays an increasingly important role,
    Panorama Studio provides unique services to enhance your brands,
    products and digital meetings.',
    'f' => 'VIRTUAL FAIRS <br> EVENTS',
    'fp' => 'Create events in unique virtual worlds for live
    virtual reality appointments from a computer or
    smartphone. <br> <br> 
    This is a major evolution of the event. Our team of
    experts creates and develops amazing exhibits to
    make your virtual events unique immersive
    experiences.',
    'sw' => 'VIRTUALS SHOW <br> ROOMS',
    'swp' => 'We create spaces that resemble you, to present
    your products or brands in an original way.  <br> <br>
    We create original worlds in which your
    customers can circulate and watch, as in a real
    show room, all your products. They will have
    access to the data of the products presented and
    will be able to interact directly.',
    'md' => 'VIRTUAL <br> DECORS  <br> MEDIOS DE <br> FOR ANIMATIQUE <br> MEDIA',
    'mdp' => 'Whether for green backgrounds or large screens,
    we provide you with a wide range of technological
    tools to meet your expectations. <br><br>
    These custom-made decorations will enhance
    your spaces (TV trays, green backgrounds,
    building halls...).',
    'an' => 'ANIMATED',
    'anp' => 'Whether it’s simple anime like GIFs, or more complex anime, our creative teams are there to support you in your
    projects. <br>
    The length of the format is dictated according to your specifications..',
    'and' => 'ANIMATION 2D & <br> 3D / SHOW REEL / <br> GIF',
    'andp' => 'Our artists create compelling and compelling animations that capture
    the history of your brand and communicate your message in style. <br> <br>
    Whether you need an animated film for your website, your social
    networks, Panorama Studio will make a difference !',
    'cr' => 'GIF MEDIA CREATION',
    'crp' => 'The perfect way to add a little movement and excitement to your
    digital advertising communication to generate a high level of
    engagement.<br> <br> 
    Our personalized creations are appreciated for their offbeat and
    humorous side.',
    're' => 'REAL ESTATE',
    'rep' => 'Panorama Studio is an actor who accompanies you in all your real estate needs. We offer 360° models, BIM
    models, perspectives, virtual tours, pre-calculated or real-time films, axonometries, 3D insertion in drone views…',
    'perp' => 'EXTERNAL AND INTERNAL PERSPECTIVES',
    'off' => 'Offer a unique representation of your indoor and
    outdoor spaces to your customers.',
    'm2' => '360° LAYOUT',
    'repre' => 'Represent your entire
    program through an orbital 3D
    model with or without floor
    cutting.',
    'vv' => 'EXTERNAL VIRTUAL TOUR',
    'des' => 'Discover the external environment
    of your project through real-time
    or pre-calculated simulations.',
    'vvi' => 'INTERNAL VIRTUAL TOUR',
    'vvip' => 'Immerse your customers in the very
    heart of your projects by allowing
    them to visit offices,
    accommodations, with our
    simulations of visits in real time or
    in pre calculated.',
    'sph' => 'SPHERE BALL',
    'sphp' => 'More than just a photo, offer
    immersion in an indoor and
    outdoor environment at 360°.',
    'rea' => 'AUGMENTED
    REALITY',
    'reap' => 'Immerse your customers in a
    virtual projection, for a
    realistic feeling of visit.',
    'ax' => 'AXONOMETRY',
    'axp' => 'More than a 3D tool, offer a
    different view of your
    projects that will sublimate
    all your spaces.',
    'pm' => 'MASS PLAN',
    'obt' => 'Offer an overview
    of your projects
    and its surrounding
    spaces thanks to
    the side plan.',
    'peli' => 'ANIMATED FILM',
    'real' =>  'Real commercial added value, display all the
    interior and exterior potential of your
    constructions through a 3D video. Motion
    design and sound design created to measure
    for each project.',
    'bim' => 'BIM',
    'pm' => 'Centralize and
    share your data in
    one collaborative
    work tool.',
    'prec' => '3D PRECALCULATED',
    "precp" => "Present your project through a
    rendering with a very high-quality
    graphics. It represents the most
    efficient solution to give a real insight
    into your future well.
    ANIMATED FILM
    Real commercial added value, display all the
    interior and exterior potential of your
    constructions through a 3D video. Motion
    design and sound design created to measure
    for each project",
    '3d' => '3D REAL TIME',
    'zaeaze' => 'Give a complete view and total
    immersion in every corner of your
    project consisting of a multitude of
    images calculated instantly just
    before the display on the screen.',
    'vide' => 'VIDEO <br>
    GAMES',
    'commu' => 'Communicate differently through video games! Promote the brand
    image of your company on social networks, in the context of
    marketing operations or at trade shows with a personalized mobile
    game.<br><br>
    Create your event game to improve your digital presence and
    stand out, for sure, from your competitors.<br><br>
    Our gaming division imagines and develops simple, easy to access,
    challenging video games that highlight the values of your company
    or the quality of your services.',
    'nft' => 'NFT',
    'nftp' => 'A true revolution in the digital world, NFTs are now akin to art and
    are traded or sold in a fast-growing market, in a unique format that
    seduces and fits into the era of time.',
    'nftpp' => 'You want to create NFT, Panorama Studio
    accompanies you in the artistic creation of these
    digital tokens. <br><br>
    Our teams will listen to you, and implement
    digitalization while respecting the vision of your
    creations, or will make proposals that
    correspond to your expectations, to give them
    life. <br><br>
    For a long time, artists worked conventionally,
    with brushes, chisels, etc... Then, with the
    appearance of the computer, the techniques
    change. We now use the computer to make
    images, to produce digital artistic objects.',
    'mtion' => 'MOTION DESIGN',
    'mtionp' => 'Panorama Studio supports companies around the world, to offer
    digital creations that meet the specifications and image of your
    brands or events. <br><br>
    We create in an original way, in a short and impactful digital format,
    that your customers will not forget.
    Step out of the conventional, break established codes, and our teams
    will stage your fantasy. <br><br>
    We will accompany these digitalizations with our sound design
    teams to sublimate these creations.',
    'sound' => 'SOUND <br>
    DESIGN',
    'sounp' => 'The sound design is made by our artists to put an original sound on all
    your digital works. <br><br>
    Panorama Studio will respect the musical atmosphere and type
    recommended.',
    'sobno' => 'ABOUT <br>
    US',
    'sobnop' => 'Panorama Studio, and its founders, from the world of
    communication and 3D development, have combined their skills to
    offer you a unique ecosystem around digital imaging, specializing in
    digital, design, animation and new technologies. Our mastery of 2D
    and 3D will allow us to make real your imagination.',
    'art' => 'DIGITAL ARTIST <br>
    AND CREATOR OF TECHNOLOGY',
    'artp' => 'Panorama Studio is immersed in the world of real
    estate, cuture, arts and digital innovation for ever
    more realistic looks that will convince your
    customers. <br><br>
    Our solutions use scalable technologies. Our
    experienced teams (artistic directors, designers,
    developers, graphic designers, screenwriters, graphic
    designers, 3D animators, motion designers,
    illustrators and sound designers) bring together the
    technical skills and all the creative audacity to offer
    you the best digital stories.',
    'cnos' => 'CONTACT',
    'envie'  => 'SEND'

];