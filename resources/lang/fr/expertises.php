<?php

return [
    'page' => 'AGENCE CREATIVE DIGITALE NATIVE',
    'title_1' => 'Pour wave.art, l’art et technologie vont de pair.',
    'title_2' => 'Let us bring your story to life!',
    'text_1' => 'Liven up your brand with a fresh perspective
    With our team of creative minds, we are able to deliver the best digital and technology works.',
    'text_2' => 'From A to Z, we have your organization covered. Our team is highly experienced in the field of
    technology and we always work on providing our clients with the best possible results within the
    stipulated time frame.',
    'digital_technologie' => 'ARTISTE DU DIGITAL <br> ET CRÉATEUR <br> DE TECHNOLOGIE',
    'digital_technologie_desc' => "Wave.art baigne dans la culture et l’innovation digitale pour aller toujours plus haut. <br> <br>
    Nos solutions font appels à des technologies évolutives. Nos infrastructures conçues pour se développer, augmentent votre capacité à concurrencer vos compétiteurs. <br> <br>
    Nos équipes expérimentées (directeurs artistiques, designers, développeurs, graphistes, scénaristes, infographistes, animateurs 3D, motion designers, illustrateurs et Sound designers) réunissent les compétences techniques et toutes les audaces créatives pour vous offrir les meilleurs histoires digitales.",
    'branding_contenu' => 'BRANDING <br> ET CRÉATION DE CONTENU',
    'branding_contenu_desc' => "Nous sommes une force de frappe en matière de branding et de contenu.
    Wave.art est le partenaire idéal pour les entreprises qui souhaitent créer un branding et un
    contenu puissant et convaincant. Nous disposons d'une équipe de créatifs et de designers
    expérimentés qui vous aideront à communiquer votre message de manière claire et attrayante.
    Ensemble, Faisons passer votre entreprise au niveau supérieur.",
    'salon_virtuel' => 'SALONS <br> ET EVENEMENTS VIRTUELS',
    'salon_virtuel_desc' => "Être là où ils sont, sans avoir à y être. <br><br>
    Wave.art, a très vite compris que les événements virtuels s'imposent comme une évolution majeure de l’événementiel. Notre équipe d'experts crée et développe des expositions stupéfiantes pour faire de vos évènements virtuels des expériences immersives uniques.",
    'design_emballage' => 'DESIGN <br> ET  EMBALLAGE',
    'design_emballage_desc' => "Nous aidons les marques à accroître leur part de marché en leur proposant des solutions
    créatives efficaces.
    Wave.art est une association de designers et d'artistes qui aident les enseignes à trouver leur
    voix créative et à la partager avec le monde entier. Notre équipe de professionnels qualifiés
    peut vous aider à développer des supports marketing dynamiques et attrayants qui permettront
    à votre marque de se démarquer de la concurrence.
    Nous sommes spécialisés dans l'aide à nos clients pour créer des communications visuelles
    puissantes qui captent l'attention de leur public cible et génèrent des résultats. Notre équipe de
    professionnels expérimentés saisit l'art de la persuasion, et nous utilisons notre expertise pour
    développer des solutions créatives qui aident nos clients à atteindre les résultats souhaités.
    Avec Wave.art à vos côtés, vous pouvez être sûr que vos efforts de marketing atteindront de
    nouveaux sommets.",

    'culture_media' => 'CULTURE <br> ET  MÉDIAS',
    'culture_media_desc' => "Fournir le meilleur contenu en matière de culture et de médias.
    Wave.art est la première destination pour le contenu culturel et médiatique. Nous proposons à
    nos lecteurs des histoires pertinentes, attrayantes et stimulantes qui remettent en question le
    statu-quo. Notre équipe de créateurs talentueux se consacre à vous apporter le meilleur de
    l'actualité alternative, de l'art et du commentaire culturel.
    Nous -- donnons vie à votre histoireCULTURE ET MÉDIAS",

    'show_virtuel' => 'SALONS <br> ET EVENEMENTS VIRTUELS',
    'show_virtuel_desc' => "Être là où ils sont, sans avoir à y être.
    Vos salons et évènements virtuels en direct depuis votre canapé à 360°en réalité virtuelle depuis votre ordinateur ou votre smartphone. <br><br>
    Wave.art, a très vite compris que les événements virtuels s'imposeront comme une évolution majeure de l’événementiel. Notre équipe d'experts crée et développe des expositions stupéfiantes pour faire de vos évènements virtuels des expériences immersives uniques.",

    'realit_augmente' => 'APPLICATIONS <br> DE RÉALITÉ <br> AUGMENTÉE',
    'realit_augmente_desc' => "Wave.art développe des applications de réalité augmentée à la pointe des dernières méthodes et technologies. <br><br>
    Nos applications sont conçues pour permettre d'explorer le monde dans une nouvelle dimension. Le monde virtuel entre alors dans une séquence d’images réelles ou réalistes.",
    'animation' => 'ANIMATIONS <br> 2D & 3D',
    'animation_desc' => "Nos artistes talentueux créent des animations attrayantes et convaincantes qui capturent l'histoire de votre marque et communiquent votre message avec style. <br><br> Que vous ayez besoin d'un film d'animation pour votre site web, vos réseaux sociaux, Wave.art marquera votre différence!",
    'visite_immer' => 'VISITE <br> IMMERSIVE',
    'visite_immer_desc' => 'Nous mêlons réalité et expérience virtuelle pour une expérience 
    immersive réussie. <br><br>
    Wave.art propose de planer dans un univers fantasmé pour vivre une expérience inédite le temps de quelques minutes.',

    'film_animation' => 'FILMS D’ANIMATION <br> & VISUALISATION ARCHITECTURALE',
    'film_animation_desc' => "La promenade architecturale trouve sans conteste son meilleur outil dans le film d'animation.<br><br>
    L’équipe d’architectes, de graphistes, de motions designers de Wave.art réalise des animations qui transportent les spectateurs au coeur des projets immobilier. <br><br>
    Elle magnifie votre projet dans un soucis du détail. Notre travail esthétique, visuel et sonore, et nos méthodes de montages rendent compte de la volumétrie, des ambiances, des usages et de la qualité urbaines. <br><br>
    Associez-y une perspective photo-réaliste comme meilleure carte de visite de votre projet.",

    'sound_design' => 'SOUND <br> DESIGN',
    'sound_design_desc' => "Parce que la conception sonore et aussi importante que la conception graphique, nos Sound designers développent tout un paysage sonore et artistique, en implémentant une véritable identité à vos projets.",

    'poesie_virt' => 'POÉSIE <br> VISUELLE',
    'poesie_virt_desc' => "Nos créations digitales, mélange d'art et de narrations visuelles captent l’attention. <br><br>
    Notre approche artistique donne naissance à des créations poétiques capables d’enchanter, esthétiquement, un ouvrage architectural ou l'univers d'une marque.",
    'support_gif' => 'CREATION DE <br> SUPPORTS GIF',
    'support_gif_desc' => "Le moyen idéal d'ajouter un peu de mouvement et d'excitation à votre communication digitale publicitaire pour générer un taux d'engagement important. <br><br>
    Nos créations personnalisées sont appréciées pour leur côté décalé et humoristique.",

    'jeux_video' => 'JEUX VIDÉO <br> PERSONNALISÉS',
    'jeux_video_desc' => 'Communiquer autrement grâce aux jeux vidéo ! Mettez en avant l\'image de marque de votre entreprise sur les réseaux sociaux, dans le cadre d\'opération marketing ou encore lors de salons professionnels grâce à un jeu mobile personnalisé.
    <br><br>
    Créez votre event game pour améliorez votre présence digitale et vous démarquer, à coup sûr, de vos concurrents. <br><br> Notre pôle gaming imagine et développe des jeux vidéo simples, faciles d’accès, challengeant qui mettent en avant les valeurs de votre entreprise ou la qualité de vos services.',

    'decor_virtuel' => 'DÉCOR <br> VIRTUEL',
    'decor_virtuel_desc' => "Notre nouvelle solution pour la création de contenus de «décors virtuels» originaux et exclusifs destinés aux industries créatives. <br><br>
    Quelque soit l'ambiance, le style ou le contexte, nous créons des contenus hyper-réalistes qui vous feront passer d'un décor à un autre instantanément.",

    'branding' => 'BRANDING',
    'branding_desc' => "Nos studios de branding et de design créatifs vous aideront à donner vie à votre marque. Nous
    sommes spécialisés dans la création d'une image de marque attrayante et efficace qui
    distingue votre entreprise de la concurrence et la propulsera vers le succès !",

    'art_culture' => 'ART <br> & CULTURE',
    'art_culture_desc' => "L'art et la culture qui inspirent la créativité. Notre sélection de pièces vous aidera à donner vie à
    vos idées, et notre équipe d'experts est toujours disponible pour vous aider à trouver la pièce
    parfaite pour votre projet.",

    'identite_visuelle' => 'BRANDING <br> & IDENTITÉ VISUELLE',
    'identite_visuelle_desc' => "Notre équipe de designers qualifiés vous aidera à construire votre identité visuelle, à créer un
    logo qui représente parfaitement votre marque et tous les actifs qui accompagnent votre
    identité corporative. Grâce à nos services d'identité visuelle, vous serez en mesure de créer une
    marque qui se démarque de la concurrence et laisse une impression durable sur les clients.",

    'webtoon' => 'BD DIGITALE',
    'webtoon_desc' => "Parce qu'une image vaut mille mots et encore plus lorsque elle est captivante. <br><br>
    Le talent et la multiculturalité de nos dessinateurs et graphistes permettent une approche innovante, fraîche et pétillante. Une belle illustration alliée à un bon storytelling suscitera un véritable plaisir.",

    'graphic_design' => 'GRAPHIC DESIGN <br> & PACKAGING',
    'graphic_design_desc' => 'Créer un beau design est dans notre ADN, il suffit de nous envoyer vos idées et nous nous
    occupons du reste ! Notre équipe chevronnée travaillera avec vous pour créer un design
    époustouflant qui épatera vos clients.',
    "motion_design" => 'MOTION DESIGN',
    'motion_design_desc' => "Optez pour une présentation de votre entreprise ou vos produits en motion design. Très en vogue pour exprimer les idées, les activités d'une entreprise ou raconter sa légende. <br><br>
    La diversité de notre équipe de motion designers nous permet d'offrir un très large éventail de styles graphiques et de designs.",

    'see_more' => 'Lire plus'

];