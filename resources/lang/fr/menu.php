<?php

return [
    'home' => 'ACCUEIL',
    'about' => 'A PROPOS',
    'expertises' => 'EXPERTISES',
    'realisations' => 'REALISATIONS',
    'group' => 'LE GROUPE',
    'contact' => 'CONTACT'
];