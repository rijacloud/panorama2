<?php

return [
    'title' => 'A propos',
    'about_1' => 'Wave.art fait partie d’un écosystème multi-culturel et international de créatifs et d’agences spécialisées dans le digital, le design, l’animation et les nouvelles technologies.',
    'about_2' => 'Wave.art, une admirable synthèse entre l’art, la créativité et la technologie.',
    'tit' => 'DÉDIÉE AU LUXE, À L\'ART ET À LA CULTURE, WAVE ART EST UNE AGENCE CRÉATIVE DIGITALE ET TECHNOLOGIQUE, GLOBALE ET INDÉPENDANTE.',
    'desc_1' => 'Démarches techniques et créatives éprouvées, connaissances de l’art contemporain, de la communication visuelle, maîtrise des technologies (3D, VR, animation, gaming …), combinés au talent de nos équipes, voici les atouts qui font de wave.art un partenaire idéal.',
    'desc_2' => 'Nous imaginons et concevons des <strong>narrations visuelles</strong>, des <strong>évènements virtuels</strong>, des <strong>films d’animation 3D</strong>, des <strong>décors virtuels ou visites immersives</strong>, des <strong>jeux vidéos personnalisés</strong> qui transportent vos clients dans votre univers.',
    'desc_3' => 'Imprégnés de différentes cultures, nous adoptons un point de vue artistique qui transforment vos projets en de belles histoires virtuelles et digitales. Notre savoir-faire technologique propulse ces histoires dans le vaste champ des possibles.',
    'expertises' => 'NOS EXPERTISES',
    'group' => 'A PROPOS DU GROUPE'
];