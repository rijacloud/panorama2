<?php

return [
    'title_1' => 'Nous tissons des histoires avec le fil de vos idées, et innovons en créant un héritage intemporel à travers vos projets.',
    'title_2' => 'Quand la technologie créative se met au service des sens, votre imagination prend vie.',
    'title_3' => 'Osez l’incroyable au travers de réalités augmentées ou de visites immersives. Parce que nous l\'avons compris : votre art, c\'est une part de vous et une part de nous.',
    'all' => 'SHOWREAL',
    'salons' => 'SALONS & <br> ÉVÉNEMENTS VIRTUELS',
    'animation' => 'ANIMATIONS <br> 2D & 3D',
    'motion' => 'MOTION <br> DESIGN',
    'poesie' => 'POÉSIE <br> VISUELLE',
    'film' => 'FILMS D’ANIMATION & <br> VISUALISATION ARCHITECTURALE',
    'decor' => 'DÉCOR <br> VIRTUEL',
    'creation' => 'CREATION <br> DE SUPPORTS GIF',
    'jeux' => 'JEUX VIDÉO <br> PERSONNALISÉS',
    'application' => 'APPLICATIONS <br> DE RÉALITÉ AUGMENTÉE',
    'bd' => 'BD <br> DIGITALE',
    'sound' => 'SOUND <br> DESIGN',
    'visite' => 'VISITE <br> IMMERSIVE',
    'title' => 'Réalisations - THE STORIES WE BROUGHT TO LIFE'
];