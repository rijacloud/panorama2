<?php

return [
    'title' => 'Contactez-nous',
    'desc' => '<span class="font-true-medium">
        "Le luxe est dans chaque détail." <br> 
        -- Hubert de Givenchy
        </span>
        <br> 
        Entrons dans les détails. <br> 
        Donnons formes et couleurs à vos projets.<br> 
        Construisons un héritage intemporel. <br> 
        Créons l’incroyable. <br> 
        Contactez-nous. ',
    'send' => 'ENVOYER',
    'nom' => 'Nom (*)',
    'prenom' => 'Prénom (*)',
    'email' => 'Adresse email (*)',
    'object' => 'Sujet (*)',
    'message' => 'Message (*)'
];