<?php

return [
    'pmr_1' => "L'alliance du web, de la technologie 3D et du talent de décorateurs d’intérieur.",
    "pmr_2" => "Grâce à une maîtrise totale de ces 3 aspects et une passion assumée pour la décoration et le lifestyle, Pimp my Room Company développe avec succès des services aux professionnels et aux particuliers, qui tirent le meilleur parti de l'union de la technique et la créativité.",
    "pano_title" => "PANORAMA <br> STUDIO",
    "pano_sub" => "Panorama Studio, filiale de wave.art sur les marchés espagnols.",
    "pano_desc_1" => "Basée à Madrid, l’agence a développé un écosystème unique alliant Marketing et communication.",
    "pano_desc_2" => "Agence <br>
    Creative <br>
    Communication <br>
    Digital",
    "pano_desc_3" => "Nous intervenons à toutes les étapes de votre projet pour le mettre en valeur tout en respectant le fil conducteur.",
    "pano_art" => "PANORAMA <br> ART STUDIO",
    "pano_art_desc" => "Panorama Art Studio, filiale de wave.art sur les marchés du moyen orient / Asie / Etats Unis.",
    "pano_art_desc_1" => "Basée à Dubaï, l’agence créative propose des créations de virtual stores et autres expériences virtuelles. ",
    "pano_art_desc_2" => "Promotional Brand Experiences <br>
    Permanent Retail Experiences <br>
    Conception de salons et d'expositions <br>
    Expériences virtuelles 360°",
    "logia_title" => "LOGIA",
    "logia_sub" => "Logia, l'agence créative basée à Madagascar",
    "logia_desc" => "Logia offre un large panel de services. De la stratégie aux travaux créatifs, en passant par la production et la réalisation de campagnes offline et online. Nous sommes présents sur tous les fronts afin de livrer de grandes campagnes qui reflètent votre image.",
    "logia_desc_2" => "What makes us at Logia exclusive is our passion and our vision to strive for greatness. We combine creativity, fervor and innovation to offer our clients impactful and offbeat strategies.",
    "isart_desc" => "Is’art, centre d'art contemporain basé à Madagascar, est un lieu d'exposition et de vente d’art.",
    "isart_desc_2" => "Pensé comme un lieu de résidence de création pour des artistes malgaches et étrangers, Is’art est aussi une scène locale de diffusion et de performances musicales et, de diffusion d’art vidéo. En somme, un lieu de promotion de l'art contemporain toute discipline confondu.",
    "isart_desc_3" => "Véritable scène de rencontre et de partage ouvert à toutes propositions créatives.",
    'voir' => 'Lire plus',
    'down' => 'Télécharger la présentation'

];