<?php

return [
    'title' => 'Sobre',
    'about_1' => 'Wave.art forma parte de un ecosistema multicultural e internacional de creativos y agencias especializadas en digital, diseño, animación y nuevas tecnologías.',
    'about_2' => 'Wave.art, una admirable síntesis de arte, creatividad y tecnología.',
    'tit' => 'DEDICADA AL LUJO, EL ARTE Y LA CULTURA, WAVE.ART ES UNA AGENCIA CREATIVA DE BASE DIGITAL Y TECNOLÓGICA, GLOBAL E INDEPENDIENTE.',
    'desc_1' => 'Los enfoques técnicos y creativos probados, el conocimiento del arte contemporáneo, la comunicación visual, el dominio de las tecnologías (3D, VR, animación, gaming...), combinados con el talento de nuestros equipos, son las bazas que hacen de wave.art un socio ideal.',
    'desc_2' => 'Imaginamos y diseñamos <strong>narrativas visuales</strong>, <strong>eventos virtuales</strong>, <strong>películas de animación en 3D</strong>, <strong>decorados virtuales o visitas inmersivas</strong>, y <strong>videojuegos personalizados</strong> que transportan a sus clientes a su mundo.',
    'desc_3' => 'Imbuidos de diferentes culturas, adoptamos un punto de vista artístico que transforma sus proyectos en bellas historias virtuales y digitales. Nuestros conocimientos tecnológicos impulsan estas historias en el vasto campo de las posibilidades.',
    'expertises' => 'NUESTRA EXPERIENCIA',
    'group' => 'SOBRE EL GRUPO'
];