<?php

return [
    'page' => 'AGENCIA CREATIVA NATIVA DIGITAL',
    'title_1' => 'En wave.art, el arte y la tecnología van de la mano.',
    'title_2' => 'Déjenos dar vida a su historia.',
    'text_1' => 'Liven up your brand with a fresh perspective
    With our team of creative minds, we are able to deliver the best digital and technology works.',
    'text_2' => 'From A to Z, we have your organization covered. Our team is highly experienced in the field of
    technology and we always work on providing our clients with the best possible results within the
    stipulated time frame.',

    'digital_technologie' => 'ARTISTA DIGITAL  <br> Y CREADOR <br> DE TECNOLOGÍA',
    'digital_technologie_desc' => "Wave.art está inmersa en la cultura digital y la innovación, y siempre se esfuerza por alcanzar nuevas cotas.<br> <br>
    Nuestras soluciones se basan en tecnologías en evolución. Nuestra infraestructura está diseñada para crecer, aumentando su capacidad de competir con sus competidores.<br> <br>
    Nuestros experimentados equipos (directores de arte, diseñadores, desarrolladores, artistas gráficos, guionistas, infografistas, animadores 3D, diseñadores de movimiento, ilustradores y diseñadores de sonido) reúnen habilidades técnicas y audacia creativa para ofrecerle las mejores historias digitales.",

    'branding_contenu' => 'BRANDING <br> ET CRÉATION DE CONTENU',
    'branding_contenu_desc' => "Nous sommes une force de frappe en matière de branding et de contenu.
    Wave.art est le partenaire idéal pour les entreprises qui souhaitent créer un branding et un
    contenu puissant et convaincant. Nous disposons d'une équipe de créatifs et de designers
    expérimentés qui vous aideront à communiquer votre message de manière claire et attrayante.
    Ensemble, Faisons passer votre entreprise au niveau supérieur.",

    'salon_virtuel' => 'EXPOSICIONES <br> Y EVENTOS VIRTUALES',
    'salon_virtuel_desc' => "Esté donde ellos están, sin tener que estar allí. Sus exposiciones y eventos virtuales en directo desde su sofá en realidad virtual de 360° desde su ordenador o su smartphone.  <br><br>
    Wave.art se dio cuenta rápidamente de que los eventos virtuales iban a suponer un gran avance en el sector de los eventos. Nuestro equipo de expertos crea y desarrolla exposiciones increíbles para que sus eventos virtuales sean experiencias inmersivas únicas.",

    'design_emballage' => 'DESIGN <br> ET  EMBALLAGE',
    'design_emballage_desc' => "Nous aidons les marques à accroître leur part de marché en leur proposant des solutions
    créatives efficaces.
    Wave.art est une association de designers et d'artistes qui aident les enseignes à trouver leur
    voix créative et à la partager avec le monde entier. Notre équipe de professionnels qualifiés
    peut vous aider à développer des supports marketing dynamiques et attrayants qui permettront
    à votre marque de se démarquer de la concurrence.
    Nous sommes spécialisés dans l'aide à nos clients pour créer des communications visuelles
    puissantes qui captent l'attention de leur public cible et génèrent des résultats. Notre équipe de
    professionnels expérimentés saisit l'art de la persuasion, et nous utilisons notre expertise pour
    développer des solutions créatives qui aident nos clients à atteindre les résultats souhaités.
    Avec Wave.art à vos côtés, vous pouvez être sûr que vos efforts de marketing atteindront de
    nouveaux sommets.",

    'culture_media' => 'CULTURE <br> ET  MÉDIAS',
    'culture_media_desc' => "Fournir le meilleur contenu en matière de culture et de médias.
    Wave.art est la première destination pour le contenu culturel et médiatique. Nous proposons à
    nos lecteurs des histoires pertinentes, attrayantes et stimulantes qui remettent en question le
    statu-quo. Notre équipe de créateurs talentueux se consacre à vous apporter le meilleur de
    l'actualité alternative, de l'art et du commentaire culturel.
    Nous -- donnons vie à votre histoireCULTURE ET MÉDIAS",

    'show_virtuel' => 'EXPOSICIONES <br> Y EVENTOS VIRTUALES',
    'show_virtuel_desc' => "Esté donde ellos están, sin tener que estar allí. <br><br>
    Wave.art se dio cuenta rápidamente de que los eventos virtuales iban a suponer un gran avance en el sector de los eventos. Nuestro equipo de expertos crea y desarrolla exposiciones increíbles para que sus eventos virtuales sean experiencias inmersivas únicas.",

    'realit_augmente' => 'APLICACIONES <br> DE REALIDAD <br> AUMENTADA',
    'realit_augmente_desc' => "Wave.art desarrolla aplicaciones de realidad aumentada utilizando los últimos métodos y tecnologías. <br><br>
    Nuestras aplicaciones están diseñadas para explorar el mundo en una nueva dimensión. El mundo virtual entra entonces en una secuencia de imágenes reales o realistas.",

    'animation' => 'ANIMACIONES <br> 2D Y 3D',
    'animation_desc' => "Nuestros artistas de gran talento crean animaciones convincentes que captan la historia de su marca y comunican su mensaje con estilo. <br><br> Tanto si necesita una película de animación para su sitio web como para las redes sociales, ¡Wave.art marcará la diferencia!",

    'visite_immer' => 'TOUR <br> INMERSIVO',
    'visite_immer_desc' => 'Mezclamos la realidad y la experiencia virtual para lograr una experiencia inmersiva exitosa <br><br>
    Wave.art le permite deslizarse por un mundo de fantasía y experimentar algo nuevo durante unos minutos.',

    'film_animation' => 'PELÍCULAS DE ANIMACIÓN <br> Y VISUALIZACIÓN ARQUITECTÓNICA',
    'film_animation_desc' => "No cabe duda de que el paseo arquitectónico se ilustra mejor con películas de animación. <br><br>
    El equipo de arquitectos, diseñadores gráficos y motion designers de Wave.art crea animaciones que transportan el espectador al corazón de los proyectos inmobiliarios. <br><br>
    Realzan su proyecto con un ojo para el detalle. Nuestro trabajo estético, visual y sonoro, y nuestros métodos de edición reflejan la volumetría, la atmósfera, los usos y la calidad urbana. <br><br>
    Asocie una perspectiva fotorealista como la mejor tarjeta de presentación de su proyecto.",

    'sound_design' => 'DISEÑO <br> DE SONIDO',
    'sound_design_desc' => "Porque el diseño de sonido es tan importante como el diseño gráfico, nuestros diseñadores de sonido desarrollan todo un paisaje sonoro y artístico, implementando una verdadera identidad a sus proyectos.",

    'poesie_virt' => 'POESÍA <br> VISUAL',
    'poesie_virt_desc' => "Nuestras creaciones digitales son una mezcla de arte y narración visual que capta la atención. <br><br>
    Nuestro enfoque artístico da lugar a creaciones poéticas capaces de encantar estéticamente una obra arquitectónica o el universo de una marca.",

    'support_gif' => 'CREACIÓN <br> DE MEDIOS GIF',
    'support_gif_desc' => "La forma perfecta de añadir un poco de movimiento y emoción a su publicidad digital para generar altos niveles de compromiso. <br><br> Nuestras creaciones personalizadas son apreciadas por su carácter extravagante y humorístico.",

    'jeux_video' => 'VIDEOJUEGOS <br> PERSONALIZADOS',
    'jeux_video_desc' => 'Comunica de forma diferente a través de los videojuegos. Promueva la imagen de marca de su empresa en las redes sociales, durante las operaciones de marketing o en las ferias con un juego móvil personalizado. <br><br>
    Cree su evento game para mejorar su presencia digital y diferenciarse de sus competidores. <br><br> Nuestra división de juegos diseña y desarrolla videojuegos sencillos, de fácil acceso y que suponen un reto para destacar los valores de su empresa o la calidad de sus servicios.',

    'decor_virtuel' => 'ESCENOGRAFÍA <br> VIRTUAL',
    'decor_virtuel_desc' => "Nuestra nueva solución para la creación de contenidos originales y exclusivos de 'escenarios virtuales' para las industrias creativas. <br><br>
    Sea cual sea el estado de ánimo, el estilo o el contexto, creamos contenidos hiperrealistas que le llevarán de un escenario a otro al instante.",

    'branding' => 'BRANDING',
    'branding_desc' => "Nos studios de branding et de design créatifs vous aideront à donner vie à votre marque. Nous
    sommes spécialisés dans la création d'une image de marque attrayante et efficace qui
    distingue votre entreprise de la concurrence et la propulsera vers le succès !",

    'art_culture' => 'ART <br> & CULTURE',
    'art_culture_desc' => "L'art et la culture qui inspirent la créativité. Notre sélection de pièces vous aidera à donner vie à
    vos idées, et notre équipe d'experts est toujours disponible pour vous aider à trouver la pièce
    parfaite pour votre projet.",

    'identite_visuelle' => 'BRANDING <br> & IDENTITÉ VISUELLE',
    'identite_visuelle_desc' => "Notre équipe de designers qualifiés vous aidera à construire votre identité visuelle, à créer un
    logo qui représente parfaitement votre marque et tous les actifs qui accompagnent votre
    identité corporative. Grâce à nos services d'identité visuelle, vous serez en mesure de créer une
    marque qui se démarque de la concurrence et laisse une impression durable sur les clients.",

    'webtoon' => 'CÓMIC DIGITAL',
    'webtoon_desc' => "Porque una imagen vale más que mil palabras y más aún si es cautivadora. <br><br> El talento y la multiculturalidad de nuestros artistas y diseñadores gráficos permiten un enfoque innovador, fresco y chispeante. Una bella ilustración combinada con una buena narración le proporcionará un verdadero placer.",

    'graphic_design' => 'GRAPHIC DESIGN <br> & PACKAGING',
    'graphic_design_desc' => 'Créer un beau design est dans notre ADN, il suffit de nous envoyer vos idées et nous nous
    occupons du reste ! Notre équipe chevronnée travaillera avec vous pour créer un design
    époustouflant qui épatera vos clients.',

    "motion_design" => 'MOTION DESIGN',
    'motion_design_desc' => "Elija presentar su empresa o sus productos en un vídeo de diseño en movimiento. Muy popular para expresar ideas, actividades de una empresa o contar su leyenda. <br><br>
    La diversidad de nuestro equipo de motion designers nos permite ofrecer una amplia gama de estilos gráficos y diseños.",

    'see_more' => 'Lee mas'

];