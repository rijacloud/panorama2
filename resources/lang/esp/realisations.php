<?php

return [
    'title_1' => 'Tejemos historias con el hilo de tus ideas, e innovamos creando un legado atemporal a través de tus proyectos.',
    'title_2' => 'Cuando la tecnología creativa se une a los sentidos, tu imaginación cobra vida.',
    'title_3' => 'Atrévete a experimentar lo increíble a través de la realidad aumentada o las visitas inmersivas. Porque lo sabemos: tu arte forma parte de ti y de nosotros.',
    'all' => 'SHOWREAL',
    'salons' => 'EXPOSICIONES Y <br> EVENTOS VIRTUALES',
    'animation' => 'ANIMACIONES <br> 2D Y 3D',
    'motion' => 'DISEÑO <br> DE MOVIMIENTO',
    'poesie' => 'POESÍA <br> VISUAL',
    'film' => 'PELÍCULAS DE ANIMACIÓN Y <br> VISUALIZACIÓN ARQUITECTÓNICA',
    'decor' => 'ESCENOGRAFÍA <br> VIRTUAL',
    'creation' => 'CREACIÓN <br> DE MEDIOS GIF',
    'jeux' => 'VIDEOJUEGOS <br> PERSONALIZADOS',
    'application' => 'APLICACIONES <br> DE REALIDAD AUMENTADA',
    'bd' => 'COMICS <br> DIGITALES',
    'sound' => 'DISEÑO <br> DE SONIDO',
    'visite' => 'TOUR <br> INMERSIVO',
    'title' => 'Logros - THE STORIES WE BROUGHT TO LIFE'


];