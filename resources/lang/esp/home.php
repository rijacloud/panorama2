<?php

return [
    'plus' => 'LEARN MORE',
    'title' => 'We bring your story to life',
    'creative' => 'CREATIVA',
    'studio' => 'STUDIO 3D',
    'digital' => 'DIGITAL',
    'v' => 'Virtual Media',
    'a' => 'Animados',
    'r' => 'Real Estate',
    'g' => 'Gaming',
    'n' => 'NFT',
    'm' => 'Motion design',
    's' => 'Sound design',
    'vm' => 'VIRTUAL <br>
    MEDIA',
    'vmp' => 'En un mundo en el que los contenidos multimedia ocupan un lugar cada vez
    más importante, Panorama Studio realiza prestaciones únicas para
    valorizar sus marcas, productos y reuniones digitales.',
    'f' => 'FERIAS Y EVENTOS <br> VIRTUALES',
    'fp' => '     Crear eventos en universos virtuales únicos para dar cita en vivo en realidad virtual desde un ordenador o smartphone. <br><br>
    Esta es una evolución importante del evento. Nuestro equipo de expertos crea y desarrolla impresionantes exposiciones para hacer de sus eventos virtuales experiencias inmersivas únicas
    ',
    'sw' => 'SHOW ROOMS <br> VIRTUALES',
    'swp' => '     Crear eventos en universos virtuales únicos para dar cita en vivo en realidad virtual desde un ordenador o smartphone.
    Esta es una evolución importante del evento. Nuestro equipo de expertos crea y desarrolla impresionantes exposiciones para hacer de sus eventos virtuales experiencias inmersivas únicas
',
    'md' => 'MEDIADECORS <br> VIRTUALES PARA  <br> MEDIOS DE <br> COMUNICACIÓN <br> ANIMADOS',
    'mdp' => 'Ya sea para fondos verdes o grandes pantallas,
    ponemos a su disposición una amplia gama de
    herramientas tecnológicas para satisfacer sus
    expectativas. <br><br>
    Estos decorados creados a medida, sabrán poner
    en valor sus espacios (bandejas TV, decorados de
    fondos verdes, pasillos de edificio...).',
    'an' => 'ANIMADOS',
    'anp' => '                            Ya se trate de animaciones simples como GIF o animadas más complejas, nuestros equipos creativos están aquí
    para acompañarle en sus proyectos.  <br> 
    La duración del formato se dicta según sus especificaciones.',
    'and' => 'ANIMATION 2D & <br> 3D / SHOW REEL / <br> GIF',
    'andp' => '                            Nuestros artistas crean animaciones atractivas y convincentes que
    capturan la historia de su marca y transmiten su mensaje con estilo. <br> <br>
    Ya sea que necesite una película de animación para su sitio web, sus
    redes sociales, Panorama Studio marcará su diferencia!',
    'cr' => 'CREACIÓN DE SOPORTES GIF',
    'crp' => 'La forma ideal de añadir un poco de movimiento y emoción a su comunicación digital publicitaria para generar una tasa de participación significativa.
    Nuestras creaciones personalizadas son apreciadas por su lado extravagante y humorístico.',
    're' => 'REAL ESTATE',
    'rep' => 'Panorama Studio le acompaña en todas sus necesidades inmobiliarias. Ofrecemos modelos 360o, maquetas BIM, perspectivas, visitas virtuales, películas en precalculado o en tiempo real, axonometrías, inserción 3D en vistas drones...',
    'perp' => 'PERSPECTIVAS EXTERNAS O INTERNAS',
    'off' => 'Ofrezca a sus clientes una representación única de sus espacios interiores o exteriores.',
    'm2' => 'MAQUETA 360°',
    'repre' => 'Represente todo su programa a través de una maqueta orbital 3D con o sin etapas de corte.',
    'vv' => 'VISITA VIRTUAL EXTERIOR',
    'des' => 'Descubre el entorno exterior de tu proyecto a través de simulaciones en tiempo real o pre-calculadas.',
    'vvi' => 'VISITA VIRTUAL INTERIOR',
    'vvip' => 'Sumerja a sus clientes en el corazón mismo de sus proyectos, permitiéndoles visitar oficinas, alojamientos, con nuestras simulaciones de visitas en tiempo real o en pre-calculado.',
    'sph' => 'SPHERE BALL',
    'sphp' => 'Más que una simple foto, proponga una inmersión en un ambiente interior y exterior de 360°.',
    'rea' => 'REALIDAD AUMENTADA',
    'reap' => 'Sumerja a sus clientes en una proyección virtual, para una sensación realista de la visita.',
    'ax' => 'AXONOMETRÍA',
    'axp' => 'Más que una herramienta 3D, ofrece una toma diferente de tus proyectos que sublimará todos tus espacios.',
    'pm' => 'PLAN MASA',
    'obt' => 'Obtenga una visión general de sus proyectos y de sus espacios circundantes con el plano lateral',
    'peli' => 'PELÍCULA PROMOCIONAL',
    'real' =>  'Real plusvalía comercial, muestre todo el potencial interior y exterior de sus construcciones a través de un vídeo 3D. Diseño de sonido y movimiento creado a medida para cada proyecto.',
    'bim' => 'BIM',
    'pmb' => 'Desde nubes de
    puntos o planos, realizamos todo tipo de maqueta BIM.',
    'prec' => '3D PRECALCULADO',
    "precp" => "Presente su proyecto a través de un renderizado con gráficos de muy alta calidad. Representa la solución más eficaz para dar una visión real de su futuro bien.",
    '3d' => '3D TIEMPO REA',
    'zaeaze' => 'Presente su proyecto a través de un renderizado con gráficos de muy alta calidad. Representa la solución más eficaz para dar una visión real de su futuro bien.',
    'vide' => 'VIDEO <br>
    JUEGOS',
    'commu' => '¡Comunicar de forma diferente gracias a los videojuegos! Promociona la marca de su empresa en las redes sociales, en operaciones de marketing o en ferias profesionales con un juego móvil personalizado.
    Crea su event game para mejorar su presencia digital y destacarle de la competencia.
    Nuestro centro de juegos imagina y desarrolla videojuegos sencillos, fáciles de acceder, desafiantes que ponen de relieve los valores de su empresa o la calidad de sus servicios.',
    'nft' => 'NFT',
    'nftp' => 'Verdadera revolución del mundo digital, las NFTs se asemejan hoy al arte y se intercambian o venden en un mercado en fuerte crecimiento, en un formato inédito que seduce, y que se inscribe en la era del tiempo.',
    'nftpp' => 'Si desea crear NFT, Panorama Studio le acompaña en la creación artística de estos tokens digitales.
    Nuestros equipos le escucharán y realizarán la digitalización respetando la visión de sus creaciones, o le harán propuestas que correspondan a sus expectativas, para darles vida.
    <br> <br>
    Durante mucho tiempo, los artistas han trabajado convencionalmente, con pincel, cincel, etc… Luego, con la aparición de la computadora, las técnicas cambian. Ahora usamos la computadora para hacer imágenes, producir objetos de arte digital.',
    'mtion' => 'MOTION DESIGN',
    'mtionp' => 'Panorama Studio acompaña a empresas de todo el mundo para proponer creaciones digitales que respondan a las especificaciones y a la imagen de sus marcas o eventos.
    Ponemos en escena de manera original, en un formato digital corto y llamativo, que sus clientes no olvidarán.
    Salgan de lo convencional, rompan los códigos establecidos, y nuestros equipos pondrán en escena su imaginación.
    Acompañaremos estas digitalizaciones con nuestros equipos de diseño de sonido para sublimar estas creaciones.',
    'sound' => 'SOUND <br>
    DESIGN',
    'sounp' => 'El sound design está hecho por nuestros artistas para poner un sonido original en todas sus obras digitales.
    <br> <br>
    Panorama Studio respetará el ambiente musical y el tipo recomendado.',
    'sobno' => 'SOBRE <br>
    NOSOTROS',
    'sobnop' => 'Panorama Studio y sus fundadores, provenientes del universo de la comunicación y el desarrollo 3D, han reunido sus competencias para proponerle un ecosistema único en torno a la imagen digital, especializada en los 3D, el diseño, la animación y las nuevas tecnologías. Nuestra maestría en 2D y 3D nos permitirá hacer realidad su imaginación.',
    'art' => 'ARTISTA DIGITAL <br>
    Y CREADOR DE TECNOLOGÍA<',
    'artp' => 'Panorama Studio está cerca del mundo inmobiliario, de la cultura, de las artes y de la innovación digital para hacer cada vez más realistas, que convencerán a sus clientes. <br>
    Nuestras soluciones utilizan tecnologías escalables. Nuestros equipos experimentados (directores de arte, diseñadores, desarrolladores, diseñadores gráficos, guionistas, diseñadores gráficos, animadores 3D, diseñadores de movimiento, ilustradores y diseñadores de sonido) reúnen las habilidades técnicas y todas las audacias creativas para ofrecerle las mejores historias digitales.',
    'cnos' => 'CONTÁCTE NOS',
    'envie'  => 'ENVIAR',


];