<?php

return [
    'title' => 'Contáctenos',
    'desc' => '
    <span class="font-true-medium"> "El lujo está en cada detalle." <br>
    -- Hubert de Givenchy  </span>
    Entremos en los detalles.  <br>
    Demos forma y color a sus proyectos.  <br>
    Construyamos un legado atemporal.  <br>
    Creemos lo increíble.  <br>
    Póngase en contacto con nosotros para obtener más información.',
    'send' => 'ENVIADO',
    'nom' => 'Nombre (*)',
    'prenom' => 'Apellido (*)',
    'email' => 'Email*',
    'object' => 'Asunto*',
    'message' => 'Mensaje (*)'
];