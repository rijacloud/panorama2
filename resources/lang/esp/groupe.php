<?php

return [
    'pmr_1' => "La alianza de la web, la tecnología 3D y el talento de los diseñadores de interiores.",
    "pmr_2" => "Gracias a un dominio total de estos 3 aspectos y a una pasión asumida por la decoración y el estilo de vida, la empresa Pimp my Room desarrolla con éxito servicios para profesionales y particulares que aprovechan la unión de tecnología y creatividad.",
    "pano_title" => "PANORAMA <br> STUDIO",
    "pano_sub" => "Panorama Studio, la filial de wave.art para los mercados españoles.",
    "pano_desc_1" => "Con sede en Madrid, la agencia ha desarrollado un ecosistema único que combina marketing y comunicación.",
    "pano_desc_2" => "Agencia <br>
    Creativa <br>
    Comunicación <br>
    Digital",
    "pano_desc_3" => "",
    "pano_art" => "PANORAMA <br> ART STUDIO",
    "pano_art_desc" => "Panorama Art Studio, filial de wave.art para los mercados de Oriente Medio /Asia / Estados Unidos.",
    "pano_art_desc_1" => "Con sede en Dubai, esta agencia creativa ofrece tiendas virtuales y otras experiencias virtuales.",
    "pano_art_desc_2" => "Experiencias de marca promocionales <br>
    Experiencias permanentes de venta al por menor <br>
    Ferias y diseño de exposiciones <br>
    Exposiciones virtuales de 360°.",
    "logia_title" => "LOGIA",
    "logia_sub" => "Logia, la agencia creativa con sede en Madagascar",
    "logia_desc" => "Logía ofrece una amplia gama de servicios. Desde la estrategia, los trabajos creativos, la producción y la distribución de campañas tanto offline como online. Estamos presentes en todos los frentes para ofrecer grandes campañas que reflejan su imagen",
    "logia_desc_2" => "Lo que nos hace exclusivos en Logia es nuestra pasión y nuestra visión de luchar por la grandeza. Combinamos creatividad, fervor e innovación para ofrecer a nuestros clientes estrategias impactantes y fuera de lo común.",
    "isart_desc" => "Is'art, centro de arte contemporáneo con sede en Madagascar, es un lugar de exposición y venta de arte.",
    "isart_desc_2" => "Concebido como lugar de residencia de artistas malgaches y extranjeros, Isart es también un escenario local para la difusión de actuaciones musicales y de videoarte. En resumen, es un lugar para la promoción del arte contemporáneo en todas las disciplinas.",
    "isart_desc_3" => "Es un verdadero escenario de encuentro y convivencia abierto a todas las propuestas creativas.",
    'voir' => 'Lee mas',
    'down' => 'Descargue la presentación'

];