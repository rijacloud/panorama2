<?php

return [
    'home' => 'INICIO',
    'about' => 'SOBRE',
    'expertises' => 'Experiencias',
    'realisations' => 'Realizaciones',
    'group' => 'El Grupo',
    'contact' => 'Contacto'
];