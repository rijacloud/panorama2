@extends('template')
@section('title')
{{ __('realisations.title') }}
@stop
@section('description')
@stop
@section('content')
<div class="paris real">
    <div class="bg">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="font-medium h1-about text-white">
                        <div id="text-stories">the stories</div>
                        <div id="text-brought">We brought</div>
                        <div id="text-life">to life</div>
                    </h1>
                </div>
                <div class="col">
    
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="w80 d-flex justify-content-center align-items-center text-justify" id="text-0">
                        <div class="d-bottom">
                            <h2 class="font-barlow text-white h2-about" id="item-text-0">{{__('realisations.title_1')}}
                            </h2>
                            <h2 class="font-barlow text-white h2-about" id="item-text-1">{{__('realisations.title_2')}}  
                                <br>
                                {{__('realisations.title_3')}} </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="expert-list rea">
    <div class="container-fluid">
        <div class="row">
            <div class="list">
                
                <button class="font-regular text-white active" data-tag="all">{!!__('realisations.all')!!}</button>
                <button class="font-regular text-white" data-tag="salons">{!!__('realisations.salons')!!}</button>
                <button class="font-regular text-white" data-tag="animation">{!!__('realisations.animation')!!}</button>
                <button class="font-regular text-white" data-tag="motion">{!!__('realisations.motion')!!}</button>
                <button class="font-regular text-white" data-tag="poesie">{!!__('realisations.poesie')!!}</button>
                <button class="font-regular text-white" data-tag="film">{!!__('realisations.film')!!}</button>
                <button class="font-regular text-white" data-tag="decor">{!!__('realisations.decor')!!}</button>
                <button class="font-regular text-white" data-tag="creation">{!!__('realisations.creation')!!}</button>
                <button class="font-regular text-white" data-tag="jeux">{!!__('realisations.jeux')!!}</button>
                <button class="font-regular text-white" data-tag="application">{!!__('realisations.application')!!}</button>
                <button class="font-regular text-white" data-tag="visite">{!!__('realisations.visite')!!}</button>
                <button class="font-regular text-white" data-tag="bd">{!!__('realisations.bd')!!}</button>
                <button class="font-regular text-white" data-tag="sound">{!!__('realisations.sound')!!}</button>
                
                <div id="filter_selecter">
                    <span class="font-regular text-white">Filtre : </span>
                    <select name="filter" id="filter" onchange="filter(this)">
                        <option value="all">{!!__('realisations.all')!!}</option>
                        <option class="font-regular not_dark" value="salons">{!!__('realisations.salons')!!}</option>
                        <option class="font-regular not_dark" value="animation">{!!__('realisations.animation')!!}</option>
                        <option class="font-regular not_dark" value="motion">{!!__('realisations.motion')!!}</option>
                        <option class="font-regular not_dark" value="poesie">{!!__('realisations.poesie')!!}</option>
                        <option class="font-regular not_dark" value="film">{!!__('realisations.film')!!}</option>
                        <option class="font-regular not_dark" value="decor">{!!__('realisations.decor')!!}</option>
                        <option class="font-regular not_dark" value="creation">{!!__('realisations.creation')!!}</option>
                        <option class="font-regular not_dark" value="jeux">{!!__('realisations.jeux')!!}</option>
                        <option class="font-regular not_dark" value="application">{!!__('realisations.application')!!}</option>
                        <option class="font-regular not_dark" value="visite">{!!__('realisations.visite')!!}</option>
                        <option class="font-regular not_dark" value="bd">{!!__('realisations.bd')!!}</option>
                        <option class="font-regular not_dark" value="sound">{!!__('realisations.sound')!!}</option>
                    </select>
                </div>
                    
            </div>
        </div>
        <div class="range">
            <button class="up"><img src="{{ asset('/images/in.png') }}" /></button>
            <button class="down"><img src="{{ asset('/images/out.png') }}" /></button>
        </div>
        <div class="row" id="plists">

            <div class=" item position-relative visible" id="cg-1" data-tag="all">
                <a href="#showreal" data-fslightbox data-title="Salon">
                    <img data-src="{{ asset('images/tig.png') }}" alt="" class="lazy" id="bg-1">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-60" data-tag="salons">
                <a href="#salon" data-fslightbox data-title="Salon">
                    <img data-src="{{ asset('images/salon.JPG') }}" alt="" class="lazy" id="bg-60">
                </a>
            </div>
            <div class=" item position-relative hidden" id="cg-2" data-tag="jeux">
                <a href="#orlinski" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/orli.JPG') }}" alt="visite virtuelle" class="lazy" id="bg-2">                
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-4" data-tag="poesie">
                <a href="#initial_2" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/Screenshot_5.png') }}" alt="visite virtuelle" class="lazy" id="bg-4">                
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-12" data-tag="creation">
                <a href="{{ asset('images/Kong-Ozuna-Stadium.gif') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/Kong-Ozuna-Stadium.gif') }}" alt="Kong-Ozuna-Stadium" class="lazy" id="bg-32">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-7" data-tag="visite">
                <a href="#immer_1" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/v.png') }}" alt="visite virtuelle" class="lazy" id="bg-7">                
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-8" data-tag="visite">
                <a href="#immer_2" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/v2.png') }}" alt="visite virtuelle" class="lazy" id="bg-8">                
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-9" data-tag="sound">
                <a href="#sound" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/sound.png') }}" alt="sound" class="lazy" id="bg-9">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-10" data-tag="application">
                <a href="{{ asset('images/Everpinkboat.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/Everpinkboat.jpg') }}" alt="Everpinkboat" class="lazy" id="bg-10">    
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-3" data-tag="poesie">
                <a href="#initial_1" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/Screenshot_4.png') }}" alt="visite virtuelle" class="lazy" id="bg-3">
                </a>
            </div>


            <div class=" item position-relative hidden" id="cg-11" data-tag="creation">
                <a href="{{ asset('images/Ozuna-x-Kong---Porto-Rico.gif') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/Ozuna-x-Kong---Porto-Rico.gif') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-11">    
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-6" data-tag="creation">
                <a href="#kong" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/kong.png') }}" alt="visite virtuelle" class="lazy" id="bg-6">                
                </a>
            </div>


            <div class=" item position-relative hidden" id="cg-12" data-tag="decor">
                <a href="#mb" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/mb.png') }}" alt="visite virtuelle" class="lazy" id="bg-5">
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-22" data-tag="animation">
                <a href="#Tete-de-Kong" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Tete de Kong - Orlinski.png') }}" alt="visite virtuelle" class="lazy" id="bg-22">
                </a>
            </div>


            
            <div class=" item position-relative hidden" id="cg-19" data-tag="animation">
                <a href="#Kong-Statue" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Kong Statue.png') }}" alt="visite virtuelle" class="lazy" id="bg-19">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-17" data-tag="animation">
                <a href="#Dynam-Studio-Signature-1" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Dynam Studio Signature (1).png') }}" alt="visite virtuelle" class="lazy" id="bg-17">
                </a>
            </div>

                     
            <div class=" item position-relative hidden" id="cg-18" data-tag="animation">
                <a href="#Dynam-Studio-Signature-2" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Dynam Studio Signature (2).png') }}" alt="visite virtuelle" class="lazy" id="bg-18">
                </a>
            </div>

            

            <div class=" item position-relative hidden" id="cg-12" data-tag="animation">
                <a href="#ORLINDJ" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/ORLINDJ.png') }}" alt="visite virtuelle" class="lazy" id="bg-12">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-13" data-tag="animation">
                <a href="#Orlinski-La" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Orlinski - La Nuit Au Musee.png') }}" alt="visite virtuelle" class="lazy" id="bg-13">
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-14" data-tag="animation">
                <a href="#ORLINSKI-Origine" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/ORLINSKI - Origine.png') }}" alt="visite virtuelle" class="lazy" id="bg-14">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-15" data-tag="animation">
                <a href="#OZUNA-KONG" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/OZUNA & KONG - Voiture 80s.png') }}" alt="visite virtuelle" class="lazy" id="bg-15">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-16" data-tag="animation">
                <a href="#Ozuna-x-Kong" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Ozuna x Kong - No Mans Land.png') }}" alt="visite virtuelle" class="lazy" id="bg-16">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-20" data-tag="animation">
                <a href="#ORLINDJ-3D" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/ORLINDJ 3D.png') }}" alt="visite virtuelle" class="lazy" id="bg-20">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-21" data-tag="animation">
                <a href="#ORLINSKI-ENDGAME" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/ORLINSKI - ENDGAME.png') }}" alt="visite virtuelle" class="lazy" id="bg-21">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-23" data-tag="animation">
                <a href="#Orlinski-PART-I" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Tribute to Richard Orlinski - PART I.png') }}" alt="visite virtuelle" class="lazy" id="bg-23">
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-24" data-tag="animation">
                <a href="#Orlinski-PART-II" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Tribute To Richard Orlinski - Part II.png') }}" alt="visite virtuelle" class="lazy" id="bg-24">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-25" data-tag="film">
                <a href="#Montage_Dynam" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Galilée Montage_Dynam.png') }}" alt="visite virtuelle" class="lazy" id="bg-25">
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-26" data-tag="film">
                <a href="#Highline_20220111" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Highline_202201.png') }}" alt="visite virtuelle" class="lazy" id="bg-26">
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-27" data-tag="film">
                <a href="#Pleyel_1106" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Pleyel_1106.png') }}" alt="visite virtuelle" class="lazy" id="bg-27">
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-28" data-tag="motion">
                <a href="#Rocks_edit" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/Charly Rocks_edit.png') }}" alt="visite virtuelle" class="lazy" id="bg-28">
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-29" data-tag="motion">
                <a href="#Les-Atouts" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/MDP - Les Atouts.png') }}" alt="visite virtuelle" class="lazy" id="bg-29">
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-30" data-tag="motion">
                <a href="#Ventes-Aux-Encheres" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/MDP - Ventes Aux Encheres.png') }}" alt="visite virtuelle" class="lazy" id="bg-30">
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-31" data-tag="bd">
                <a href="{{ asset('images/md/BD/Arche de Noé/page-1.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/Arche de Noé/page-1.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-31">    
                </a>
            </div>
            
            <div class=" item position-relative hidden" id="cg-32" data-tag="bd">
                <a href="{{ asset('images/md/BD/Arche de Noé/page-2.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/Arche de Noé/page-2.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-42">    
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-33" data-tag="bd">
                <a href="{{ asset('images/md/BD/Arche de Noé/page-3.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/Arche de Noé/page-3.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-33">    
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-34" data-tag="bd">
                <a href="{{ asset('images/md/BD/Arche de Noé/page-4.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/Arche de Noé/page-4.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-34">    
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-35" data-tag="bd">
                <a href="{{ asset('images/md/BD/Arche de Noé/page-5.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/Arche de Noé/page-5.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-35">    
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-36" data-tag="bd">
                <a href="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-1.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-1.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-36">    
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-37" data-tag="bd">
                <a href="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-2.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-2.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-37">    
                </a>
            </div>

            <!--div class=" item position-relative hidden" id="cg-38" data-tag="bd all">
                <a href="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-2.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-2.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-38">    
                </a>
            </div-->

            <div class=" item position-relative hidden" id="cg-39" data-tag="bd">
                <a href="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-3.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-3.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-39">    
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-40" data-tag="bd">
                <a href="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-4.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-4.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-40">    
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-41" data-tag="bd">
                <a href="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-5.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/ORLINDJ/ORLINDJ-page-5.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-41">    
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-42" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-1.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-1.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-52">    
                </a>
            </div>
            <div class=" item position-relative hidden" id="cg-43" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-2.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-2.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-43">    
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-44" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-3.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-3.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-44">    
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-45" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-4.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-4.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-45">    
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-46" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-5.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 1/TRIBUTE1-page-5.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-46">    
                </a>
            </div>

            <div class=" item position-relative hidden" id="cg-47" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-1.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-1.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-47">    
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-48" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-2.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-2.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-48">    
                </a>
            </div>

            
            <div class=" item position-relative hidden" id="cg-49" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-3.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-3.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-49">    
                </a>
            </div>

             
            <div class=" item position-relative hidden" id="cg-50" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-4.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-4.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-50">    
                </a>
            </div>

            
             
            <div class=" item position-relative hidden" id="cg-51" data-tag="bd">
                <a href="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-5.jpg') }}" data-fslightbox data-title="Click the right half of the image to move forward.">
                    <img data-src="{{ asset('images/md/BD/TRIBUTE 2/TRIBUTE2-page-5.jpg') }}" alt="Ozuna-x-Kong---Porto-Rico" class="lazy" id="bg-51">    
                </a>
            </div>
        </div>

        <div class="row footer">
            <div class="col" id="col_ft">
                <a href="{{ route('home') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="169.806" height="24.356" viewBox="0 0 169.806 24.356">
                        <g id="Groupe_27" data-name="Groupe 27" transform="translate(-226.21 -458.865)">
                          <path id="Tracé_821" data-name="Tracé 821" d="M463.941,246.639l-8.9,20.379H454.3l-9.517-20.41-2.54,1.184,10.272,22.028h4.36l9.637-22.059Z" transform="translate(-149.918 212.8)" fill="#fff"/>
                          <path id="Tracé_822" data-name="Tracé 822" d="M349.386,246.711l-8.518,20.358h-.961l-7.126-18.178h-4.2l-7.126,18.178h-.961l-8.519-20.358-2.584,1.082,9.238,22.079h4.738l7.126-18.178h.378l7.126,18.178h4.738l9.238-22.079Z" transform="translate(-83.179 212.748)" fill="#fff"/>
                          <path id="Tracé_823" data-name="Tracé 823" d="M403.848,267.635a9.038,9.038,0,1,1,9.038-9.039A9.048,9.048,0,0,1,403.848,267.635Zm9.037-16.671a11.84,11.84,0,1,0,0,15.266V269.9h2.8V247.3h-2.8Z" transform="translate(-124.683 212.726)" fill="#fff"/>
                          <path id="Tracé_824" data-name="Tracé 824" d="M515.142,254.28a12.8,12.8,0,0,1-12.789,12.789,10.389,10.389,0,0,1-10.377-10.376,8.384,8.384,0,0,1,8.375-8.375,6.72,6.72,0,0,1,6.712,6.712,5.34,5.34,0,0,1-5.333,5.334v2.8a8.145,8.145,0,0,0,8.135-8.136,9.526,9.526,0,0,0-9.515-9.515,11.19,11.19,0,0,0-11.177,11.177,13.2,13.2,0,0,0,13.18,13.179,15.61,15.61,0,0,0,15.592-15.592Z" transform="translate(-173.495 213.349)" fill="#fff"/>
                          <path id="Tracé_825" data-name="Tracé 825" d="M546.989,290.492v-2.453c0-.04.029-.06.089-.06h2.483c.058,0,.089.02.089.06v2.453a.079.079,0,0,1-.089.089h-2.483a.1.1,0,0,1-.089-.089Z" transform="translate(-202.54 192.017)" fill="#fff"/>
                          <path id="Tracé_826" data-name="Tracé 826" d="M571.565,274.192a3.01,3.01,0,0,0,.665-1.847h.029l-.029-1.951a1.337,1.337,0,0,1-.71.473,6.491,6.491,0,0,1-1.419.207l-5.32.414a5.059,5.059,0,0,0-2.586.695,1.859,1.859,0,0,0-.7,1.552v.236a2.038,2.038,0,0,0,.961,1.937,6.963,6.963,0,0,0,3.236.546,9.2,9.2,0,0,0,3.7-.649A5.178,5.178,0,0,0,571.565,274.192Zm4.566,3.651c0,.1-.07.147-.207.147h-1.507a2.482,2.482,0,0,1-1.656-.443,1.8,1.8,0,0,1-.5-1.419v-.768h-.207a8.089,8.089,0,0,1-1.182,1.226,7.163,7.163,0,0,1-2.187,1.108,11.594,11.594,0,0,1-3.813.532,7.47,7.47,0,0,1-4.359-.991,3.647,3.647,0,0,1-1.345-3.148v-.354q0-3.4,4.552-3.754l5.764-.443a8.616,8.616,0,0,0,2.054-.369.987.987,0,0,0,.7-1.02v-.03a3.124,3.124,0,0,0-1.182-2.571,6.928,6.928,0,0,0-4.079-.888q-5.32,0-5.32,3.163v.325a.131.131,0,0,1-.149.148H559.52a.131.131,0,0,1-.148-.148v-.325a4.277,4.277,0,0,1,1.729-3.576q1.729-1.332,5.867-1.331t5.852,1.434a5.213,5.213,0,0,1,1.715,4.242l.029,6.827a.921.921,0,0,0,.177.65.972.972,0,0,0,.681.178h.5c.137,0,.207.05.207.147Z" transform="translate(-208.657 204.607)" fill="#fff"/>
                          <path id="Tracé_827" data-name="Tracé 827" d="M611.012,268.236v1.508a.131.131,0,0,1-.148.148h-1.98a.131.131,0,0,1-.148-.148V268.5a3.863,3.863,0,0,0-.975-2.911,4.531,4.531,0,0,0-3.192-.932,4.817,4.817,0,0,0-2.6.665,4.375,4.375,0,0,0-1.6,1.671,4.157,4.157,0,0,0-.532,1.951v8.9a.13.13,0,0,1-.147.147h-2.01a.131.131,0,0,1-.148-.147V263.27a.13.13,0,0,1,.148-.147h2.01a.13.13,0,0,1,.147.147v2.453h.207a5.342,5.342,0,0,1,1.8-1.906,6.377,6.377,0,0,1,3.665-.9,5.668,5.668,0,0,1,4.2,1.331A5.489,5.489,0,0,1,611.012,268.236Z" transform="translate(-227.931 204.607)" fill="#fff"/>
                          <path id="Tracé_828" data-name="Tracé 828" d="M632.677,269.677a3.292,3.292,0,0,0,.5,1.995,1.906,1.906,0,0,0,1.626.665h3.576a.13.13,0,0,1,.147.147v1.449a.13.13,0,0,1-.147.147H634.1a3.6,3.6,0,0,1-2.748-1,4.024,4.024,0,0,1-.946-2.868v-9.251h-3.695a.13.13,0,0,1-.147-.148v-1.448a.13.13,0,0,1,.147-.147H630.4v-3.931a.131.131,0,0,1,.148-.148h1.98a.131.131,0,0,1,.148.148v3.931h5.7a.13.13,0,0,1,.147.147v1.448a.13.13,0,0,1-.147.148h-5.7Z" transform="translate(-242.513 208.516)" fill="#fff"/>
                        </g>
                      </svg>                              
                </a>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col d-flex">
                        <a href="#" class="d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.201" height="15.361" viewBox="0 0 19.201 15.361">
                                <path id="Icon_material-mail" data-name="Icon material-mail" d="M20.281,6H4.92A1.918,1.918,0,0,0,3.01,7.92L3,19.441a1.926,1.926,0,0,0,1.92,1.92H20.281a1.926,1.926,0,0,0,1.92-1.92V7.92A1.926,1.926,0,0,0,20.281,6Zm0,3.84-7.681,4.8L4.92,9.84V7.92l7.681,4.8,7.681-4.8Z" transform="translate(-3 -6)" fill="#fff"/>
                            </svg>
                            <span class="font-barlow text-white">
                                bertrand@wave.art
                            </span>  
                        </a>
                    </div>
                    <div class="col d-flex">
                        <svg xmlns="http://www.w3.org/2000/svg" width="19.157" height="19.158" viewBox="0 0 19.157 19.158">
                            <path id="Icon_awesome-phone-alt" data-name="Icon awesome-phone-alt" d="M18.611,13.538l-4.191-1.8A.9.9,0,0,0,13.373,12l-1.856,2.267a13.869,13.869,0,0,1-6.63-6.63L7.154,5.782a.9.9,0,0,0,.258-1.048L5.616.543A.9.9,0,0,0,4.587.023L.7.921A.9.9,0,0,0,0,1.8,17.36,17.36,0,0,0,17.362,19.158a.9.9,0,0,0,.876-.7l.9-3.891A.909.909,0,0,0,18.611,13.538Z" transform="translate(0 0)" fill="#fff"/>
                        </svg>
                        <div>
                            <a href="tel:+33611945624" class="font-barlow text-white">+33 (0) 6 11 94 56 24</a> <br>
                            <a href="tel:+261324831579" class="font-barlow text-white">+261 (0) 32 48 315 79</a>    
                        </div>  
                    </div>
                    <div class="col d-flex">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13.263" height="19.158" viewBox="0 0 13.263 19.158">
                            <path id="Icon_ionic-ios-pin" data-name="Icon ionic-ios-pin" d="M14.507,3.375A6.41,6.41,0,0,0,7.875,9.532c0,4.79,6.632,13,6.632,13s6.632-8.211,6.632-13A6.41,6.41,0,0,0,14.507,3.375Zm0,8.792a2.16,2.16,0,1,1,2.16-2.16A2.16,2.16,0,0,1,14.507,12.167Z" transform="translate(-7.875 -3.375)" fill="#fff"/>
                        </svg>
                        <span class="font-barlow text-white">
                            120 rue Jean Jaures, 92300 Levallois Perret
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="iframe">
        <iframe
            src="https://player.vimeo.com/video/644256333?h=b1023a687c"
            id="salon"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>
            
        <iframe
            src="https://player.vimeo.com/video/670720624"
            id="showreal"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/665987940"
            id="orlinski"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>
    
        <iframe
            src="https://player.vimeo.com/video/665673121"
            id="initial_1"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>
    
        <iframe
            src="https://player.vimeo.com/video/665672939"
            id="initial_2"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/665672780"
            id="mb"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/665672561"
            id="kong"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/665670528"
            id="immer_1"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        
        <iframe
            src="https://player.vimeo.com/video/665670307"
            id="immer_2"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

                
        <iframe
            src="https://player.vimeo.com/video/665669972"
            id="sound"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669736413"
            id="ORLINDJ"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>
        
        <iframe
            src="https://player.vimeo.com/video/669736463"
            id="Orlinski-La"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669736522"
            id="ORLINSKI-Origine"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669736614"
            id="OZUNA-KONG"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        
        <iframe
            src="https://player.vimeo.com/video/669736648"
            id="Ozuna-x-Kong"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

                
        <iframe
            src="https://player.vimeo.com/video/669737455"
            id="Dynam-Studio-Signature-1"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669737492"
            id="Dynam-Studio-Signature-2"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        
        <iframe
            src="https://player.vimeo.com/video/669737541"
            id="Kong-Statue"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669737615"
            id="ORLINDJ-3D"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        
        <iframe
            src="https://player.vimeo.com/video/669737776"
            id="ORLINSKI-ENDGAME"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669737061"
            id="Tete-de-Kong"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669737252"
            id="Orlinski-PART-I"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669737336"
            id="Orlinski-PART-II"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669739444"
            id="Montage_Dynam"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669739589"
            id="Highline_20220111"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669739745"
            id="Pleyel_1106"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        
        <iframe
            src="https://player.vimeo.com/video/669740319"
            id="Rocks_edit"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

                
        <iframe
            src="https://player.vimeo.com/video/669740193"
            id="Les-Atouts"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>

        <iframe
            src="https://player.vimeo.com/video/669740260"
            id="Ventes-Aux-Encheres"
            width="1920px"
            height="1080px"
            frameBorder="0"
            allow="autoplay; fullscreen"
            allowFullScreen>
        </iframe>
    </div>
    
</div>
@stop

@section('scripts')
<script>
(function() {
    var css = [
        
        {
            link: "https://fonts.googleapis.com/css2?family=Barlow:wght@500&display=swap",
            integrity: "",
            crossorigin: "",
            type: "font",
        },
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css",
            integrity: "sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3",
            crossorigin: "anonymous",
            type: "css"
        },
        
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css",
            integrity: "",
            crossorigin: "",
            type: "font",
        },
        
        {
            link: "css/app.css",
            integrity: "",
            crossorigin: "",
            type: "css"
        }
    ];

    var js = [
        
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js",
            integrity: "sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p",
            crossorigin: "anonymous",
            type: "js"
        },

        {
            link: "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        
        {
            link: "/js/fslightbox.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/gsap.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },

        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollToPlugin.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollTrigger.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        
        {
            link: "animate-realisations.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },

        {
            link: "/js/realisation.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        }
    ];

    var img = getImgs();
    var done = 0;
    var srcs = [...css, ...img, ...js];

    addSCript(done, srcs);

    function getImgs() {
        var imgs = document.querySelectorAll('.background')
        var src = srcs
        var arr = []
        for (let i = 0; i < imgs.length; i++) {
            var url = imgs[i].getAttribute('data-src')
            if(url) {
                arr.push({
                    link: url,
                    id: imgs[i].getAttribute('id'),
                    type: "img"     
                })
            }
        }
        return arr
    }

    function insertAt(array, index, ...elementsArray) {
        array.splice(index, 0, ...elementsArray);
    }

    function updateDone(done) {
        var percent = (done / srcs.length) * 100;
        percent = Math.round((percent + Number.EPSILON) * 100) / 100
        document.getElementById('loading').innerHTML = Math.round(percent)
    }

    function insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    function addSCript(index, srcs) {
        if (index < srcs.length) {
            if(srcs[index].type == "img") {
            var img = document.getElementById(srcs[index].id)
            img.src = srcs[index].link
                done++;
                updateDone(done)
                setTimeout(function() {
                    addSCript(index + 1, srcs)
                }, 100)
            } else {
                var src = srcs[index].type == "js" ? document.createElement('script') : document.createElement('link');                
                if (srcs[index].integrity != "") {
                    src.setAttribute('integrity', srcs[index].integrity)
                }
                if (srcs[index].crossorigin != "") {
                    src.setAttribute('crossorigin', srcs[index].crossorigin)
                }
                if (srcs[index].type == "js") {
                    src.src = srcs[index].link;
                    document.body.append(src);
                } else {
                    src.href = srcs[index].link;
                    src.setAttribute('rel', "stylesheet")
                    document.head.append(src);
                }
                src.onload = function() {
                    done++;
                    if(index === 2) {
                        document.getElementById('loader-text').classList.remove('hidden')
                    }
                    updateDone(done)
                    setTimeout(function() {
                        addSCript(index + 1, srcs)
                    }, 100)
                };
            }
        } else {
            setTimeout(function() {
                document.getElementById('loader').classList.add('hidden')
            }, 150);
        }

    }
})()
</script>
<script>
    (function() {
        document.addEventListener("DOMContentLoaded", function(){
            window.addEventListener('scroll', function() {
                if (window.scrollY > 50) {
                    document.getElementById('navbar_top').classList.add('bg-black');
                } else {
                    document.getElementById('navbar_top').classList.remove('bg-black');
                } 
            });
        }); 
        // DOMContentLoaded  end
    })()
    function filter(obj) {
        
        let Class = obj.value;
        document
            .querySelectorAll(".expert-list .item")
            .forEach(function (item) {
                item.classList.remove("visible");
                item.classList.remove("hidden");
                let tags = item.getAttribute("data-tag").split(" ");
                if (tags.includes(Class)) {
                    item.classList.add("visible");
                } else {
                    item.classList.add("hidden");
                }
            });
    }
</script>
@stop