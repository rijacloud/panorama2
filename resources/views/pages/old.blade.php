<div class="row">
    <div class="col-4 item position-relative" id="cg-1">

        <img data-src="{{ asset('images/a3a1002e7e13048a19252b6fa8d07bbfbbb758c3.jpg') }}" alt="visite virtuelle" class="background" id="bg-1">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-1">visite <br>
                virtuelle</h4>
            <p class="text-white font-barlow" id="text-1b">Visualisez vos espaces à travers des visites virtuelles. Vivez le moment dans des simulations au réalisme a couper le souffle.</p>
        </div>

    </div>
    <div class="col-8 item position-relative" id="cg-2">
        <img data-src="{{ asset('images/Groupe-26.jpg') }}" alt="visite immersive" class="background" id="bg-2">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-2">visite <br>
                immersive</h4>
            <p class="text-white font-barlow" id="text-2b">Immergez vous dans une réalité épique en vous laissant submerger par un mélange complexe de réalité et de virtuel. 
            </p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-8 item position-relative" id="cg-3">
        <img data-src="{{ asset('images/pexels-tima-miroshnichenko-6474473.jpg') }}" alt="visite immersive" class="background" id="bg-3">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-3">application <br>
                de réalité <br>
                immersive</h4>
            <p class="text-white font-barlow" id="text-3b">Offrez vous la possibilité de combiner le virtuel et le réel, sans limites, dans vos applications mobiles. Une opportunité pour incruster vos produits dans des environnements sans demarcations . 
            </p>
        </div>
    </div>
    <div class="col-4 item position-relative" id="cg-4">

        <img data-src="{{ asset('images/Groupe-27.jpg') }}" alt="visite virtuelle" class="background" id="bg-4">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-4">poésie<br>
                virtuelle</h4>
            <p class="text-white font-barlow" id="text-4b">Lorem ipsum dolor sit amet, populo eirmod audire te vim, ea
                natum ceteros
                conclusionemque
                sed, ex vix quodsi latine constituam. </p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-4 item position-relative" id="cg-5">

        <img data-src="{{ asset('images/Groupe-28.jpg') }}" alt="création de salons et show virtuels" class="background" id="bg-5">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-5">création de <br>
                salons et <br>
                show virtuels</h4>
            <p class="text-white font-barlow" id="text-5b">Assurer le succès de vos événements sans se soucier des limites du monde et des frontières.
                Attirez vos prospects et vos clients dans des évènements 100% virtuels pour booster vos ventes.</p>
        </div>

    </div>
    <div class="col-8 item position-relative" id="cg-9">
        <img data-src="{{ asset('images/Screen-Shot-2021-11-08-at-15.00.jpg') }}" alt="films d’animations en 2D et 3D " class="background" id="bg-9">

        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-6">films <br>
                d’animations<br>
                en 2D et 3D </h4>
            <p class="text-white font-barlow" id="text-6b">Grace à notre maitrise dans le domaine de l’animation, nous écrivons vos histoire à travers des oeuvres de niveau international.</p>
                
        </div>
    </div>
</div>


<div class="row">
    <div class="col-8 item position-relative" id="cg-6">
        <img data-src="{{ asset('images/gidro.jpg') }}" alt="création de supports GI" class="background" id="bg-6">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-7">création de <br>
                supports GIF</h4>
            <p class="text-white font-barlow" id="text-7b">Gagnez en visibilité et booster vos ventes grâces à nos petites animation GIF. 
                Idéales pour vos sites web et vos campagnes pubs, et pour dynamiser vos emails.
                L’outil parfait pour les marques dynamiques.</p>
        </div>
    </div>
    <div class="col-4 item position-relative" id="cg-7">

        <img data-src="{{ asset('images/Groupe 30@2x.jpg') }}" alt="Virtual Decor" class="background" id="bg-7">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-8">Virtual <br>
                Decor</h4>
            <p class="text-white font-barlow" id="text-8b">Lorem ipsum dolor sit amet, populo eirmod audire te vim, ea
                natum ceteros
                conclusionemque
                sed, ex vix quodsi latine constituam. </p>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-4 item position-relative" id="cg-8">

        <img data-src="{{ asset('images/manette.jpg') }}" alt="Jeux personnalisés" class="background" id="bg-8">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-9">Jeux <br>
                personnalisés </h4>
            <p class="text-white font-barlow" id="text-9b">Tout est possible, tout se créer. Pour créer des lien forts entre votre marque et vos clients, nos équipe créera et développera des jeux vidéos à votre image. </p>
        </div>

    </div>
    <div class="col-8 item position-relative" id="cg-12">
        <img data-src="{{ asset('images/sound-engineer-in-headphones-listens-composition-2021-08-26-16-26-33-utc.jpg') }}" alt="Sound Design" class="background" id="bg-12">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-10">Sound <br>
                Design</h4>
            <p class="text-white font-barlow" id="text-10b">La conception sonore est un art, et nous en avons fait un métier. Le son reste un element majeur dans l’industrie du cinema et des jeux vidéos. Confiez-nous vos projets de compositions et de creation, et nous donnerons vie a vous histoires. </p>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-8 item position-relative" id="cg-13">
        <img data-src="{{ asset('images/parent.jpg') }}" alt="création de supports GI" class="background" id="bg-13">
        <div class="text">
            <h4 class="font-expanded-bold text-white text-uppercase" id="text-11">Web<br>
                Toons</h4>
            <p class="text-white font-barlow" id="text-11b">Communiquez, c’est oser. La créativité est le principal atout de la communication. Parfois, il est nécessaire de rester simple en étant persuasifs dans nos messages. Plongez vos cibles dans des histories passionnantes grace que webtoons, un belle façon de tisser du lien.  </p>
        </div>
    </div>
    <div class="col-4 item position-relative" id="cg-3">
    </div>
</div>