@extends('template')
@section('title')
{{ __('contact.title') }}
@stop
@section('description')
{{ __('contact.title') }}
@stop
@section('content')
<div class="paris contact">

    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <h1 class="font-medium h1-about text-white">
                    {{ __('contact.title') }}
                </h1>
            </div>
            <div class="col">
                <p class="float-c font-regular">
                    {!! __('contact.desc') !!}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                
                <form action="{{ route('message') }}" method="POST" class="form-c">
                    @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <p>
                            {{ Session::get('success') }}
                        </p>
                    </div>
                    @endif
                    @if(Session::has('errors'))
                    <div class="alert alert-warning" role="warning">
                        <?php
                            $errors = Session::get('errors');
                        ?>
                        
                        <p>
                            {!! implode('', $errors->all('<p>:message</p>')) !!}
                        </p>
                        <?php 
                        ?>
                    </div>
                    @endif
                    @csrf
                    <div class="mb-3">
                        <input type="text" class="form-control" id="nom" placeholder="{{ __('contact.nom') }}" name="nom" required>
                    </div>
                    
                    <div class="mb-3">
                        <input type="text" class="form-control" id="prenom" placeholder="{{ __('contact.prenom') }}" name="prenom" required>
                    </div>
                    <div class="mb-3">
                        <input type="email" class="form-control" id="email" placeholder="{{ __('contact.email') }}" name="email" required>
                    </div>
                    
                    <div class="mb-3">
                        <input type="text" class="form-control" id="objet" placeholder="{{ __('contact.object') }}" name="objet" required>
                    </div>
                    <div class="mb-3">
                        <textarea name="message" id="message" cols="30" rows="10" class="form-control" required></textarea>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="m-btn font-expanded-bold">
                            <span>{{ __('contact.send') }}</span>
                        </button>
                    </div>
                </form>
                <div class="w80 s d-flex justify-content-center align-items-center text-justify">
                    <div class="d-bottom">
                        <div class="d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.202" height="15.361" viewBox="0 0 19.202 15.361">
                                <path id="Icon_material-mail" data-name="Icon material-mail" d="M20.281,6H4.92A1.918,1.918,0,0,0,3.01,7.92L3,19.441a1.926,1.926,0,0,0,1.92,1.92H20.281a1.926,1.926,0,0,0,1.92-1.92V7.92A1.926,1.926,0,0,0,20.281,6Zm0,3.84-7.681,4.8L4.92,9.84V7.92l7.681,4.8,7.681-4.8Z" transform="translate(-3 -6)" fill="#e2051e"/>
                            </svg>
                            <a href="mailto:bertrand@wave.art" class="text-white font-true-medium">
                                bertrand@wave.art
                            </a>
                        </div>
                        <div class="d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.157" height="19.157" viewBox="0 0 19.157 19.157">
                                <path id="Icon_awesome-phone-alt" data-name="Icon awesome-phone-alt" d="M18.611,13.538l-4.191-1.8A.9.9,0,0,0,13.373,12l-1.856,2.267a13.869,13.869,0,0,1-6.63-6.63L7.154,5.782a.9.9,0,0,0,.258-1.048L5.616.543A.9.9,0,0,0,4.587.023L.7.921A.9.9,0,0,0,0,1.8,17.466,17.466,0,0,0,11.275,18.061a16.907,16.907,0,0,0,6.086,1.1.9.9,0,0,0,.876-.7l.9-3.891A.909.909,0,0,0,18.611,13.538Z" transform="translate(0 0)" fill="#e2051e"/>
                            </svg>
                            <div class="d-b">
                                <a href="tel:+33611945624" class="text-white font-true-medium">
                                    +33 (0) 6 11 94 56 24
                                </a>
                                
                                <a href="tel:+261324831579" class="text-white font-true-medium">
                                    +261 (0) 32 48 315 79
                                </a>
                            </div>                   
                        </div>
                        
                        <div class="d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="13.264" height="19.158" viewBox="0 0 13.264 19.158">
                                <path id="Icon_ionic-ios-pin" data-name="Icon ionic-ios-pin" d="M14.507,3.375A6.41,6.41,0,0,0,7.875,9.532c0,4.79,6.632,13,6.632,13s6.632-8.211,6.632-13A6.41,6.41,0,0,0,14.507,3.375Zm0,8.792a2.16,2.16,0,1,1,2.16-2.16A2.16,2.16,0,0,1,14.507,12.167Z" transform="translate(-7.875 -3.375)" fill="#e2051e"/>
                            </svg>                              
                            <div class="d-b">
                                <p class="text-white font-true-medium">
                                    120 rue Jean <br> Jaures, 92300 <br> Levallois Perret
                                </p>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
(function() {
    var css = [
        
        {
            link: "https://fonts.googleapis.com/css2?family=Barlow:wght@500&display=swap",
            integrity: "",
            crossorigin: "",
            type: "font",
        },
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css",
            integrity: "sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3",
            crossorigin: "anonymous",
            type: "css"
        },
        
        {
            link: "css/app.css",
            integrity: "",
            crossorigin: "",
            type: "css"
        }
    ];

    var js = [
        
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js",
            integrity: "sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p",
            crossorigin: "anonymous",
            type: "js"
        },

        {
            link: "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        
        {
            link: "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },

        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/gsap.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },

        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollToPlugin.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollTrigger.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        }
    ];

    var img = getImgs();
    var done = 0;
    var srcs = [...css, ...img, ...js];

    addSCript(done, srcs);

    function getImgs() {
        var imgs = document.querySelectorAll('.background')
        var src = srcs
        var arr = []
        for (let i = 0; i < imgs.length; i++) {
            var url = imgs[i].getAttribute('data-src')
            if(url) {
                arr.push({
                    link: url,
                    id: imgs[i].getAttribute('id'),
                    type: "img"     
                })
            }
        }
        return arr
    }

    function insertAt(array, index, ...elementsArray) {
        array.splice(index, 0, ...elementsArray);
    }

    function updateDone(done) {
        var percent = (done / srcs.length) * 100;
        percent = Math.round((percent + Number.EPSILON) * 100) / 100
        document.getElementById('loading').innerHTML = Math.round(percent)
    }

    function insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    function addSCript(index, srcs) {
        if (index < srcs.length) {
            if(srcs[index].type == "img") {
            var img = document.getElementById(srcs[index].id)
            img.src = srcs[index].link
                done++;
                updateDone(done)
                setTimeout(function() {
                    addSCript(index + 1, srcs)
                }, 100)
            } else {
                var src = srcs[index].type == "js" ? document.createElement('script') : document.createElement('link');                
                if (srcs[index].integrity != "") {
                    src.setAttribute('integrity', srcs[index].integrity)
                }
                if (srcs[index].crossorigin != "") {
                    src.setAttribute('crossorigin', srcs[index].crossorigin)
                }
                if (srcs[index].type == "js") {
                    src.src = srcs[index].link;
                    document.body.append(src);
                } else {
                    src.href = srcs[index].link;
                    src.setAttribute('rel', "stylesheet")
                    document.head.append(src);
                }
                src.onload = function() {
                    done++;
                    if(index === 2) {
                        document.getElementById('loader-text').classList.remove('hidden')
                    }
                    updateDone(done)
                    setTimeout(function() {
                        addSCript(index + 1, srcs)
                    }, 100)
                };
            }
        } else {
            setTimeout(function() {
                document.getElementById('loader').classList.add('hidden')
            }, 150);
        }

    }
})()
</script>
<script>
    (function() {
        document.addEventListener("DOMContentLoaded", function(){
            window.addEventListener('scroll', function() {
                if (window.scrollY > 50) {
                    document.getElementById('navbar_top').classList.add('bg-black');
                } else {
                    document.getElementById('navbar_top').classList.remove('bg-black');
                } 
            });
        }); 
        // DOMContentLoaded  end
    })()
</script>
@stop