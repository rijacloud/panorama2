@extends('template')

@section('title')
Wave.art
@stop
@section('description')
We bring your story to life
@stop
@section('keywords')
ART, Creative, Digital, Technology, Fashion, Gaming, Culture, Media, We bring your story to life
@stop
@section('content')
<h1 style="display: none;">Wave.art We bring your story to life</h1>
<div class="h-animated" id="mainContainer">
    <img data-src="{{ asset('images/pexels-kara-muse-2346120.jpg') }}" alt="" class="imgSlide lazy" id="image-13">
    <img data-src="{{ asset('images/pexels-merlin-lightpainting-9854296.jpg') }}" class="imgSlide lazy" alt="" id="image-12">
    <img data-src="{{ asset('images/pexels-inna-mikitas-7758903.jpg') }}" alt="" class="imgSlide lazy" id="image-11">
    <img data-src="{{ asset('images/pawel-czerwinski-NTYYL9Eb9y8-unsplash.jpg') }}" class="imgSlide lazy" alt="" id="image-10">
    <img data-src="{{ asset('images/pexels-kyle-loftus-3379934.jpg') }}" alt="" class="imgSlide lazy" id="image-9">
    <!-- <img src="{{ asset('images/daria-sheveleva-_sue1D9nFCM-unsplash.jpg') }}" alt="" class="imgSlide lazy" id="image-8"> -->


    <img data-src="{{ asset('images/man-using-vr-headset-for-playing-fighting-game-2021-08-29-19-48-22-utc.jpg') }}" alt="" class="imgSlide lazy" id="image-7">
    <div class="font-expanded-extrabold white gaming text-uppercase textSlide" id="text-slide-7">
        Gaming
    </div>
    <svg width="105vw" height="100vh" class="imgSlide lazy" style="object-fit: contain; margin-left: -15px"
        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="svg-2">
        <defs>
            <mask id="mask2" x="0" y="0" width="100%" height="100%">
                <svg viewBox="0 0 100 100" height="100%" width="100%">
                    <polygon points="50 0, 200 100, 200 300, -100 300, -100 100" transform="translate(0 150)"
                        id="mTriangle" fill="#fff" />
                </svg>
            </mask>
        </defs>
        <foreignObject x="0" y="0" height="100%" width="100%" style="mask: url(#mask2)">
            <img data-src="{{  asset('images/daria-sheveleva-_sue1D9nFCM-unsplash.jpg')  }}" alt="" class="imgSlide lazy"
                id="image-8">

        </foreignObject>
        <foreignObject x="0" y="50vh" height="100%" width="100%" style="mask: url(#mask2)" id="text-slide-8">
            <div class="font-true-medium white culture text-uppercase">
                <div class="text-center">
                    Culture
                </div>
            </div>
        </foreignObject>
    </svg>


    <img data-src="{{ asset('images/pexels-anastasiya-gepp-2036646.jpg') }}" alt="" class="imgSlide lazy" id="image-6">
    <div class="font-expanded-extrabold white fashion text-uppercase textSlide" id="text-slide-6">
        Fashion
    </div>
    <!-- <img src="{{ asset('images/pexels-ron-lach-9785021.jpg') }}" alt="" class="imgSlide lazy" id="image-3"> -->
    <img data-src="{{ asset('images/pexels-ruvim-3622517.jpg') }}" alt="" class="imgSlide lazy" id="image-2">
    <div class="font-condensed-regular white creative text-uppercase textSlide" id="text-slide-2">
        Creative
    </div>
    <svg width="105vw" height="100vh" class="imgSlide lazy" style="object-fit: contain; margin-left: -15px"
        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="svg-1">
        <defs>
            <mask id="mask1" x="0" y="0" width="100%" height="100%">
                <circle cx="100%" cy="100%" r="0%" fill="#fff" id="mCircle" />
                <rect x="50%" y="50%" width="0%" height="0%" fill="#000" id="mRect" />
            </mask>
        </defs>
        <foreignObject x="0" y="0" height="100%" width="100%" style="mask: url(#mask1)">
            <img data-src="{{ asset('images/pexels-ron-lach-9785021.jpg') }}" alt="" class="imgSlide lazy" id="image-3">
        </foreignObject>
        <foreignObject x="0" y="0" height="100%" width="100%" style="mask: url(#mask1)">
            <img data-src="{{ asset('images/note-thanun-GI10ZiPO_3w-unsplash.jpg') }}" alt="" class="imgSlide lazy" id="image-4">
        </foreignObject>
        <!-- <image xlink:href="{{ asset('images/pexels-ruvim-3622517.jpg') }}" x="0" y="0" height="100%" style="min-width: 100%" /> -->
    </svg>

    <img data-src="{{ asset('images/leo-manjarrez--vygi0Cvz_c-unsplash.jpg') }}" alt="" class="imgSlide lazy" id="image-1">
    <div class="font-condensed-bold red art text-uppercase textSlide" id="text-slide-1">
        ART
    </div>
    

    <div class="font-expanded-extrabold white digital text-uppercase textSlide" id="text-slide-3">
        Digital
    </div>
    <div class="textTechnoContainer">
        <div class="font-condensed-light white techno text-uppercase textSlide">
            <div class="text-left textTechno textTechnoContent" id="text-slide-4">
                Techno
            </div>
            <div class="text-left textTechno textTechnoContent" id="text-slide-5">
                logy
            </div>
        </div>
    </div>
    <div class="font-expanded-extrabold white media text-uppercase textSlide" id="text-slide-9">
        Media
    </div>
    <div class="textEndContainer">
        <div class="font-bold white bring text-uppercase textEnd textSlide mx-auto" id="text-end">
            <div class="text-left textEnd textEndContent" id="text-slide-10">
                We
            </div>
            <div class="text-left textEnd textEndContent" id="text-slide-11">
                bring
            </div>
            <div class="text-left textEnd textEndContent" id="text-slide-12">
                your
            </div>
            <div class="text-left textEnd textEndContent" id="text-slide-13">
                story
            </div>
            <div class="text-left textEnd textEndContent" id="text-slide-14">
                to life
            </div>
        </div>
    </div>
    <img data-src="{{ asset('images/leo-manjarrez--vygi0Cvz_c-unsplash.jpg') }}" alt="" class="imgSlide lazy image0"
        id="image-0">
    <div class="font-condensed-bold red art text-uppercase textSlide image0" id="text-slide-0">
        ART
    </div>

    <section class="d-flex justify-content-center align-items-center text-center slides" id="slide-1">


    </section>

    <section class="slides" id="slide-2">


    </section>


    <section class="d-flex justify-content-center align-items-center text-center slides" id="slide-3">

    </section>


    <section class="d-flex justify-content-center align-items-center text-center slides" id="slide-4">


    </section>

    <section class="d-flex justify-content-center align-items-center text-center slides" id="slide-6">

    </section>



    <section class="d-flex justify-content-center align-items-center text-center slides" id="slide-7">


    </section>


    <section class="d-flex justify-content-center align-items-center text-center slides" id="slide-8">


    </section>

    <section class="d-flex justify-content-center align-items-center text-center slides" id="slide-9">


    </section>
    <section class="d-flex justify-content-center align-items-center text-center slides" id="slide-10">

    </section>


</div>
@stop

@section('scripts')
<script>
(function() {
    var css = [
        {
            link: "https://fonts.googleapis.com/css2?family=Barlow:wght@500&display=swap",
            integrity: "",
            crossorigin: "",
            type: "font",
        },
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css",
            integrity: "sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3",
            crossorigin: "anonymous",
            type: "css"
        },
        
        {
            link: "css/app.css",
            integrity: "",
            crossorigin: "",
            type: "css"
        }
    ];

    var js = [
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js",
            integrity: "sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p",
            crossorigin: "anonymous",
            type: "js"
        },

        {
            link: "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },

        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/gsap.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },

        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollToPlugin.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollTrigger.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        {
            link: "animate-index.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        }
    ];

    var img = getImgs()
    var done = 0;

    var srcs = [...css, ...img, ...js];

    addSCript(done, srcs);

    function getImgs() {
        var imgs = document.querySelectorAll('.imgSlide')
        var src = srcs
        var arr = []
        for (let i = 0; i < imgs.length; i++) {
            var url = imgs[i].getAttribute('data-src')
            if(url) {
                arr.push({
                    link: url,
                    id: imgs[i].getAttribute('id'),
                    type: "img"     
                })
            }
        }
        return arr
    }

    function insertAt(array, index, ...elementsArray) {
        array.splice(index, 0, ...elementsArray);
    }

    function updateDone(done) {
        var percent = (done / srcs.length) * 100;
        percent = Math.round((percent + Number.EPSILON) * 100) / 100
        document.getElementById('loading').innerHTML = Math.round(percent)
    }

    function insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    function addSCript(index, srcs) {
        if (index < srcs.length) {
            if(srcs[index].type == "img") {
               var img = document.getElementById(srcs[index].id)
               img.src = srcs[index].link

                done++;
                updateDone(done)
                setTimeout(function() {
                    addSCript(index + 1, srcs)
                }, 100)
            } else {
                var src = srcs[index].type == "js" ? document.createElement('script') : document.createElement('link');                
                if (srcs[index].integrity != "") {
                    src.setAttribute('integrity', srcs[index].integrity)
                }
                if (srcs[index].crossorigin != "") {
                    src.setAttribute('crossorigin', srcs[index].crossorigin)
                }
                if (srcs[index].type == "js") {
                    src.src = srcs[index].link;
                    document.body.append(src);
                } else {
                    src.href = srcs[index].link;
                    src.setAttribute('rel', "stylesheet")
                    document.head.append(src);
                }
                src.onload = function() {
                    done++;
                    if(index === 2) {
                        document.getElementById('loader-text').classList.remove('hidden')
                    }
                    updateDone(done)
                    setTimeout(function() {
                        addSCript(index + 1, srcs)
                    }, 100)
                };
            }
        }

    }
})()
</script>
@stop