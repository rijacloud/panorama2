@extends('template')
@section('title')
PANORAMA STUDIO
@stop
@section('description')
{{-- __('home.about_1') --}}
@stop
@section('content')
<div class="paris" id="home">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <h1 class="house-regular text-white">
                    <span class="text-slider active">
                        {{ __('home.creative') }}
                    </span>
                    <span class="text-slider">
                        {{ __('home.studio') }}

                    </span>
                    <span class="text-slider">
                        {{ __('home.digital') }}

                    </span>

                    <span class="text-slider">
                        <img class="plexel" data-src="/images/panorama/logo-p.png" alt="" style="width: 50%;" id="okok">
                    </span>
                </h1>
            </div>
        </div>
    </div>
</div>

<div class="alignment nav nav-tabs" id="nav-tab" role="tablist">
    <ul>
        <li>
            <a href="?tab=virtual" class="nav-link" id="nav-home-tab" target-tab="virtual" page="Virtual">
                <img class="plexel" data-src="/images/panorama/image2.png" alt="Virtual Media" id="img1">
                <span>
                    {{ __('home.v') }}

                </span>
            </a>
        </li>
        <li>
            <a href="?tab=animados" class="nav-link" id="nav-home-animados" target-tab="animados" page="Animados">
                <img class="plexel" data-src="/images/panorama/image3.png" alt="Virtual Media" id="img2">
                <span>
                    {{ __('home.a') }}

                </span>
            </a>
        </li>
        <li>
            <a href="?tab=realestate" class="nav-link " id="nav-home-realestate" target-tab="realestate" page="Realestate">
                <img class="plexel" data-src="/images/panorama/image4.png" alt="Virtual Media" id="img7">
                <span>
                    {{ __('home.r') }}

                </span>
            </a>
        </li>
        <li>
            <a href="?tab=gaming" class="nav-link " id="nav-home-gaming" target-tab="gaming" page="Gaming">
                <img class="plexel" data-class="plexel" data-src="/images/panorama/image5.png" alt="Virtual Media" id="img6">
                <span>
                    {{ __('home.g') }}
                    
                </span>
            </a>
        </li>
        <li>
            <a href="?tab=nft" class="nav-link " id="nav-home-nft" target-tab="nft" page="NFT">
                <img class="plexel" data-class="plexel" data-src="/images/panorama/image6.png" alt="Virtual Media" id="img3">
                <span>
                    
                    {{ __('home.n') }}

                </span>
            </a>
        </li>
        <li>
            <a href="?tab=motion" class="nav-link " id="nav-home-motion" target-tab="motion" page="Motion">
                <img class="plexel" data-src="/images/panorama/image7.png" alt="Virtual Media" id="img4">
                <span>
                    {{ __('home.m') }}

                </span>
            </a>
        </li>
        <li>
            <a href="?tab=sound" class="nav-link" id="nav-home-sound" target-tab="sound" page="Sound">
                <img class="plexel" data-src="/images/panorama/image59.png" alt="Virtual Media" id="img5">
                <span>
                    {{ __('home.s') }}

                </span>
            </a>
        </li>
    </ul>
</div>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade" id="virtual" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="animados">
            <div class="container-fluid top">
                <div class="row">
                    <div class="col-md-4 sobrenos">
                        <h2>
                    {!! __('home.vm') !!}

                        </h2>
                    </div>
                    <div class="col sobre-p">
                        <p>
                    {!! __('home.vmp') !!}

                        </p>
                    </div>
                </div>
            </div>
            <div class="container-fluid middle">
                <div class="row">
                    <div class="col ferias">
                        <h2>
                    {!! __('home.f') !!}

                        </h2>
                        <p>
                            {!! __('home.fp') !!}
                        </p>
                    </div>
                    <div class="col position-relative blog">
                        <a href="#salon" data-fslightbox data-title="Click the right half of the image to move forward.">
                            
                            <div style="background-image:url(/images/panorama/image16.png)" class="playu">
                                <svg xmlns="http://www.w3.org/2000/svg" width="86.368" height="98.713" viewBox="0 0 86.368 98.713">
                                    <path id="Icon_awesome-play" data-name="Icon awesome-play" d="M81.821,41.388,13.958,1.268C8.444-1.99,0,1.172,0,9.231v80.22a9.246,9.246,0,0,0,13.958,7.962l67.862-40.1a9.244,9.244,0,0,0,0-15.925Z" transform="translate(0 -0.002)" fill="#fff"/>
                                  </svg>
                                  
                            </div>

                        </a>
                    </div>

                </div> 
            </div>

            <div class="container-fluid middle toleft">
                <div class="row">
                    
                    <div class="col position-relative">
                            <div style="background-image:url(/images/panorama/image15.jpg)" class="playu pleft"></div>
                    </div>

                    <div class="col">
                        <h2 class="blue">
                            {!! __('home.sw') !!}

                        </h2>
                        <p>
                            {!! __('home.swp') !!}
                        </p>
                    </div>
                </div> 
            </div>

            <div class="container-fluid middle">
                <div class="row">
                    <div class="col">
                        <h2>
                            {!! __('home.md') !!}

                        </h2>
                        <p>
                            {!! __('home.mdp') !!}

                        </p>
                    </div>
                    <div class="col position-relative">
                        <a href="#initial_1"  data-fslightbox data-title="Click the right half of the image to move forward.">
                            <div style="background-image:url(/images/panorama/image17.png)" class="playu">
                                <svg xmlns="http://www.w3.org/2000/svg" width="86.368" height="98.713" viewBox="0 0 86.368 98.713">
                                    <path id="Icon_awesome-play" data-name="Icon awesome-play" d="M81.821,41.388,13.958,1.268C8.444-1.99,0,1.172,0,9.231v80.22a9.246,9.246,0,0,0,13.958,7.962l67.862-40.1a9.244,9.244,0,0,0,0-15.925Z" transform="translate(0 -0.002)" fill="#fff"/>
                                  </svg>
                                  
                                  
                            </div>
                        </a>
                    </div>

                </div> 
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <a href="#mb" data-fslightbox data-title="Click the right half of the image to move forward.">
                            <img class="plexel fw" data-src="/images/panorama/video.png" alt="" id="azeaze">
                        </a>
                    </div>

                </div> 
            </div>
        </div>
        <div class="footer" id="foot">
            <div class="row">
                <div class="col" id="col_ft">
                    <a href="{{ route('home') }}">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="215" height="85" viewBox="0 0 215 85">
                            <defs>
                              <pattern id="pattern" preserveAspectRatio="xMidYMid slice" width="100%" height="100%" viewBox="0 0 1549 623">
                                <image width="1549" height="623" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABg0AAAJvCAYAAABI/ZFHAAAACXBIWXMAAA7DAAAOwwHHb6hkAAA8A0lEQVR42u3dP64rR3YHYO3FgJOBQwMDZ96FjPEKnHgBswABEzpR5EyYWKngzCAYGATIsUEYZMDAW/DYI1sG6NdSvacrvr73ks3q7qpzvgK+wJ5/uuzqU93n19X9xfV6/QIAAAAAAMCPAAAAAAAACA0AAAAAAAChAQAAAAAAIDQAAAAAAACEBgAAAAAAgNAAAAAAAAAQGgAAAAAAAEIDAAAAAABAaAAAAAAAAAgNAAAAAAAAoQEAAAAAACA0AAAAAAAAhAYAAAAAAIDQAAAAAAAAEBoAANCg0/lyfY/fCQAAQGgAAEAnDf/NdndtdQgeAAAAhAYAAFQKBVoOBGqHC445AACA0AAAQDCQJBgQKAAAAAgNAAB4ERAYwgQAAAChAQCAgMAQJAAAAAgNAAAy8IqhNkc5LuYoAAAgNAAAYFZGv8P8BQAAhAYAANhJYAgRAAAAoQEAAA/yTYKcw+uMAAAAoQEAAHYTGHYhAAAAQgMAAAzjvrE/HIUIAACA0AAAQFDQ7xhes/Sae3+vt/47su7OECAAAABCAwAAQUETAUDPx2H45y8NdwECAACA0AAAYDm9PQXfeyAgVBAgAAAAQgMAgOaazi2PIcgQDsQ8tiPDcQMAAIQGAAAr8dS5IEGAAAAACA0AALJq7TU2dhCYF4IkAABAaAAAkHdXgWMhRDB/AAAAoQEAwNJaeA2Np8SFTnaoAAAAQgMAgBUNHw72NDiB59enUf5ZHBcAAEBoAADQ2NPgfv+EGnuNkWMCAAAIDQAANGmxA+Hn4dVFAACA0AAASGet7xX4RgEd7HoxVwEAAKEBAOBpbrsKEG59Pnz3AAAAEBoAAMICT2pj94HwAAAAEBoAAMICuwowp81xAABAaAAAaKxqpGKOm/MAAIDQAADQSNU4pU8tfPfAcQAAAKEBAICwwO+O8MA5AQAAQgMAAGEBOCfGhw8mAwCA0AAAIMVT1eV/0+9PL4QHAACA0AAA0AytPfaHo+YnzhdhGwAAIDQAADQ//e7YmeNcAgAAhAYAgLAAnEvOLQAAQGgAALRhxQ+6+v0RHjjPAAAAoQEAYHcBOMeWGr4XAgAAQgMAgFYbmX57nHPOPwAAQGgAAAgLwPknPAAAAIQGAIDAAGgqPPDKIgAAEBoAABqUi47T+aIpCXYdAAAAQgMAoAXlSWKNSGjUZrtrKDtwPAAAQGgAAHiKWfMR7Dpw7gIAgNAAANB8rD28Ix1CBAfOYwAAEBoAAJqOmowgOPh5+CYJAAAIDQAAjUaBAaxs5W+ROL8BAEBoAAAIDDQUwblt1wEAAAgNAAANxYljs91pIkKi4EBICAAAQgMAQCNR4xCc785/AAAQGgAALWrkXeeOBQgO1AIAABAaAACaho4DrKW8EkxwAAAAQgMAAM1BoM3gwPdNAABAaAAA5AoLNAShIafz5drocHwAAEBoAAAIDIDkNUK9AAAAoQEAkOQpYscEBAdqBwAACA0AAA1AxwPUDTUEAACEBgCAxp9jAeqHWgIAAEIDAEDDz7EAdaTm2Gx36goAAAgNAIBOm3yOCagp6gsAAAgNAADNPccE1BZ1BgAAhAYAgKae4wFh7A/HplOD0/mi5gAAgNAAABiUZpnAAJhV+Y5A68OxAgAAoQEA2F2gaQcIDtQgAAAQGgCAwECzDlB71CIAABAaAICmnSYdoAapSQAAIDQAAM26xkZ5ZYljA2qR2gQAAEIDAGAJ+8PRE72A4ECNAgAAoQEAZNf4x0cdIxAcCA4AAEBoAABoyDk+INTcXTsajhkAAEIDAEBgoPkG2G2gdgEAgB8BADThZhmn80XjDRAcAACA0AAA0HzTcAMEBwAAIDQAADTdNNqAQMFB+R6DYwYAgNAAAGhPeeWPwAAQGggOAABAaAAAmZWmVdPDdwwAwQEAAAgNAACBgV0GQPjgQJ0DAEBoAABoqmmkATPp5JVr6h0AAEIDAEBgoIEGqHPqHgAAQgMAQCPtoeE934DgAAAAhAYAgAaaphkgOHDcAAAQGgAAGmeaZYDQQC0EAEBoAABompWxPxw1ygDBgeAAAAChAQCgWaZBBqiF6iIAAEIDAECTTGMMUA/VRwAAhAYAgAaZphigLqqRAAAIDQAAjTHNMGAxp/MlQG7gOAIAIDQAAAQGAHYbqJkAAAgNAIAkzTDHDlAr1U0AAIQGAIAmmGMHqJfqJwAAQgMAIH0DbLPdaXoB6qbgAAAAoQEAoPGl2QWsJ8hHkdVSAACEBgDAz8qT+l2O0rBzHAGh65PDri0AAIQGAEDXgYEnYwHBgeAAAAChAQAgMBAYAEKDmcb+cFRfAQAQGgCAJpfQAEBwoL4CACA0AADNLQ0tAHVVnQUAQGgAABpbmlkA6qtaCwCA0AAANLQ0sQDUWDUXAAChAQBoZmlgAai16i4AAEIDANDE0rgCUG/VXwAAhAYAoIGlaQWg7k4dm+1ODQYAQGgAAL0rTR6BAYDQ4OlxOl/UYgAAhAYA0KvS3BEaAAgO1GIAAIQGAIAmFYAarCYDACA0AAA0qADUYnUZAAChAQCgMQWgHqvPAAAIDQCAqA0qxxVQlxscm+1OjQYAQGgAAK0qzRuBAYDQQJ0GAEBoAABoRgGoz2o1AABCAwAg4DidLxpRgBotOAAAAKEBAGhGaUABcZQQVHAAAABCAwAQGEwZPq4JqNf9jf3hqHYDACA0AIC1BP3wsadVATVb/QYAAKEBAHhqVdMJULfVcAAAEBoAgMaTZhOgdqvlAAAgNAAATSeNJkANjzd83wAAAKEBAGg2PT1O54smE6COC4EBAEBoAACaTRpMQHzlCXzBAQAACA0AQGDw1thsd5pLgHouOAAAAKEBAGgwaSoBanrU4dVzAAAIDQBAc0loAKC2q/EAAAgNAEBjyVOoAGq74AAAAKEBAGgqaSQB3KV8xyXVKB+BdvwBABAaAIDAQGgAYLeBeg8AgNAAACYpT2NqIAEIDdR9AAAQGgCA5hGAeq/2AwCA0AAAEo3yTm/HHFDzhQYAACA0AADNI8ccUPcFBwAAIDQAAM0jDSMAwYFjDwCA0AAANI2GUT727LgDCA0AAEBoAAAaRo47gNDAegAAgNAAADSMNIkArAPWBAAAhAYAoFE0DK8mArAWCA0AABAaAIAmkeYQwIjNdic2MA8AABAaAIDGEAB2GwzDLjQAAIQGAGgOaQoBIDQQKgMAIDQAwGsoNIQAEBpYJwAAEBoAQPbGkOMPYH2wTgAAIDQAgOwNIa8mArBGCA4AABAaAEBxOl80gQAQGrwzymv8zAcAAIQGAGgGCQ0ArBWGNQMAAKEBAJpAnhoFQGggOAAAQGgAgCaQxg8A1gtrBwAAQgMANIA0fgB4oXw03rB+AAAgNABA80fTBwBh88tR1lFzAgAAoQEAGj8CAwBrh2EdAQBAaACApo9mD4D1w7CWAAAgNABA00ejBwBriLUEAAChAQCaPQHHZrvT6AGwjggOAAAQGgCQ1+l80dbR4AEQGlhTAAAQGgCARo8GD8Dz9oejFcS6AgCA0AAAgYHmDgDWlNdG2c1nbgAACA0AQIOnt1GekjUnAKwpAmkAAIQGAGjuGOYEgHWl/thsd9YYAAChAQBo7ggNAHLxXQNrDAAAQgMABAYaOgBYX+w2AABAaABAr8pHGY0Xw/cMAIQGwmkAAIQGAGjoGBo5ANYY6w0AAEIDAHLyvmlNHADrjPUGAAChAQB4AlQTB8BaY80BAEBoAAA/KR9hNG6Gj1MCCA2EBgAACA0A0MQxNG8ArDfWHgAAhAYAaOAYGjcAc7GzzdoDAIDQAAChgcYNANYd6w8AAEIDADRuNG0AsPZYfwAAEBoAoHHT+TidL5o2ANaeVUZ5jZO5AgAgNAAATRtPeQJYfwzrEACA0AAANG00awAS2B+OVhi7DQAAhAYAIDAQGgBgHbIWAQDgRwBAs0ajBgDrkN0GAAAIDQDQqOlx+AgygLVIiA0AgNAAAI0aQ4MGwFokyAYAQGgAgCaNITQAWIqPIVuXAACEBgAgNNCcAcCaZF0CABAa+BEAaEH5mKKhOQMgNLA2AQAgNAAA457h3dEA1iWhAQAAQgMA7DIwNGUAhAbWKAAAhAYAaMwYGjIA1iZrFAAAQgMAEtgfjlosGjIAzSmvgzMeGGXnoPkDACA0AABPcgoNAKxRhnUKAEBoAACe4tSMARAaGGWUtd3cAQAQGgCAZoxGDIB1yhBwAwAIDQBAM0YTBsA6ZVivAACEBgCgEaMJA2CtMqxZAABCAwDQiNGAAQjNt3esWQAAQgMAWMBmu9NK0YABEHIHHvvD0boFACA0AAANGKEBgDXLsG4BAAgNAMCrHjyxCSA0MIQGAABCAwDQfJl/lLDF/AGwbgkOAAAQGgCg+WKYOwDWLesXAABCAwA0XgxNF4DVbLY7K5D1CwBAaAAAQgNNFwB8i8caBgAgNAAATRcNFwCE3tYwAAChAQBouGi4AGANqzv2h6N1DABAaAAAGi61RnmftjkEYA0TfgMAIDQAoH8+IvncKK92MpcAhAZCAwAAhAYAaLYY5hCAdazvYdccAIDQAAA0W4QGANYxw3oGACA0AACNFk0WgDjKa+IM6xkAgNAAAIQGmiwAQgOhQY3hFUUAAEIDADRZdEiEBgBCcMOaBgAgNAAADRYNFgBrmmFNAwAQGgCABku1sT8cNVgArGlhhlcUAQAIDQBIqjS7jSdHecWTOQUgNLDbAAAAoQEAmiuGuQRgXbO2AQAgNABAc8XQWAGwrgUcXlEEACA0ACAZryYSGgBY2wzrGwCA0AAAPI2pqQIQUvnGjGF9AwAQGgCA0EBTBQDrW81RQhhzCgBAaACAJzENoQGA0MCwxgEACA0A0FAxNFQArHGGNQ4AQGgAgIaK8fjw6gYAa5zQAAAAoQEAGiqG0ADAGic4AABAaACAZoqhkQJgnbPWAQAgNABAM8XQSAGwzlnrAAAQGgCgmWJopAC0b7PdWZkqD6/iAwAQGgCgkWIIDQC6VBrchvUOAEBoAAB3MDRRAIQGhvUOAEBoAABCA00UAOudYb0DABAaAIAmiiYKgPXOeGLsD0drHgCA0AAADRRDaABgzTOseQAAQgMANFCMd0d5b7a5BWDNExoAACA0AEADRWggNACw5gkNAAAQGgDQmdLcNjRPAIQGhrAcAEBoAACG0ADAumdY+wAAhAYAoHmicQJg3TOsfQAAQgMA0DzROAHIyGv5rH0AAEIDABAaaJwAIDSw9gEACA0AQGCgcQKA0MD6BwAgNAAAoYGmCQDWP+sfAIDQAJj61NlLfhM0TQxNEwDrn2H9w70eAK/XVb+H0ABS3TjuD0eFD00TQ9MEwPpnWP+Id777XQDUVaEBMPnG0W9DWJvtTltjpiF0BBAaZB2eOMS9HkDsHQbugYUGXW0vrPFBs2GC21qTTylsLiRxA2VomABYA42FxvDwglcbsPK57vcBUFeFBi6sNYGYPLf9Rm7qu5gXNcJTw3oB4PrCaC1cMF+Z41x3vaYe6wU4JggNhAYurGcZtt4IDRAaCA0MdQPA9YUhRMC9HvHqceKej/ssRt35imO/VbbQoNVm1MtXCt3zpMDLf+87r6Vx8argKXga4T3ODcPcAMB6OcuY8jqhWq+Std4y03nud+rjntx1vTXS7iB1lYZ3GrRYgJpsUipkIeei38rFT6qw0K4FNxcArgmMue9vKjYLzWkmnece/lPb1Re/IUIDoYHQYPGLWRcgQgNcAAkNhAYACA2EBosdE3ObKffwfjO1/dWR8BVF7o2oMTf8XkKDHKHBs0004UH/89D3K1wAZVsQhQbmBJjjRGadazI0qHK95rodzS3H3bW90AB1FaFBV//8XlvU/Tz0m7kASnNua6a4MAZzHKGBsda1TqXjY65bw8yXxMe98rcS0v1+Nb8Bal7279H54OFpoUHm0ODZv8Pk7azgOW5Cg2zzQzPFfABzHKGB0cADEtZjlpo3frd4x77a/V+iBugsv595qa4iNMgYGggO8jWP/XZCgxTzQzPFXABzHNcFRgOhgeCAReaM11ppeqslQgOEBkIDoUFrf4+JLDRAaCA0EBqA0ACEBkIDwQHu9ajY9PaKoqdeTSQ04IsK55HfUGiQOzSo8K43k1nIw8LHvvIFZLg5IjQwD8AcxzWhESg0ULec1+ZIziflvaKogd/OvFRXERpk3mng6Zdc889vGODYz9UUj7K1WWigToA5jmtCo6HQQHCAez3WbnyHnR8jD8IKDahSVxu5fhAaCA1coOA48XBoMFfDQGiQeCT6UBpCAxAaCA16vDYx971Cw/xIFBpUeDNEhvnx2t/od8PxFxoIDZxMCp6GYNrQQHAgNAjXWAGhAQgNgq5trtNwP85qze/AvQChAeqq0EBo4IRS8Bwjx3+J0KD3xrHQQGgA1kFcFxqtrW0Vnxg2/53T5ofmt/nx9quJnFPJ1VpzPXgrNLCYV/zbNJyav8lwfOKFBnYbCA2EBiA0wBw3+ljbXM8z9/ns9wwWGugFTApa1FucN0IDF3qN/n0mePs3hn7TQKFB5QvJ7ueJ0EBtAPMc14aG0ICM57OnYjXAk9UPoQFCA6GB0MDfp+ApeubAOze/GshCA7UBhAaY40bDoUHlaxTnQQAzXbf6bTXAw8+PkY+H623x2twwD4QGmuot/X1ecdH2TaHjEzI0EBwIDVwMgdAAc9xo/7rXmo3rdx4NDdQOvxfqqtBAaGALD4vdFPpthQbvjvLqI6GB0ACEBiA0EBo0dOy8hsa5bK0TGiSfH0IDFqurPfZFhAZCA7sNFDyLn9BAqi40UBdAaIA5biQLDdQzr9AwN4QGWefHHa8mEho4j9RVoYHQwIVsuoLn6SRz4Z6b39QLpNBAzQbzHM1GQ2hAb2tVzfPaA3whQwONcL8VD8yNGa6V/L5CA6GBk6rNY/FiO5TjIzQQGggNhAYgNCAxa12/zdOax05jOGTj03Wd+bBEI/zqt3IeRVN6Zuqq0EBo0Eto4Gn2WbbcKXhCg+uS53Fv80UjRU0A8xyhgZEhNFDTwu0od69nTggN6ryaSGjgHFJXhQZCg9ZDAyfVLAVP0RMavHvcRlL2NPNFI0U9APMcoYHRamjgWp7r+Cs0qp/bHuALGRpohvudeHxuWHOFBkIDoUGqiwbHR2iwxrksNBAagNAArHVCA6EBdc9f84M1muFXv5NzKPC1v/NGaCA0EBrkLHg1P+jy4lsJxAsNUgYHGinqNZjnCA0MoQGaW+aHZni4+fFp3LGTxjmkrjpvhAZCg9ZvQnyga7bFUNETGqwSGrR+TmukqAVgnmOeG0IDOr+uN0eEBhrifiMqzY2ar2/26jehgdBAI6qHxdDxcXNht4HQQK0GoQHmuSE0oBMrfMTVHNEQjzQ/hAZM2WWgrgoNhAZCg3QFT9ETGggOhAZqNQgNMM8NoQHB7vW8jlZooIY89WoioUHSc+edemftFRoIDYQGaQqeoic0EBoIDdRqEBpgnhtCAzSGzZNccyNjU9zvwz2vHVJXhQZCA6FB+u2qCp7QoJXmgtBAaABCAxAaCA2EBtTdUW6eCA3MD6EB6qrQQGggNGBywVP0hAarntstfgRIaKAGgHmOeW4IDWj5uD9wDR32mp3nmuI1X2Hl3FFjnTfmhtBAaCA0iJeeu+kQGthtIDRQA0BogHludBIa1LxO8a760K/QcK+nF7DY/Oiglqx57pibsR66VVeFBkIDoUGegjdyAeoJFKFB6uBAaBCjqQJCAxAaCA3cY2luuScXGpgfQgPeO8YP9rLUVaGB0KCVhpsnYGYveIqe0GD1JkNLN+RCA6EBWOcwz41W1zevRnC+rnyuOxaB5kiSejK1t+Sccc548FZoIDRo/QZEI2pSk9OFJHOGBqF3GwgN1GqwzmGeGw2vb67bkxlp7Joz1AyWqoyGH/Zc9XcxL8Pt3lJXhQZCg8ZuQEz4+Queoic0EBwIDYQGIDTAPDfyhAbOgST3ejWfJvcWAKFBh3VFaEDNHSj6Z0IDF3lCg5QNYcfMHBEaCA2EBiA0wDw3hAbEeTWRez3zZLFXrQRrDDtf7N5SV4UGLvKEBgpe7SdQNA/ThAYhgwOhgfMeXJdgnhuRQwNrdrod5ZpbQoNF5keD72df/TcxL9VV80RooPA4cSIUPMdOaNBEg33tbc9CAw0IsL5hnhstrm8Vnwg2/zs8Tys0ZN3rCQ3SzI2GvvtobsbcveXVb0IDoYGLkTwFz7ETGjTWdBAaaKqA0ACEBta3+sfM3O/ESEjkXo9Z+gJBX1H07D+Tc8VDt+qq0EBoUFPNpK3BrW3hC17NiwXHL1VoECo4EBoIDcAFPea5ETE0cH2uuaW5JTSYe340VGeEBsy9e0tdFRoIDZwwqQqeYyg0WD0wFBp0O5xnCA1AaCA0aPv6xLzPu6PcvZ75kmZuVNql4zxxnizaOxDsCw2EBncO7/Na5B19LiSFBnYbCA2c7yA0wDw3OggNrNO5jDyQ416PJZqhUeaG34Ildm+pq0IDoYG/L13BU/SEBqmDA6GBcx3MdcxzI1hoYL6713vtCezJw1OxQoOG54bQgCV7Iu6phQaa6opqiu2qip4FUmggNHCug7mOeW60FRpYn93rudejp0b5anOj4gfEnSN2b6mrQgON9Zb+Nh/SbKPg1Xw/vVdNpQsNug8OhAYubsBcxzw3Wrk/efLJcPPcLgPNLaFBtp6V3wF1FaGBv0vBm/l3U/SEBs003ZcMn4QGznMw1zHPjYYeavLQTuJzc8Zj6RpQaBB1bggNWHr3lgdvhQaa6wv8XSZ2IwXPhaTQoNFGhNBAaABCAxAaZAoNrMfJVHy1ins9PYJ0c6PyGxmcHx66VVeFBkIDgYGCN3fj1EeyUoYG3QYHQgN1HMx1zHOjgdDgoeHJRPd6a53/XjEc8sHCXu8P/Aa8eSxn7k25txYaKDwz/E0mdJsFT9ETGtR+UkpoIDQAoQEIDYQG9a9DzGeNX/d6tNgwv/oNzM01jaypPfURHEOhQfeTx0kQc7uqC0mhQdrdBkID5ziY65jnRuvXTXby2mXgXg+hwWyvJhIaqKvqqtBAaLDW32L7bDcFT9ETGqQLDoQGzm8w1zHPjaWumyZ8PNHctaO8uV3BQqyQu1R6u0fI/vfTRh/E/bXQQOEx8VNsV1X0LJZCA8P5jfXWXEdoIDj4cG3wjD/8279fp15feF+8ez33enTeNL8m//vNzZXMsPtEXRUaCA0EBgrek08/vTrsMEkbGnQVHAgN1Hcw1zHPDespK5+Pmlus2jSv2QdYsmdRcdeL80JdVVeFBkKDhf75Tdp+C56iJzRoshE/198hNHBug7mOeW60NOw2yHU+rvCgletBoUHv/avMfztt7d6qGrh59ZvQoJfC4wI3acFzISk0aLw5ITQQGoDQAIQG1ly6NfJtAfd6RGucXxP/7eZmvodu1VWhQY7CMzUhExbEKngzNFEd05yhQRfBgdDAeQ3mOua5sfR10xNPJprL7vWaeSrW62hjhQadvKLo06j8VLZ7pCDnw0pP67vPFhqk2d5lMuferqroCQ3mfJJKaCA0AKEBCA3ShwbPHhsPbWn2utczj2Y6Bt2EBkl7d7x/P6+uCg3yXpzXuEB8pknmaYLw21UVPKFBqt0GQgPnNZjrmOdGI9dNDw/vP7bLwL2euZRsbggNUFcRGrh4VvAaKBaKntCgyVpY8wZZaOCcBnMd89zo/brJ/ZpXaDxiht3Ajm2Q0KDy3Ojp/HEuOBeauc6ypgsNmh52Eyh4QgOhQQe1UGggNAChAQgNoj4sZX12r+dej7XmUqv1tPm/2bxc1sh3ONRVoUHKi/NPza3aTwVIsBS8B/55Jg9bp9OHBs0GB0IDFzFgrmOeG43eG1mj7SjX3KLnBrrQAHXVXBIaKDwELXiKntCgmjma82t/40VoAEIDEBoIDVo8bh7YcX3uXk9oMFWjryj6NGZ6M4bzwHnQ1LljHRcaKDw0XfBcSLop6aRhITQQGoDQAIQGQgM1zI5y93p6B43NjYoN/i7+XvMy/UO36qrQQOEhVcFT9IQGoYMDoYFzGY1Fw/nt3DYavm4SHLjXWzLMmDw8FSs0mLkGCQ1YeveJ/pnQQGiAgqfoCQ2WVPu7LEIDTUU0Fg3nN87t4KGB4ECD170ea8ypVubFEr0S50BHRnoK6qrQQGiAgtd7wfPhbaHBjI0LoYGmIhqLhvMb57bQYGR4CtwuA80tocFac6NCoz9TQIK6ak4JDUwSUt/EOeZCg+rzfurFqNDAeYzGouH8dm4bHVw32W3g3LOG0GsjXWiAurpe6CY0EBqg4LmQJHdoMHVuCQ2cx2gsGs5v57aRJDRwfjv3rCFCg956Wln+Tu5U81ss6qrQQGiAi8jGhu3RQoOWggOhgXUJ65Lh/HZuG1mum7wq1HmX/BwSGqw0h5/oAQgNcD3jmAsNTBAFr4NtR4qe0KCHRV9o4MIFatYhvw9usjU87Tag9cauez1zK9y8GLlP07tDXUVoYHIoeAoeQoNl5pfQwDkM5jrmuSE0oLaRV2i416PXPsJa8yLD38hzx0ldRWhgAih4ih6BQ4NVgwOhgfMXzHXMcyPhwxbOiQWPVUevZ60yvI5WaFBpXggN6Pmh29rfX3D8hQYmh4InNCBfaFBezSU0EBqA0ACEBkID95fu9dzrEbOhfvc/68j9odqa3ApzQl0VGig8KHhrNXc7+IaD0CDBbgOhgXUJzHXMc6OX66aK1y3OCTvKNbeEBi33tqL/feTYvaWuCg0UHlIVPEVPaNBNQ+Oev11o4NwFcx3z3OjsuinL36mh616PYE31B3ofQgNC1FUP3goNFB5cSJrbQoNOdxsIDZy7YK5jnhvZQgP1bdHdIO71EBo88M+70lsZzHm7t9RVoYELOhQ8RU9oIDgQGjhvQWiAeW4IDXCvd6vmhzs9FRvuIcSlak/kv40nj0+ndcV9uNBA4SFFQ1fRM8d6bGwIDXI2VUBoAEIDoYH6Zke5ez3zrK95ITRAXTXPhAYmhIKn4JnfQoN1dxsIDYQGYI3CPDeEBtQw8pS+emFehOkp1NyF0uA5ZL7bvaWuCg1c0KHgKXpCg4zNjdc+uiU0EBqA9Qnz3BAaUPvYPPDB19DzLMDvIDRYtv5E/bvI/dDtIqGb0EBogIInNCBFaDBTM19oIDQAoQHmuSE0UN/c67nXM9fanBdCAz5Z6aPY6qrQQOHBdtUZCrgnUIQG3b2mSGggNADXYJjnRsbQwFpuR7nmltCgQk+j2rx4+d+9Qn/AXLd7S10VGggNUPAUPaFB5ibH7e8hNNBoAGsT5rnR0/pWq2lnLbfLwL2e+dZYnyvi34S6OkvvwYO3QgOFR8FzIYnQYJ4hNNBoAKEB5rnR5fpW+drFuTHPMVE3zDWhwePzQmhA5N1b6qrQQOEhXcFT9IQGXQcHQgOhAViXMM8NoQHu9T5X83W05b/LXAkQGtScF9c2Xk2kd6enoX8mNBAaoOApeuabeSY0EBqA0ADz3Oh+fXPtrYHrXs+cW/N3rT0nov09qKvqqtBA4UHBq/kxJI1IocHCzQ6hgXMVhAaY54bQgJr3QmqH+SY0eHxeCA3I8qYOdVVooPCQquApekKDLhsew9ZaoYFzFcxzzHNDaECNYxH4FTyuH4UGc76i6NNY8eOu5rf57cFboYHCg4LnQlJo0Ks5GvxzXfAKDUBoAEIDoYH7TPd66oc5F3jeRbq/ML8bMXJ/rq6ad0IDE8B21WCFvcenC4QGmh6GtQk33uY5bk6FBu4zsaNcc0toIDQwv1c+FsF7Q+qq0MAEIFUzXNETGmh8CA1AaADWTqGB+0yNWzXEvMsx96qNlV/vZW6b292+RcGDt0IDhUPBcyGJ0GCGdwAazlMwzzHPDaEBj0r0Cg33evoMGeaFuW33lvNHaKDwoOApekIDc81wjoLQAPPcEBo4L9zrPRGSTB6eihUaCA1I3MfQPxMaKDyk22Wg6FlsNUCEBiA0AGum0MB9pns9dcTcyzX/nh4rv5pITTWv1VWhgcLDqu8nS/F313xtTAMXDkIDDRDD2oSLdb8P1kyhgftM3rvnUUfMPaFB3/PBvLZ7S10VGig8KHiKntDAfDOcnyA0IPXDLEbnoUGtV8Ukef3DrOtGwgedXEsKDYQG5rRdBh68FRooPCh4LiSFBhohhvMToQFYK4UGEY+p0MC9nns9c7CBeaF3Z/fWVV01B4UGDrqC58b0oeEjWUKDFRZZw9qEC3XzHNdmQoMU1z9CAzvKK9zrmn9CA6GB68Zqv3/SHpB7dKGBg67gKXrmv9BAcCA0AKEBCA2EBkIDzVrX3dZZoYHQAHVVXRUaOOAKnoJn/gsNunnqyXBuYs0yzxEaCA3ca3Jr5FsS1k3zT8/huXmhnuI3VFeFBg64gue3cA4IDew2EBqA0AAE60ID95p2lAcLTyYPr6MVGqin5rLfT/9MaOCAK3iKnnNAaCA4EBqA0ACsjUID95ru9dQVv2WwuTgxoFZP8fupq0IDBzvl1m9PtVUa5WkW80xooDEiNAChAVgbhQZ1XzvlHJl2b6OuuKbUaH1uXujd4fdTV4UGDraC5zdxHggNNEeEBiA0AOui0KDBY+scefC39kCTez2hgdDAPLbLYIFwevLI9uo3oQEKngtJv6fQYOkn7wznJNYqvw+ux4QGQgPc67nXMx/rNz317jTI/XbqqtDAgVbwFD3ngtBAg0RoAEIDsCYKDVo7tj5Ea0f5Qg3iN4fdG6FCrB7vI9wfrfibW4f0z4QGDrTtqn4b54LQQJNEYwWEBmA9tLbZbaBBq76Yh0IDvTtz2O+mrgoNHGQFz2/jXBAaaJJorIDQAKyH1jahQV/KA2F+K/d6+g/TzxmhAXZvqatCA4XHCe14KXpCA00SjRUQGoD10Nq2lBqviLFT2is0pprhW2J+1xgPLfZ2jM1f8zfkdVuWe3ahAQqeouc3FhpMuon+1a9/owuisQJCA8xxI/La5hrbvZ57PfPStZXeXY9Bo99FXRUaOMC2q2ZW8yNZnvARGqz45JNhfUJoAEIDoUHUax7nih3lmltCg1lfUdTwvbz5q66qq0IDhQfbVRU9oYF5ZzgXQWiAOW7ECQ0qHmfni2tq93pCgznnhN4dfq8ngrcpI0OvUmiAgudC0m/tBkejRGgAQgOwFgoNZjrWrhvd67nXMzeFBubuXEbeQOF3UVeFBg6ugoeiJzTQKBEagNAArIVCgw6O97P/u16h4d74qeHD3KECrU+j8aed3Rst/Ds7z/XPhAYOroKHoic00CgRGoDQAKyFQoPYwUHE2mktcK9nfub85zRvzQd1VWgwLvKHMzUP27+JSvBhIOdC37Wnu3P89nf91a9/oyPiwgOEBqQR+d7GGjnv9ft79yWvvW+5sYep3IPkDBbD78boqBcx23HRu3NN0er5HnRuCg2CLphufvs7kf1OOc8Fv5m67jwCoQG4jtP06fn4m/vu9Zx77vc++7sXCCTcZyWdw42e7+aj0MCBdhPlQtK54CKy14agHQfWFBAa4HrXyNLIeG1XQOSmjXs9oYH7vXZCA707oYHQwHyMEhr8uHBG5Map/WPtd/I7+92WWbT//C//RjdDaABCA7yK03iqUdHjdVPlAMG9nnsQ9yxBfmv9HMdCXTU3e60PLvgB0DARGoDQAKyB1sCZmxtjwYhzCQAQGgCQ7tUMf/FXf6v9ITRAU9UcR2hg3LXLwNwCABAaAKBpYmioIjQA65+hNgAACA0A0DQxPGmJ0ACsf4baAAAgNABA08QQGiA0AOufoTYAAAgNANA0Md4e+8NR4wShAVj/BOYAAAgNANA4MTRVERqAtc+6BwCA0AAAjRND8wShAVj7rHsAAAgNANA4MTRPEBqAtc+6BwCA0AAAjRND8wShAXSkvHvfqDh8ywcAQGgAgOaJITRAaADWPUNNAAAQGgCQsDloaKAACA0Max4AgNAAAKGBoYECYM0zrHkAAEIDADRQDA0UAGue8c4oOzfMKwAAoQEAkXhVg9AAQGhgWO8AAIQGAKCJ4slLAOudITQAABAaAIAmitAAwHpnTB+b7c5aBwAgNABAE8XQSAGw3hl2GQAACA0A0EgxNFMArHWGdQ4AQGgAgEaKoZkCYK0zrHMAAEIDADRSDM0UgBDK92YMaxwAgNAAAO5R3sNvaKgACA0MaxwAgNAAAOw20FABEI4b1jgAAKEBAAgNNFQArHHGHWN/OFrjAACEBgBoqBhCAwBrnGF9AwAQGgCgqWJMGuXd2eYTgPVNaAAAgNAAAE0VoYHQAMD6FmuU70KYSwAAQgMANFUMT2MCWN8M8wgAQGgAQDrlA4eG5gqA0MCwrgEACA0AQGNFcwXA2mb8cng1EQCA0AAAjRVDaAAQQvnOjGFNAwAQGgDAFOVpQkODBcC6ZljTAACEBgBgt4EGC4A1zfhpeDURAIDQAAA0WIQGANY0w3oGACA0AACvcqg1yju0zScAoYHQAAAAoQEAmizZh9c5AFjPeh/7w9FaBgAgNAAATRZPZwJYzwzrGACA0AAAXvCKIs0WAKGBdQwAAKEBAGi0aLYAhFC+L2NMGL7NAwAgNAAAoYHQAMCuOcMaBgAgNAAAzRYNFwDht2ENAwAQGgCAhouGC4A1zLB+AQAIDQBAw2Xe4X3QANYwoQEAAEIDAELZH45aJxovAEKDJKO8mtDcAQAQGgCApovQAMD6ZZg3AABCAwDQdNF8AQimvCLOsG4BAAgNAEBwoPkCkJ3X61mzAACEBgAgNNCAAcC6Zc0CABAaAMDcyocRDQ0YAKFBwOEDyAAAQgMA0IBZYJR3aps7ANYsITcAAEIDADRgDI0YAGuWtQoAAKEBAAGVp+YNjRgAoYF1CgAAoQEAaMJoxgAIua1TAAAIDQBAaKAZA2CtCjd8ewcAQGgAAJoxQgMA65RhjQIAEBoAgGaMpzgBrFPGT2Oz3VmfAACEBgCgIeNJTgBrlGFtAgAQGgCAhozGDIA1yrA2AQAIDQCgtvJKA0NjBqAp5ZVwhnUJAEBoAACe5NScAcCwLgEACA0AQGNGcwYAa5M1CQBAaAAAmjM9jPK6DPMFwLokNGjI//7LP13/9PvfXf/41ZfX//z7P/vMf/3D3/34rw//Pr9XXf/3H+fr3PzOdY5Nq/+dTD+X/G7LGVtb/C7z++Fft9fvv/vm+t//+NvrH3/7158dg+H/N/xrw7/nh/Mh1DExAQDQnNGkAcC6JMSe4Ptvvx4NCd4zhAtDg8Fv+HyTc8rv/6j/+edvHasKx2aOxqnGdR1D0/OZc2QIRYfmqt9SaJB9fR8M/1mhAQBo0AgNAKxJ1qKExp44zNxcEBogNMgbGrx86tr5IjTIGhZ8tr53/nCACQGABo1GDQAv7A9HK41dBk8FBkMD7sdXGbzyuqIxXl0kNBAaCA16Dw1evpLN7yo06G5tf2DNfmRnodAAADRphAYAQmzrUMLm2tAUeK/pP/zrQyPttcaCV3tMMzT073F73IZjdu9/VmNaaJCtrg3/9z3n1FsB6vCv+W2FBr0/DDD8/9/7XsHwrw07FN767xAaAIBGjSc8AaxFYcdmu0u/BtV4onZouL1sLgzNN+fd/OHCbVPU7yI0YDw0eHR3zWuvdBEcCA16DQymvmrrdq3p+VwwOQBoSmlGGBo2AEIDuwzCN0KHpxc1foQGzhWhQe+hwcdjPtZ8HT6S7DcWGvQy92u9XmvsVUe9rTsmCACaNZo2AFiHhNZ3GF4xpGkjNEBoIDR47Kltx0lo0KLhtYBzfo9jLDjo6VWEJgkAmjVCAwCK8go4w9pzdyNUM0xogNBAaPD2sbfbQGjQorGm/tzHraePhJskAAgONG4AsP7YZTCxATD1vccIDYQGQoOIocFg7BsHfmehQeu/6Rxrec/ngkkCgKaN0AAA6491Z2Jz7TZAGP71W0MjYjC83kiTU2ggNBAaRA8NfjgfPn8ty4f/n99aaNCKJV83ePu/M/xvCw0AQONGAwfA2mOXQbRXGoy8s/tRwysKenq3sdAAoYHQoLWnuIUGfpdaOwDmfG3Q7WuQvv/uG6EBAGjcCA0ArD3WmwwNh6mGZoLGp9BAaCA0iBYazPHfKTQQGsw15+f87sbt/1Yv64+JAoDmjSYOAB/sD0cri10Gk15xMDQb3npt0T3sOhAaCA2EBkIDhAb9zvlXHzL47huhAQAIDYQGANYcaw0/N04/Gt7nPTQlhlcgvPY9BL+Z0EBoIDQQGiA0WH7OD7sH7TQQGgCgiRNinM4XF4oA1hvrTKfGwoM5mxZCA6GB0IAlG/y3334RGggNWjLsEFzqmwa3630va72JAoAmjidAAbDeWGMaCA7sNhAaCA2EBlFCA69gExr0tCbM+Xve/u8MrzUUGgCARo6GDoC1xi4DRg1NNE0goYHQQGgQLTS4fYe72iY06OE3nWM3zJLhhNAAAI0cQ2gAYJ2xvgRtrGqCCg2EBkKD3kOD21cTzfnqF6EBk+fpV1/Ovtvv9pj1dC6YJABo6GjqAGBYWx5ohtb67xpeUaAJJDQQGty5M+d8cL50EBqMfa/Fq4mEBi0a2+1Xs6nf+7lgkgCgoaO5A4BhXXngCdpBjRv/JZ5yRGjQSpPz6VfefPu1pmnjocFYk9QuA6FBy+aasxHOBRMEAE0dzR0ADGvKHf70+9/9ssn/1ZeTP2g41lAYmqJ+Z6FBmICtYig29kTwcD76ndsIDYZ//+0riQShQoOeHgYYm7tTHg4Y/jNRzgWTAwBNHQ0egNQ2252VxJoy6XUrtw3MewKE15prGkBCg+i/+cfG2fCR3EfOu7EdBl55M29oMPzfw/F7y1DzxsJPx0ho0JuxV5898nDAj3XqQ127DUp7D89MDgAEBxo8ANYVw3oyobH2lqGZNvz7P3otKNBcExpke4L3rfPk0/nySvPNK2/Wr233GI67j1QLDXp7MOC9evUxRLi3TvW+28bEAMAToRo9AEKD5KOsrebCnY2F2g02gYHQIHNw8CiBQduhgdesCQ2cCzHWGRMCAA0eoQGANcUwDyb48XUETzRENa6FBmnOlVdeMfTo0+vPfkyZeRqlQ5Az9fsuCA2iPRww/Gcj7LQxGQDo0ul80d7R7AEQGFQYZU01Fyo0pYd3fN/zWpVH3ulOvSbQy/ex292xjqGxfM958ovz5duvHa+ZDb/ve98weGk4jsM55RVEQoMMNeu9VwwO/9rw74kWnJkAAGj0CA0ArCXWEGZqVGuswf3niXMF6KVmRf87HWwANHs0fQCsI9YOAAAQGgCg2aPxA2AdsXYAAIDQAAANH40fAKwbAAAgNABA0yfm2B+OGkAA1o+7ho8fAwAgNABA48dTowBYO6wXAAAIDQCIrzxlb5gLANYNawUAAEIDALDbQCMIwJphrQAAQGgAAJpAmkEA1gtrBAAAQgMA0ATSEAKwXtw3fPwYAAChAQAaQUIDAKwT1gcAAIQGAOSV/aPI5e83FwCEBgIDAACEBgBgt4HmEIA1QpgMAIDQAAAEB0IDAGuDNQEAAKEBAGgMeaoUwNogMAAAQGgAAJpDGkUA1gUBMgAAQgMA0CASGgDca7PdWQcAAEBoAABCAwDSrQeONwAAQgMA0CjSNALIvhZ4LREAAEIDAHjH6XwRGgAkluzVRI45AABCAwDwhKmnTQGsAY41AABCAwDQNNJAAkhf/wXFAAAIDQBA40hoAKDuq/cAAAgNAEDz6L5R3uPtuAPqvsAAAACEBgAgONBMAtR8NR4AAIQGACA40FACUOMBAEBoAAAaSZpKAHYZAACA0AAANJM0lgA+2B+O6joAAAgNAEBwoLkEELu+l0DEMQYAQGgAAJ5EFRwACIUdXwAAhAYAoLGkyQSgrju2AAAIDQBAg6nG2Gx3mk2Ami4wAAAAoQEACA40nIAcor52zncMAAAQGgDAwsqT+EIDAAGw+g0AgNAAALDbAEAdV7cBABAaAAC5ggPHGFC/1WwAABAaAIDgQBMKULvVagAAEBoAwEOifkhTIwqIKtp3acrf49gCACA0AABPrGpGAajZjikAAEIDANCE0pACeNjpfFGfAQBAaAAAgoNnRmmyOb6AOi0wAAAAoQEACA40pwC7DNRkAAAQGgDAw6J9YFODChDqtjV8awYAAKEBAGhMCQ4A1Ga1GAAAoQEAaE5pVAGoyeowAABCAwDQpNKwAlCP1V8AAIQGAKBRpWkFoA6rvQAACA0AQMNK8wpADVZzAQAQGgBAVJvtTmgAIDBQbwEAEBoAAHYbAKi9j48SODuGAAAIDQBA80poACAwcAwBABAaAIAmluAAQL117AAAEBoAgEZWb+N0vmhsAeqswAAAAKEBACA40NwC2lQCTTUVAACEBgAgOFh67A9HTS5AbRUYAAAgNAAABAcaXUBbygeE1VEAABAaAIDXaGh4AaifAAAgNAAAT8ZqegGonQAAIDQAAMGB5hdAp6GBYwYAgNAAAAgdHDiOgMBAvQQAQGgAACRufGmEAavq8NswjhsAAEIDAEBwAKBmOl4AAAgNAIB8wYHjB6iVaiMAAEIDAEBwoDkGqJNqIgAAQgMAQHDwYpR3jDt+gProOAEAIDQAAAQHGmWAuug4AQAgNAAABAcaZoCaqP4BACA0AAA0yW6H1xQBAgMAABAaAACCA80zQGAAAABCAwBgbpvtTnAACA3UOgAAEBoAAH0GB/vDUTMNEBgAAIDQAAAQHGioAQIDAAAQGgAAsytP8AsOgLDKB9XVNQAAEBoAAIGewtVgAyLWN8cHAAChAQCgsfbMKE8NO16AwAAAAIQGAIDgQLMN6Leele/JOD4AAAgNAACNNsEBsIRWv9ciMAAAAKEBAAgOBAeAGqZeAQCA0AAANN08tQuoXQIDAAAQGgBAEKUpb7cBIDBQowAAQGgAAFy/OJ0vggNAYKA2AQCA0AAAaL4hpzkHNLcryqvTAABAaAAAggPBAbCC/eHYVBEqu7McGwAAEBoAgOBAcAAkr0mOBwAACA0AIJ8OPpDsOIHAQN0BAAChAQBg14H3iYP6IzAAAAChAQCgcSc4AHVnweH7BQAAIDQAAAQHgHqjtgAAgNAAAHjL/nAUHAACAwAAQGgAALS960BwAGqLWgIAAEIDAEBwoNkHaordBQAAIDQAANbW4uuKBAcgMBAYAACA0AAA0PATHID6oV4AAIDQAADQ+NMIBHXD7gIAABAaAABNOZ0v1waHYwMCA7UBAACEBgCAXQeag9Catb+FUsJNxwIAAIQGAECmxqDgANpTXhumDgAAgNAAALDrQHAAasE6o4SYjgEAAAgNAADBgeAAktcAvz8AAAgNAACNw9dHeUWK4wGBz3u7CwAAQGgAAGggegIZGrHyd00cAwAAEBoAAMIDzUWwu8DvDwAAQgMAwJPIGo2QPRD02wMAgNAAANBk1HSEzCHg6Xxx7gIAgNAAAIimNP4EByD4c74CAIDQAADArgMQ9jk/AQBAaAAA0G544DiA3QUAAIDQAABI/hSzJiXYXQAAAAgNAABPNGtYQkvnog8dAwCA0AAAQHgAyc+9zXbnfAMAAKEBAED74YFmJll4FREAACA0AAC6sD8cV99y4LUpCOeEBQAAgNAAAGhIeep/7eFYICwQwAEAAEIDACD5U9HCA5xDTwyv+gIAAKEBAIDwAJwzzhMAABAaAAAID8A54rcHAAChAQCA8ACEBQAAgNAAAEB4oGmKsAAAABAaAABonI4MH39lKfvDUUgGAAAIDQAAOtl5oLFKxPnt9wcAAIQGAIDmqt0HJN5VYP4CAABCAwBAeKD5irnrGAAAAEIDACCo8sS/AAFBgZ0xAACA0AAA4Cen86W18OBa/pkcH/NRWAAAAAgNAACyPtWteWvXix0vAACA0AAAQBPXLgS7CYQFAACA0AAAwO4DIYKQYJ2xPxzNHQAAQGgAABBt98HL4XVG5opdBQAAgNAAAMDugzeDBDsScodJdhUAAABCAwAAAYIwoYLhNypN9+6Or+MHAAAIDQAAVmosRxvD35QlVAh2/JyTAACA0AAAoBU9PpX+zBj+3o8BQyshw80/T4bh3AMAAIQGAACdMAxBAQAAIDQAAECAYNQZvlEAAAAIDQAAAsv2GiPj8VHmiPMFAAAQGgAAZFOeJDcM5wMAACA0AABAiCAkAAAAEBoAAHAHrzMSEgAAAAgNAAAYdTpftN8bH75JAAAACA0AAFg1SPBaIwEBAACA0AAAALsSFhhDKFN+T3MLAAAQGvgRAAAEChmGYAAAAEBoAADASKgQLViwWwAAAEBoAADAgiHDR3N+Y+Fj8/8lxwAAAEBoAAAAAAAACA0AAAAAAAChAQAAAAAAIDQAAAAAAACEBgAAAAAAgNAAAAAAAAAQGgAAAAAAAEIDAAAAAABAaAAAAAAAAAgNAAAAAAAAoQEAAAAAACA0AAAAAAAAhAYAAAAAAIDQAAAAAAAAEBoAAAAAAABCAwAAAAAAQGgAAAAAAAAIDQAAAAAAAKEBAAAAAAAgNAAAAAAAAGL5fwvATEVScLb1AAAAAElFTkSuQmCC"/>
                              </pattern>
                            </defs>
                            <rect id="image10" width="215" height="85" fill="url(#pattern)"/>
                          </svg>
                                                    
                    </a>
                </div>
                <div class="col">
                    <div class="row ikd">
                        <div class="col d-flex">
                            <a href="mailto:jp@panorama.studio" class="d-flex">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19.095" height="12.889" viewBox="0 0 19.095 12.889">
                                    <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                                </svg>
                    
                                <span class="font-barlow text-white">
                                    jp@panorama.studio
                                </span>  
                            </a>
                        </div>
                        <div class="col d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.914" height="19.914" viewBox="0 0 19.914 19.914">
                                <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M22.821,4.5H6.2A1.625,1.625,0,0,0,4.5,6.051V22.707a1.752,1.752,0,0,0,1.7,1.707H22.816a1.663,1.663,0,0,0,1.6-1.707V6.051A1.527,1.527,0,0,0,22.821,4.5ZM10.673,21.1H7.82v-8.87h2.853ZM9.345,10.88H9.324a1.464,1.464,0,0,1-1.5-1.53A1.473,1.473,0,0,1,9.36,7.82a1.469,1.469,0,0,1,1.525,1.53A1.473,1.473,0,0,1,9.345,10.88ZM21.1,21.1H18.246v-4.85c0-1.162-.415-1.956-1.447-1.956a1.561,1.561,0,0,0-1.463,1.053,1.921,1.921,0,0,0-.1.7V21.1H12.385v-8.87h2.853v1.235a2.888,2.888,0,0,1,2.573-1.442c1.873,0,3.289,1.235,3.289,3.9V21.1Z" transform="translate(-4.5 -4.5)" fill="#fff"/>
                            </svg>
                            <div>
                                <a href="https://www.linkedin.com/company/panorama-studio-madrid" target="_blank" class="font-barlow text-white">panorama-studio-madrid</a> <br>
                             </div>  
                        </div>
                        <div class="col d-flex milay">
                            <span>Panorama Studio © 2022</span>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
    <div class="tab-pane fade" id="animados" role="tabpanel" aria-labelledby="nav-home-animados">
        <div class="animados">
            <div class="container-fluid top">
                <div class="row">
                    <div class="col-4">
                        <h2>
                            {!! __('home.an') !!}

                        </h2>
                    </div>
                    <div class="col">
                        <p>
                            {!! __('home.anp') !!}

                        </p>
                    </div>
                </div>
            </div>
            <div class="container-fluid middle">
                <div class="row">
                    <div class="col-6">
                        <h2>
                            {!! __('home.and') !!}

                        </h2>
                        <p>
                            {!! __('home.andp') !!}

                        </p>
                        <div style="background-image:url(/images/panorama/image19.png)" class="tiger"></div>

                    </div>
                    <div class="col-6 flexh">
                        <div style="background-image:url(/images/panorama/image21.png)" class="gorilla"></div>
                    </div>

                </div> 
                <div class="row mt-3 lb">
                    <div style="background-image:url(/images/panorama/image18.png)" class="yel"></div>
                    <div style="background-image:url(/images/panorama/image20.png)" class="bel"></div>
                    <div style="background-image:url(/images/panorama/image24.png)" class="sel"></div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col fix-video">
                        <video loop autoplay muted playsinline>
                        <source src="https://res.cloudinary.com/dkthctbvw/video/upload/v1653389679/dynam_nokaru.mp4" type="video/mp4">
                        Your browser does not support the video tag.
                        </video>
                        <!--a href="#Dynam-Studio-Signature-1" data-fslightbox data-title="Click the right half of the image to move forward.">
                            <img src="/images/panorama/vids.png" alt="" style="margin-bottom: 69px;">
                        </a-->
                    </div>

                </div> 
            </div>

            <div class="container-fluid magic">
                <div class="row cent">
                    <div style="background-image:url(/images/panorama/image23.png)" class="gel"></div>

                    <div style="background-image:url(/images/panorama/image25.png)" class="mont"></div>

                </div>
            </div>

            <div class="container-fluid ttop">
                <div class="row">
                    <div class="col">
                        <h3>
                            {!! __('home.cr') !!}

                        </h3>
                        <p>
                            {!! __('home.crp') !!}

                        </p>
                    </div>
                    <div class="col">
                        <img data-src="/images/Ozuna-x-Kong---Porto-Rico.gif" alt="" class="msrd plexel" id="amzeiuaze">
                    </div>
                </div>
            </div>
        </div>
        <div class="footer" id="foot">
            <div class="row">
                <div class="col" id="col_ft">
                    <a href="{{ route('home') }}">
                        <img class="plexel" data-src="/images/panorama/image10.png" alt="" id="alzeaze">
                    </a>
                </div>
                <div class="col">
                    <div class="row ikd">
                        <div class="col d-flex">
                            <a href="mailto:jp@panorama.studio" class="d-flex">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19.095" height="12.889" viewBox="0 0 19.095 12.889">
                                    <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                                </svg>
                    
                                <span class="font-barlow text-white">
                                    jp@panorama.studio
                                </span>  
                            </a>
                        </div>
                        <div class="col d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.914" height="19.914" viewBox="0 0 19.914 19.914">
                                <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M22.821,4.5H6.2A1.625,1.625,0,0,0,4.5,6.051V22.707a1.752,1.752,0,0,0,1.7,1.707H22.816a1.663,1.663,0,0,0,1.6-1.707V6.051A1.527,1.527,0,0,0,22.821,4.5ZM10.673,21.1H7.82v-8.87h2.853ZM9.345,10.88H9.324a1.464,1.464,0,0,1-1.5-1.53A1.473,1.473,0,0,1,9.36,7.82a1.469,1.469,0,0,1,1.525,1.53A1.473,1.473,0,0,1,9.345,10.88ZM21.1,21.1H18.246v-4.85c0-1.162-.415-1.956-1.447-1.956a1.561,1.561,0,0,0-1.463,1.053,1.921,1.921,0,0,0-.1.7V21.1H12.385v-8.87h2.853v1.235a2.888,2.888,0,0,1,2.573-1.442c1.873,0,3.289,1.235,3.289,3.9V21.1Z" transform="translate(-4.5 -4.5)" fill="#fff"/>
                            </svg>
                            <div>
                                <a href="https://www.linkedin.com/company/panorama-studio-madrid" target="_blank" class="font-barlow text-white">panorama-studio-madrid</a> <br>
                             </div>  
                        </div>
                        <div class="col d-flex milay">
                            <span>Panorama Studio © 2022</span>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
    <div class="tab-pane fade " id="realestate" role="tabpanel" aria-labelledby="realestate">
        <div class="animados">
            <div class="container-fluid top">
                <div class="row">
                    <div class="col-4">
                        <h2>                            {!! __('home.re') !!}
                        </h2>
                    </div>
                    <div class="col full">
                        <p style="max-width: 536px;">
                           {!! __('home.rep') !!}
                        
                        </p>
                    </div>
                </div>
            </div>

            <div class="container-fluid tttop">
                <div class="row">
                    
                    <div class="col-3">
                        <h3>
                            
                          {!! __('home.perp') !!}

                        </h3>
                        <p>
                          {!! __('home.off') !!}

                        </p>
                    </div>
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image37.png" alt="" style="margin-bottom: 69px;" id="azeazezes">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image38.png" alt="" style="margin-bottom: 69px;" id="zerwer">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image4656.png" alt="" style="margin-bottom: 69px;" id="zswrer">
                    </div>
                </div> 
                <div class="row">
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image33.png" alt="" style="margin-bottom: 69px;" id="mmrer">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image34.png" alt="" style="margin-bottom: 69px;" id="mlerer">
                    </div>
                    
                    <div class="col-3 ml-50">
                        <h3>
                          {!! __('home.m2') !!}

                        </h3>
                        <p>
                          {!! __('home.repre') !!}
                          </p>
                    </div>
                    <div class="col-3">
                
                    </div>
                </div> 
                <div class="row">
                    
                    <div class="col-3">
                        <h3>                          {!! __('home.vv') !!}
                        </h3>
                        <p>{!! __('home.des') !!}</p>
                    </div>
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image40.png" alt="" style="margin-bottom: 69px;" id="mledrer">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image39.png" alt="" style="margin-bottom: 69px;" id="erermlk">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image41.png" alt="" style="margin-bottom: 69px;" id="mlererss">
                    </div>
                </div> 
                
                <div class="row">
                    
                    <div class="col-3">
                        <h3> {!! __('home.vvi') !!}</h3>
                        <p>
                            {!! __('home.vvip') !!}
                        </p>
                    </div>
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image32.png" alt="" style="margin-bottom: 69px;" id="merbbb">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image30.png" alt="" style="margin-bottom: 69px;" id="leernnh">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image31.png" alt="" style="margin-bottom: 69px;" id="lller">
                    </div>
                </div> 
                <div class="row">

                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image29.png" alt="" style="margin-bottom: 69px;" id="llerr">
                    </div>
                                        
                    <div class="col-3 ml-50">
                        <h3>
                            {!! __('home.sph') !!}

                        </h3>
                        <p>
                            {!! __('home.sphp') !!}

                        </p>
                    </div>
                    <div class="col-3">
                    
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image35.png" alt="" style="margin-bottom: 69px;" id="llmelrer">
                    </div>
                </div> 
                <div class="row">
                    
                    <div class="col-3">
                        <h3>
                            {!! __('home.rea') !!}
                        </h3>
                        <p>
                            {!! __('home.reap') !!}

                        </p>
                    </div>
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image42.png" alt="" style="margin-bottom: 69px;" id="mmmer">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image44.png" alt="" style="margin-bottom: 69px;" id="mmermmer">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image43.png" alt="" style="margin-bottom: 69px;" id="mmmerqz">
                    </div>
                </div> 
                <div class="row">
                    

                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image46.png" alt="" style="margin-bottom: 69px;max-width: 245px;" id="mmmmeerer">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image45.png" alt="" style="margin-bottom: 69px;" id="vvvver">
                    </div>
                    <div class="col-3 ml-50">
                        <h3>                            {!! __('home.ax') !!}
                        </h3>
                        <p> {!! __('home.axp') !!}</p>
                    </div>
                    <div class="col-3">
                    </div>
                </div> 
                <div class="row">
                    

                    <div class="col-3">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image_2022_05_20T07_04_34_096Z.png" alt="" style="margin-bottom: 69px;" id="mmewazaz">
                    </div>
                    <div class="col-3 ml-50">
                        <h3> {!! __('home.pm') !!}</h3>
                        <p>{!! __('home.obt') !!}</p>
                    </div>
                    <div class="col-3">
                    </div>
                </div> 
                <div class="row">
                    
                    <div class="col-3">
                        <h3> {!! __('home.peli') !!}</h3>
                        <p>{!! __('home.real') !!}
                        </p>
                    </div>
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image28.png" alt="" style="margin-bottom: 69px;" id="wwwsd">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image47.png" alt="" style="margin-bottom: 69px;" id="xszae">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image11.png" alt="" style="margin-bottom: 69px;" id="mmazerq">
                    </div>
                </div> 
                <div class="row">
                    

                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image49.png" alt="" style="margin-bottom: 69px;" id="axsqsd">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image48.png" alt="" style="margin-bottom: 69px;" id="wazeqsed">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image50.png" alt="" style="margin-bottom: 69px;" id="ppzzer">
                    </div>
                    <div class="col-3 ml-50">
                        <h3>{!! __('home.bim') !!}</h3>
                        <p>{!! __('home.pmb') !!}</p>
                    </div>
                </div> 
                <div class="row">
                    
                    <div class="col-3">
                        <h3>{!! __('home.prec') !!}</h3>
                        <p>{!! __('home.precp') !!}</p>
                    </div>
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image52.png" alt="" style="margin-bottom: 69px;" id="merero">
                    </div>
                    
                    <div class="col-3">
                        <img class="plexel fw" data-src="/images/panorama/image51.png" alt="" style="margin-bottom: 69px;" id="xxer">
                    </div>
                    
                    <div class="col-3 ml-50">
                        <h3>{!! __('home.3d') !!}</h3>
                        <p>{!! __('home.zaeaze') !!}</p>
                    </div>
                </div> 
                {{-- <div class="row">

                    <div class="col-3">
                    </div>
                    <div class="col-3">
                    </div>
                    <div class="col-3">
                    </div>
                </div>  --}}
            </div>

        </div>
        <div class="footer" id="foot">
            <div class="row">
                <div class="col" id="col_ft">
                    <a href="{{ route('home') }}">
                        <img class="plexel" data-src="/images/panorama/image10.png" alt="" id="aazeaze">
                    </a>
                </div>
                <div class="col">
                    <div class="row ikd">
                        <div class="col d-flex">
                            <a href="mailto:jp@panorama.studio" class="d-flex">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19.095" height="12.889" viewBox="0 0 19.095 12.889">
                                    <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                                </svg>
                    
                                <span class="font-barlow text-white">
                                    jp@panorama.studio
                                </span>  
                            </a>
                        </div>
                        <div class="col d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.914" height="19.914" viewBox="0 0 19.914 19.914">
                                <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M22.821,4.5H6.2A1.625,1.625,0,0,0,4.5,6.051V22.707a1.752,1.752,0,0,0,1.7,1.707H22.816a1.663,1.663,0,0,0,1.6-1.707V6.051A1.527,1.527,0,0,0,22.821,4.5ZM10.673,21.1H7.82v-8.87h2.853ZM9.345,10.88H9.324a1.464,1.464,0,0,1-1.5-1.53A1.473,1.473,0,0,1,9.36,7.82a1.469,1.469,0,0,1,1.525,1.53A1.473,1.473,0,0,1,9.345,10.88ZM21.1,21.1H18.246v-4.85c0-1.162-.415-1.956-1.447-1.956a1.561,1.561,0,0,0-1.463,1.053,1.921,1.921,0,0,0-.1.7V21.1H12.385v-8.87h2.853v1.235a2.888,2.888,0,0,1,2.573-1.442c1.873,0,3.289,1.235,3.289,3.9V21.1Z" transform="translate(-4.5 -4.5)" fill="#fff"/>
                            </svg>
                            <div>
                                <a href="https://www.linkedin.com/company/panorama-studio-madrid" target="_blank" class="font-barlow text-white">panorama-studio-madrid</a> <br>
                             </div>  
                        </div>
                        <div class="col d-flex milay">
                            <span>Panorama Studio © 2022</span>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
    <div class="tab-pane fade " id="gaming" role="tabpanel" aria-labelledby="nav-home-gaming">
        <div class="animados">
            <div class="container-fluid top">
                <div class="row">
                    <div class="col-4 surg">
                        <h2>{!! __('home.vide') !!}</h2>
                    </div>
                    <div class="col full">
                        <p>{!! __('home.commu') !!}</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <a href="#orlinski" data-fslightbox data-title="Click the right half of the image to move forward.">
                            <img class="plexel fw" data-src="/images/panorama/image55.png" alt=""  style="margin-bottom: 69px;" id="lazeazeller">
                        </a>
                    </div>
                </div> 
            </div>

        </div>
        <div class="footer" id="foot">
            <div class="row">
                <div class="col" id="col_ft">
                    <a href="{{ route('home') }}">
                        <img class="plexel" data-src="/images/panorama/image10.png" alt="" id="mlzmerlkzer">
                    </a>
                </div>
                <div class="col">
                    <div class="row ikd">
                        <div class="col d-flex">
                            <a href="mailto:jp@panorama.studio" class="d-flex">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19.095" height="12.889" viewBox="0 0 19.095 12.889">
                                    <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                                </svg>
                    
                                <span class="font-barlow text-white">
                                    jp@panorama.studio
                                </span>  
                            </a>
                        </div>
                        <div class="col d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.914" height="19.914" viewBox="0 0 19.914 19.914">
                                <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M22.821,4.5H6.2A1.625,1.625,0,0,0,4.5,6.051V22.707a1.752,1.752,0,0,0,1.7,1.707H22.816a1.663,1.663,0,0,0,1.6-1.707V6.051A1.527,1.527,0,0,0,22.821,4.5ZM10.673,21.1H7.82v-8.87h2.853ZM9.345,10.88H9.324a1.464,1.464,0,0,1-1.5-1.53A1.473,1.473,0,0,1,9.36,7.82a1.469,1.469,0,0,1,1.525,1.53A1.473,1.473,0,0,1,9.345,10.88ZM21.1,21.1H18.246v-4.85c0-1.162-.415-1.956-1.447-1.956a1.561,1.561,0,0,0-1.463,1.053,1.921,1.921,0,0,0-.1.7V21.1H12.385v-8.87h2.853v1.235a2.888,2.888,0,0,1,2.573-1.442c1.873,0,3.289,1.235,3.289,3.9V21.1Z" transform="translate(-4.5 -4.5)" fill="#fff"/>
                            </svg>
                            <div>
                                <a href="https://www.linkedin.com/company/panorama-studio-madrid" target="_blank" class="font-barlow text-white">panorama-studio-madrid</a> <br>
                             </div>  
                        </div>
                        <div class="col d-flex milay">
                            <span>Panorama Studio © 2022</span>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
    <div class="tab-pane fade " id="nft" role="tabpanel" aria-labelledby="nav-home-nft">
        <div class="animados">
            <div class="container-fluid top">
                <div class="row">
                    <div class="col-4">
                        <h2>{!! __('home.nft') !!}</h2>
                    </div>
                    <div class="col full">
                        <p>{!! __('home.nftp') !!}</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid tttop">
                <div class="row">
                    <div class="col-6">
                        <img class="plexel fw" data-src="/images/panorama/image655.png" alt="" style="margin-bottom: 69px;" id="mmmmlkzaeze">
                    </div>
                    <div class="col">
                        <p>{!! __('home.nftpp') !!}</p>
                    </div>
                </div> 
            </div>

        </div>
        <div class="footer" id="foot">
            <div class="row">
                <div class="col" id="col_ft">
                    <a href="{{ route('home') }}">
                        <img class="plexel" data-src="/images/panorama/image10.png" alt="" id="azeazeaze">
                    </a>
                </div>
                <div class="col">
                    <div class="row ikd">
                        <div class="col d-flex">
                            <a href="mailto:jp@panorama.studio" class="d-flex">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19.095" height="12.889" viewBox="0 0 19.095 12.889">
                                    <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                                </svg>
                    
                                <span class="font-barlow text-white">
                                    jp@panorama.studio
                                </span>  
                            </a>
                        </div>
                        <div class="col d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.914" height="19.914" viewBox="0 0 19.914 19.914">
                                <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M22.821,4.5H6.2A1.625,1.625,0,0,0,4.5,6.051V22.707a1.752,1.752,0,0,0,1.7,1.707H22.816a1.663,1.663,0,0,0,1.6-1.707V6.051A1.527,1.527,0,0,0,22.821,4.5ZM10.673,21.1H7.82v-8.87h2.853ZM9.345,10.88H9.324a1.464,1.464,0,0,1-1.5-1.53A1.473,1.473,0,0,1,9.36,7.82a1.469,1.469,0,0,1,1.525,1.53A1.473,1.473,0,0,1,9.345,10.88ZM21.1,21.1H18.246v-4.85c0-1.162-.415-1.956-1.447-1.956a1.561,1.561,0,0,0-1.463,1.053,1.921,1.921,0,0,0-.1.7V21.1H12.385v-8.87h2.853v1.235a2.888,2.888,0,0,1,2.573-1.442c1.873,0,3.289,1.235,3.289,3.9V21.1Z" transform="translate(-4.5 -4.5)" fill="#fff"/>
                            </svg>
                            <div>
                                <a href="https://www.linkedin.com/company/panorama-studio-madrid" target="_blank" class="font-barlow text-white">panorama-studio-madrid</a> <br>
                             </div>  
                        </div>
                        <div class="col d-flex milay">
                            <span>Panorama Studio © 2022</span>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
    <div class="tab-pane fade " id="motion" role="tabpanel" aria-labelledby="nav-home-motion">
        <div class="animados">
            <div class="container-fluid top">
                <div class="row">
                    <div class="col-4">
                        <h2>{!! __('home.mtion') !!}</h2>
                    </div>
                    <div class="col full">
                        <p>{!! __('home.mtionp') !!}</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid tttop">
                <div class="row">
                    <div class="col-6">
                        <a href="#Rocks_edit" data-fslightbox> 
                            <img class="plexel fw" data-src="/images/panorama/m.png" alt="" style="margin-bottom: 9px;" id="amwer">
                        </a>
                    </div>
                    <div class="col">
                    </div>
                </div> 
                <div class="row">
                    <div class="col">
                    </div>
                    <div class="col-8">
                        <a href="#Les-Atouts" data-fslightbox>
                            <img src="/images/panorama/n.png" alt="" class="fw fwrd" style="margin-bottom: 9px;">
                        </a>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-8">
                        <a href="#Ventes-Aux-Encheres"  data-fslightbox>
                            <img src="/images/panorama/l.png" alt="" class="fw fwre me-auto" style="margin-bottom: 9px;">
                        </a>
                    </div>
                    <div class="col">
                    </div>
                </div> 
            </div>

        </div>
        <div class="footer" id="foot">
            <div class="row">
                <div class="col" id="col_ft">
                    <a href="{{ route('home') }}">
                        <img src="/images/panorama/image10.png" alt="">
                    </a>
                </div>
                <div class="col">
                    <div class="row ikd">
                        <div class="col d-flex">
                            <a href="mailto:jp@panorama.studio" class="d-flex">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19.095" height="12.889" viewBox="0 0 19.095 12.889">
                                    <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                                </svg>
                    
                                <span class="font-barlow text-white">
                                    jp@panorama.studio
                                </span>  
                            </a>
                        </div>
                        <div class="col d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.914" height="19.914" viewBox="0 0 19.914 19.914">
                                <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M22.821,4.5H6.2A1.625,1.625,0,0,0,4.5,6.051V22.707a1.752,1.752,0,0,0,1.7,1.707H22.816a1.663,1.663,0,0,0,1.6-1.707V6.051A1.527,1.527,0,0,0,22.821,4.5ZM10.673,21.1H7.82v-8.87h2.853ZM9.345,10.88H9.324a1.464,1.464,0,0,1-1.5-1.53A1.473,1.473,0,0,1,9.36,7.82a1.469,1.469,0,0,1,1.525,1.53A1.473,1.473,0,0,1,9.345,10.88ZM21.1,21.1H18.246v-4.85c0-1.162-.415-1.956-1.447-1.956a1.561,1.561,0,0,0-1.463,1.053,1.921,1.921,0,0,0-.1.7V21.1H12.385v-8.87h2.853v1.235a2.888,2.888,0,0,1,2.573-1.442c1.873,0,3.289,1.235,3.289,3.9V21.1Z" transform="translate(-4.5 -4.5)" fill="#fff"/>
                            </svg>
                            <div>
                                <a href="https://www.linkedin.com/company/panorama-studio-madrid" target="_blank" class="font-barlow text-white">panorama-studio-madrid</a> <br>
                             </div>  
                        </div>
                        <div class="col d-flex milay">
                            <span>Panorama Studio © 2022</span>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
    <div class="tab-pane fade" id="sound" role="tabpanel" aria-labelledby="nav-home-sound">
        <div class="animados">
            <div class="container-fluid top">
                <div class="row">
                    <div class="col-4">
                        <h2>{!! __('home.sound') !!}</h2>
                    </div>
                    <div class="col full">
                        <p>{!! __('home.sounp') !!}</p>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <a href="#sounde" data-fslightbox>
                            <img src="/images/panorama/au.png" alt="" style="margin-bottom: 69px;">
                        </a>
                    </div>
                </div> 
            </div>

        </div>
        <div class="footer" id="foot">
            <div class="row">
                <div class="col" id="col_ft">
                    <a href="{{ route('home') }}">
                        <img class="plexel" data-src="/images/panorama/image10.png" alt="" id="mmmmmewxxxx">
                    </a>
                </div>
                <div class="col">
                    <div class="row ikd">
                        <div class="col d-flex">
                            <a href="mailto:jp@panorama.studio" class="d-flex">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19.095" height="12.889" viewBox="0 0 19.095 12.889">
                                    <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                                </svg>
                    
                                <span class="font-barlow text-white">
                                    jp@panorama.studio
                                </span>  
                            </a>
                        </div>
                        <div class="col d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.914" height="19.914" viewBox="0 0 19.914 19.914">
                                <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M22.821,4.5H6.2A1.625,1.625,0,0,0,4.5,6.051V22.707a1.752,1.752,0,0,0,1.7,1.707H22.816a1.663,1.663,0,0,0,1.6-1.707V6.051A1.527,1.527,0,0,0,22.821,4.5ZM10.673,21.1H7.82v-8.87h2.853ZM9.345,10.88H9.324a1.464,1.464,0,0,1-1.5-1.53A1.473,1.473,0,0,1,9.36,7.82a1.469,1.469,0,0,1,1.525,1.53A1.473,1.473,0,0,1,9.345,10.88ZM21.1,21.1H18.246v-4.85c0-1.162-.415-1.956-1.447-1.956a1.561,1.561,0,0,0-1.463,1.053,1.921,1.921,0,0,0-.1.7V21.1H12.385v-8.87h2.853v1.235a2.888,2.888,0,0,1,2.573-1.442c1.873,0,3.289,1.235,3.289,3.9V21.1Z" transform="translate(-4.5 -4.5)" fill="#fff"/>
                            </svg>
                            <div>
                                <a href="https://www.linkedin.com/company/panorama-studio-madrid" target="_blank" class="font-barlow text-white">panorama-studio-madrid</a> <br>
                             </div>  
                        </div>
                        <div class="col d-flex milay">
                            <span>Panorama Studio © 2022</span>
                        </div>
                    </div>
                </div>
            </div>
    
        </div>
    </div>
</div>
<div class="content-home" id="home-">
    <div class="container-fluid">
        <div class="row">
            <div class="col pano-logo">
                <img src="/images/panorama/image1.png" alt="panorama">
            </div>
            <div class="col sobre sobre-l">
                <h3>{!! __('home.sobno') !!}</h3>
            </div>
            <div class="col sobre sobre-p">
                <p>{!! __('home.sobnop') !!}</p>
            </div>
        </div>
        <div class="row t104">
            <div class="col-6 artista">
                <h3>{!! __('home.art') !!}</h3>
                <p>{!! __('home.artp') !!}</p>
            </div>
            <div class="col-6">
                <div class="mask11" style="background-image:url('/images/panorama/image111.png'); ">

                </div>
            </div>
        </div>
    </div>

</div>

<div class="contact-h position-relative" id="contac">
    <div class="container-fluid ">
        <div class="row">
            <div class="col">
                <h3>{!! __('home.cnos') !!}</h3>
    
                <p>
                    <ul class="ok">
                        <li><a href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.095" height="12.889" viewBox="0 0 19.095 12.889">
                                <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                            </svg>
                            jp@panorama.studio
                            </a>
                        </li>
                        <li><a href="https://www.linkedin.com/company/panorama-studio-madrid" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.914" height="19.914" viewBox="0 0 19.914 19.914">
                                <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M22.821,4.5H6.2A1.625,1.625,0,0,0,4.5,6.051V22.707a1.752,1.752,0,0,0,1.7,1.707H22.816a1.663,1.663,0,0,0,1.6-1.707V6.051A1.527,1.527,0,0,0,22.821,4.5ZM10.673,21.1H7.82v-8.87h2.853ZM9.345,10.88H9.324a1.464,1.464,0,0,1-1.5-1.53A1.473,1.473,0,0,1,9.36,7.82a1.469,1.469,0,0,1,1.525,1.53A1.473,1.473,0,0,1,9.345,10.88ZM21.1,21.1H18.246v-4.85c0-1.162-.415-1.956-1.447-1.956a1.561,1.561,0,0,0-1.463,1.053,1.921,1.921,0,0,0-.1.7V21.1H12.385v-8.87h2.853v1.235a2.888,2.888,0,0,1,2.573-1.442c1.873,0,3.289,1.235,3.289,3.9V21.1Z" transform="translate(-4.5 -4.5)" fill="#fff"/>
                            </svg>
                            
                            panorama-studio-madrid
                            </a>
                        </li>
                    </ul>
                </p>
            </div>
            <div class="col">
                <form action="{{ route('message') }}" method="POST" class="form-c">
                    @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <p>
                            {{ Session::get('success') }}
                        </p>
                    </div>
                    @endif
                    @if(Session::has('errors'))
                    <div class="alert alert-warning" role="warning">
                        <?php
                            $errors = Session::get('errors');
                        ?>
                        
                        <p>
                            {!! implode('', $errors->all('<p>:message</p>')) !!}
                        </p>
                        <?php 
                        ?>
                    </div>
                    @endif
                    @csrf
                    <div class="mb-3">
                        <input type="text" class="form-control" id="nom" placeholder="{{ __('contact.nom') }}" name="nom" required>
                    </div>
                    
                    <div class="mb-3">
                        <input type="text" class="form-control" id="prenom" placeholder="{{ __('contact.prenom') }}" name="prenom" required>
                    </div>
                    <div class="mb-3">
                        <input type="email" class="form-control" id="email" placeholder="{{ __('contact.email') }}" name="email" required>
                    </div>
                    
                    <div class="mb-3">
                        <input type="text" class="form-control" id="objet" placeholder="{{ __('contact.object') }}" name="objet" required>
                    </div>
                    <div class="mb-3">
                        <textarea name="message" id="message" cols="30" rows="10" class="form-control" required></textarea>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="m-btn font-expanded-bold">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22.414" height="22.414" viewBox="0 0 22.414 22.414">
                                <g id="send-svgrepo-com" transform="translate(-1 -0.586)">
                                <line id="Ligne_1" data-name="Ligne 1" x1="11" y2="11" transform="translate(11 2)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                <path id="Tracé_11757" data-name="Tracé 11757" d="M22,2,15,22l-4-9L2,9Z" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                </g>
                            </svg>
                            <span style="margin-left: 30px;">{!! __('home.envie') !!}</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col">
                
                <a href="#" class="float">
                    <img src="/images/panorama/panorama.png" alt="Panorama studio">
                </a>
            </div>
        </div>
    </div>
</div>

</div>

<div class="iframe">
    <iframe
        src="https://player.vimeo.com/video/644256333?h=b1023a687c"
        id="salon"
        width="1920px"
        height="1080px"
        frameBorder="0"
        allow="autoplay; fullscreen"
        allowFullScreen>
    </iframe>
        
    <iframe
        src="https://player.vimeo.com/video/670720624"
        id="showreal"
        width="1920px"
        height="1080px"
        frameBorder="0"
        allow="autoplay; fullscreen"
        allowFullScreen>
    </iframe>

    <iframe
    src="https://player.vimeo.com/video/665673121"
    id="initial_1"
    width="1920px"
    height="1080px"
    frameBorder="0"
    allow="autoplay; fullscreen"
    allowFullScreen>
</iframe>
<iframe
src="https://player.vimeo.com/video/665672780"
id="mb"
width="1920px"
height="1080px"
frameBorder="0"
allow="autoplay; fullscreen"
allowFullScreen>
</iframe>

<iframe
src="https://player.vimeo.com/video/669737455"
id="Dynam-Studio-Signature-1"
width="1920px"
height="1080px"
frameBorder="0"
allow="autoplay; fullscreen"
allowFullScreen>
</iframe>
<iframe
src="https://player.vimeo.com/video/665987940"
id="orlinski"
width="1920px"
height="1080px"
frameBorder="0"
allow="autoplay; fullscreen"
allowFullScreen>
</iframe>


<iframe
src="https://player.vimeo.com/video/669740319"
id="Rocks_edit"
width="1920px"
height="1080px"
frameBorder="0"
allow="autoplay; fullscreen"
allowFullScreen>
</iframe>

<iframe
src="https://player.vimeo.com/video/669740193"
id="Les-Atouts"
width="1920px"
height="1080px"
frameBorder="0"
allow="autoplay; fullscreen"
allowFullScreen>
</iframe>

<iframe
src="https://player.vimeo.com/video/669740260"
id="Ventes-Aux-Encheres"
width="1920px"
height="1080px"
frameBorder="0"
allow="autoplay; fullscreen"
allowFullScreen>
</iframe>

<iframe
src="https://player.vimeo.com/video/665669972"
id="sounde"
width="1920px"
height="1080px"
frameBorder="0"
allow="autoplay; fullscreen"
allowFullScreen>
</iframe>

</div>

</div>
@stop

@section('scripts')
<script>
(function() {
    var css = [
        
        {
            link: "https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,500;0,700;0,800;1,400&display=swap",
            integrity: "",
            crossorigin: "",
            type: "font",
        },
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css",
            integrity: "",
            crossorigin: "",
            type: "css",
        },
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css",
            integrity: "sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3",
            crossorigin: "anonymous",
            type: "css"
        },
        
        {
            link: "fonts/stylesheet.css",
            integrity: "",
            crossorigin: "",
            type: "css"
        },
        {
            link: "css/app.css",
            integrity: "",
            crossorigin: "",
            type: "css"
        }
    ];

    var js = [
        
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js",
            integrity: "sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p",
            crossorigin: "anonymous",
            type: "js"
        },

        {
            link: "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        
        // {
        //     link: "/js/fslightbox.js",
        //     integrity: "",
        //     crossorigin: "",
        //     type: "js"
        // },
        
        {
            link: "/js/fslightbox.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollToPlugin.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollTrigger.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        {
            link: '/js/home.js',
            integrity: "",
            crossorigin: "",
            type: "js"
        }
    ];

    var img = getImgs();
    var done = 0;
    var srcs = [...css, ...img, ...js];

    addSCript(done, srcs);

    function getImgs() {
        var imgs = document.querySelectorAll('.plexel')
        var src = srcs
        var arr = []
        for (let i = 0; i < imgs.length; i++) {
            var url = imgs[i].getAttribute('data-src')
            if(url) {
                arr.push({
                    link: url,
                    id: imgs[i].getAttribute('id'),
                    type: "img"     
                })
            }
        }
        return arr
    }

    function insertAt(array, index, ...elementsArray) {
        array.splice(index, 0, ...elementsArray);
    }

    function updateDone(done) {
        var percent = (done / srcs.length) * 100;
        percent = Math.round((percent + Number.EPSILON) * 100) / 100
        document.getElementById('loading').innerHTML = Math.round(percent)
    }

    function insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    function addSCript(index, srcs) {
        if (index < srcs.length) {
            if(srcs[index].type == "img") {
               var img = document.getElementById(srcs[index].id)
               img.src = srcs[index].link
                done++;
                updateDone(done)
                setTimeout(function() {
                    addSCript(index + 1, srcs)
                }, 100)
            } else {
                var src = srcs[index].type == "js" ? document.createElement('script') : document.createElement('link');                
                if (srcs[index].integrity != "") {
                    src.setAttribute('integrity', srcs[index].integrity)
                }
                if (srcs[index].crossorigin != "") {
                    src.setAttribute('crossorigin', srcs[index].crossorigin)
                }
                if (srcs[index].type == "js") {
                    src.src = srcs[index].link;
                    document.body.append(src);
                } else {
                    src.href = srcs[index].link;
                    src.setAttribute('rel', "stylesheet")
                    document.head.append(src);
                }
                src.onload = function() {
                    done++;
                    if(index === 2) {
                        document.getElementById('loader-text').classList.remove('hidden')
                    }
                    updateDone(done)
                    setTimeout(function() {
                        addSCript(index + 1, srcs)
                    }, 100)
                };
            }
        } else {
            setTimeout(function() {
                document.getElementById('loader').classList.add('hidden')
            }, 150);
        }

    }

})()
</script>
<script>
    (function() {
        document.addEventListener("DOMContentLoaded", function(){
            window.addEventListener('scroll', function() {
                if (window.scrollY > 50) {
                    document.getElementById('navbar_top').classList.add('bg-black');
                } else {
                    document.getElementById('navbar_top').classList.remove('bg-black');
                } 

                if(window.scrollY > 950) {
                    document.getElementById('nav-tab').classList.add('fixed-tab')
                    document.getElementById('navbar_top').classList.add('with-bg')
                    // document.getElementById('nav-tabContent').classList.add('is-fixed')
                    document.getElementById('home-').classList.add('is-fixed')
                } else {
                    document.getElementById('nav-tab').classList.remove('fixed-tab')
                    document.getElementById('navbar_top').classList.remove('with-bg')
                    // document.getElementById('nav-tabContent').classList.remove('is-fixed')
                    document.getElementById('home-').classList.remove('is-fixed')
                }
                
            });
        }); 
        // DOMContentLoaded  end
    })()
</script>
@stop