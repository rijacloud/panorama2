@extends('template')
@section('title')
{{__('menu.group')}}
@stop
@section('description')
{{__('menu.group')}}
@stop
@section('content')
<div class="gr">
    <div class="groupe">
        <div class="container-fluid position-relative">
            <div class="row" id="prez">
                <div class="col" id="pimp-prez">
                    <h2 class="font-medium text-white" >
                        <a href="https://www.pimpmyroom-company.fr" target="_blank">
                            Pimp <br>
                            My room <br>
                            Company
                        </a>
                    </h2>
                    <div class="f">
    
                        <p class="font-regular text-white">
                            {{ __('groupe.pmr_1') }}
                        </p>
                        <a href="http://localhost:8000/groupe" class="expertise-btn d-flex flex-row font-bold text-uppercase text-white pop">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 28 28">
                                <path id="Tracé_849" data-name="Tracé 849" d="M29.28,1.114a.343.343,0,0,0-.388-.388H17.956V-10.132a.343.343,0,0,0-.388-.388H12.914q-.388,0-.388.31V.726H1.668a.343.343,0,0,0-.388.388V5.768a.343.343,0,0,0,.388.388H12.526V17.092q0,.388.465.388h4.576a.343.343,0,0,0,.388-.388V6.156H28.892a.343.343,0,0,0,.388-.388Z" transform="translate(-1.28 10.52)" fill="#e2051e"></path>
                            </svg>
                            <div cass="ms-2">{{ __('groupe.voir')}}</div>
                        </a>
                    </div>
                </div>
                <div class="col" id="panorama-prez">
                    <h2 class="font-medium text-white">
                        {!! __('groupe.pano_title') !!}
                    </h2>
                    <div class="f">
                        <p class="font-regular text-white">
                            {{ __('groupe.pano_sub') }}
                        </p>
        
                        <a href="http://localhost:8000/groupe" class="expertise-btn d-flex flex-row font-bold text-uppercase text-white pop">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 28 28">
                                <path id="Tracé_849" data-name="Tracé 849" d="M29.28,1.114a.343.343,0,0,0-.388-.388H17.956V-10.132a.343.343,0,0,0-.388-.388H12.914q-.388,0-.388.31V.726H1.668a.343.343,0,0,0-.388.388V5.768a.343.343,0,0,0,.388.388H12.526V17.092q0,.388.465.388h4.576a.343.343,0,0,0,.388-.388V6.156H28.892a.343.343,0,0,0,.388-.388Z" transform="translate(-1.28 10.52)" fill="#e2051e"></path>
                            </svg>
                            <div cass="ms-2">{{ __('groupe.voir')}}</div>
                        </a>
                        
                    </div>
                    
                </div>
                
                <div class="col" id="pano-prez">
                    <h2 class="font-medium text-white">
                        {!! __('groupe.pano_art') !!}
                    </h2>
                    <div class="f">
                        <p class="font-regular text-white">
                            {{ __('groupe.pano_art_desc') }}
                        </p> 
        
                        <a href="http://localhost:8000/groupe" class="expertise-btn d-flex flex-row font-bold text-uppercase text-white pop">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 28 28">
                                <path id="Tracé_849" data-name="Tracé 849" d="M29.28,1.114a.343.343,0,0,0-.388-.388H17.956V-10.132a.343.343,0,0,0-.388-.388H12.914q-.388,0-.388.31V.726H1.668a.343.343,0,0,0-.388.388V5.768a.343.343,0,0,0,.388.388H12.526V17.092q0,.388.465.388h4.576a.343.343,0,0,0,.388-.388V6.156H28.892a.343.343,0,0,0,.388-.388Z" transform="translate(-1.28 10.52)" fill="#e2051e"></path>
                            </svg>
                            <div cass="ms-2">{{ __('groupe.voir')}}</div>
                        </a>    
                    </div>
                    
                </div>
                <div class="col" id="logia-prez">
                    <h2 class="font-medium text-white" >
                        <a href="https://www.logiastudios.com" target="_blank">
                            Logia <br>
                            Studios
                        </a>
                    </h2>
                    <div class="f">
                        <p class="font-regular text-white">
                            {{ __('groupe.logia_sub') }}
                        </p>
        
                        <a href="http://localhost:8000/groupe" class="expertise-btn d-flex flex-row font-bold text-uppercase text-white pop">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 28 28">
                                <path id="Tracé_849" data-name="Tracé 849" d="M29.28,1.114a.343.343,0,0,0-.388-.388H17.956V-10.132a.343.343,0,0,0-.388-.388H12.914q-.388,0-.388.31V.726H1.668a.343.343,0,0,0-.388.388V5.768a.343.343,0,0,0,.388.388H12.526V17.092q0,.388.465.388h4.576a.343.343,0,0,0,.388-.388V6.156H28.892a.343.343,0,0,0,.388-.388Z" transform="translate(-1.28 10.52)" fill="#e2051e"></path>
                            </svg>
                            <div cass="ms-2">{{ __('groupe.voir')}}</div>
                        </a>    
                    </div>
                    
                </div>
                <div class="col" id="isart-prez">
                    <h2 class="font-medium text-white" >
                        isart
                    </h2>
                    <div class="f">
                        <p class="font-regular text-white">
                            {{ __('groupe.isart_desc')}}
                        </p>
        
                        <a href="http://localhost:8000/groupe" class="expertise-btn d-flex flex-row font-bold text-uppercase text-white pop">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 28 28">
                                <path id="Tracé_849" data-name="Tracé 849" d="M29.28,1.114a.343.343,0,0,0-.388-.388H17.956V-10.132a.343.343,0,0,0-.388-.388H12.914q-.388,0-.388.31V.726H1.668a.343.343,0,0,0-.388.388V5.768a.343.343,0,0,0,.388.388H12.526V17.092q0,.388.465.388h4.576a.343.343,0,0,0,.388-.388V6.156H28.892a.343.343,0,0,0,.388-.388Z" transform="translate(-1.28 10.52)" fill="#e2051e"></path>
                            </svg>
                            <div cass="ms-2">{{ __('groupe.voir')}}</div>
                        </a>
        
                    </div>
                    
                </div>
    
            </div>
            <div class="row" id="pimp">
                <a class="close" href="#">
                    <img src="{{ asset('/images/close.png') }}" alt="close">
                </a>
                <div class="col-3">
                    <h2 class="font-medium text-white" >
                        <a href="https://www.pimpmyroom-company.fr" target="_blank">
                            Pimp <br>
                            My room <br>
                            Company
                        </a>
                    </h2>
    
                    <p class="font-regular text-white">
                        {{ __('groupe.pmr_1') }}
                    </p>
    
                    <p class="font-regular text-white">
                        {{ __('groupe.pmr_2') }}
                    </p>
                    <a href="https://www.pimpmyroom-company.fr" class="font-bold text-white" target="_blank">www.pimpmyroom-company.fr</a>
    
                </div>
                <div class="col position-relative ">
                    <div class="slides_min">
                        <img src="{{ asset('images/4K_PMR_Côté salon_Scandinave moderne.jpg') }}" alt="" class="background appear" id="pimp-4"><!-- 1 -->
                        <!--img src="{{ asset('images/black-and-white-photo-of-hourglass-close-up-2021-09-03-08-50-12-utc.png') }}" alt="" class="background" id="pimp-5"--><!-- 2 -->
                        <img src="{{ asset('images/Séguin_Hiver_Pimp Business (1).png') }}" alt="" class="background disapear" id="pimp-6"><!-- 3 -->
                    </div>
                </div>
            </div>
    
            <div class="row" id="panorama">
                <a class="close" href="#">
                    <img src="{{ asset('/images/close.png') }}" alt="close">
                </a>
                <div class="col-3">
                    <h2 class="font-medium text-white">
                        {!! __('groupe.pano_title') !!}
                    </h2>
                    <p class="font-regular text-white">
                        {{ __('groupe.pano_sub') }}
                        <br><br>
                        {{ __('groupe.pano_desc_1') }}
                        <br><br>
                        {!! __('groupe.pano_desc_2') !!}
                    </p>
                    <a href="{{ asset('/files/Prez Panorama V8.pdf') }}" target="_blank" class="font-bold text-uppercase text-white">{{ __('groupe.down')}} </a>

                </div>
                <div class="col position-relative ">
                    <div class="slides_min">
    
                        <img src="{{ asset('images/Asset 4@2x-100.jpg') }}" alt="" id="pano_-4" class="background appear">   
                        <img src="{{ asset('images/Asset 5@2x-100.jpg') }}" alt="" id="pano_-5" class="background disapear">   
    
                        <img src="{{ asset('images/Asset 3@2x-100.jpg') }}" alt="" id="pano_-1" class="background disapear">
                        <img src="{{ asset('images/Asset 2@2x-100.jpg') }}" alt="" id="pano_-2" class="background disapear">   
                        <img src="{{ asset('images/Asset 1@2x-100.jpg') }}" alt="" id="pano_-3" class="background disapear">   
    

                    </div>
                </div>
            </div>
    
            <div class="row" id="pano">
                <a class="close" href="#">
                    <img src="{{ asset('/images/close.png') }}" alt="close">
                </a>
                <div class="col-3">
                    <h2 class="font-medium text-white">
                        {!! __('groupe.pano_art') !!}
                    </h2>
                    <p class="font-regular text-white">
                        {{ __('groupe.pano_art_desc') }}
                    </p> 
                    
                    <p class="font-regular text-white">
                        {{ __('groupe.pano_art_desc_1') }}
                    </p> 
                    
                    <p class="font-regular text-white">
                        {!! __('groupe.pano_art_desc_2') !!}
                    </p> 
    
                </div>
                <div class="col position-relative ">
                    <div class="slides_min">
                        
                        <img src="{{ asset('images/pano/1.png') }}" alt="" id="pano-1" class="background appear">
                        <img src="{{ asset('images/pano/2.png') }}" alt="" id="pano-2" class="background disapear">   
                        <img src="{{ asset('images/pano/3.png') }}" alt="" id="pano-3" class="background disapear">   

                        
    
                    </div>
                </div>
            </div>
    
            <div class="row" id="logia">
                <a class="close" href="#">
                    <img src="{{ asset('/images/close.png') }}" alt="close">
                </a>
                <div class="col-3">
                    <h2 class="font-medium text-white" >
                        <a href="https://www.logiastudios.com" target="_blank">
                            Logia <br>
                            Studios
                        </a>
                    </h2>
                    <p class="font-regular text-white">
                        {{ __('groupe.logia_sub') }}
                    </p>
    
                    <p class="font-regular text-white ">
                        {{ __('groupe.logia_desc') }}
                    </p>
                    <a href="https://www.logiastudios.com" target="_blank" class="font-regular text-white ">
                        www.logiastudios.com
                    </a>
    
                </div>
                <div class="col position-relative ">
                    <div class="slides_min">   
                        <iframe src="https://www.youtube.com/embed/QdZeBOtyjcQ?rel=0&listType=playlist" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>        
                    </div>
                </div>
            </div>
            <div class="row" id="isart">
                <a class="close" href="#">
                    <img src="{{ asset('/images/close.png') }}" alt="close">
                </a>
                <div class="col-3">
                    <h2 class="font-medium text-white" >
                        isart
                    </h2>
                    <p class="font-regular text-white">
                        {{ __('groupe.isart_desc')}}
                    </p>
    
                    <p class="font-regular text-white">
                        {{ __('groupe.isart_desc_2')}}
                    </p>
    
                    <p class="font-regular text-white">
                        {{ __('groupe.isart_desc_3')}}
                    </p>
    
                </div>
                <div class="col position-relative ">
                    <div class="slides_min">
                        <img src="{{ asset('images/masterpiece-painting-in-art-workshop-2021-08-29-00-18-06-utc.jpg') }}" alt="" id="isart_-3" class="background appear">           
    
                        <img src="{{ asset('images/african-music-session-2021-09-04-05-16-56-utc.jpg') }}" alt="" id="isart_-1" class="background disapear">
                        <img src="{{ asset('images/pexels-luizclas-556669.png') }}" alt="" id="isart_-2" class="background disapear">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer" id="foot">
        <div class="row">
            <div class="col" id="col_ft">
                <a href="{{ route('home') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="169.806" height="24.356" viewBox="0 0 169.806 24.356">
                        <g id="Groupe_27" data-name="Groupe 27" transform="translate(-226.21 -458.865)">
                          <path id="Tracé_821" data-name="Tracé 821" d="M463.941,246.639l-8.9,20.379H454.3l-9.517-20.41-2.54,1.184,10.272,22.028h4.36l9.637-22.059Z" transform="translate(-149.918 212.8)" fill="#fff"/>
                          <path id="Tracé_822" data-name="Tracé 822" d="M349.386,246.711l-8.518,20.358h-.961l-7.126-18.178h-4.2l-7.126,18.178h-.961l-8.519-20.358-2.584,1.082,9.238,22.079h4.738l7.126-18.178h.378l7.126,18.178h4.738l9.238-22.079Z" transform="translate(-83.179 212.748)" fill="#fff"/>
                          <path id="Tracé_823" data-name="Tracé 823" d="M403.848,267.635a9.038,9.038,0,1,1,9.038-9.039A9.048,9.048,0,0,1,403.848,267.635Zm9.037-16.671a11.84,11.84,0,1,0,0,15.266V269.9h2.8V247.3h-2.8Z" transform="translate(-124.683 212.726)" fill="#fff"/>
                          <path id="Tracé_824" data-name="Tracé 824" d="M515.142,254.28a12.8,12.8,0,0,1-12.789,12.789,10.389,10.389,0,0,1-10.377-10.376,8.384,8.384,0,0,1,8.375-8.375,6.72,6.72,0,0,1,6.712,6.712,5.34,5.34,0,0,1-5.333,5.334v2.8a8.145,8.145,0,0,0,8.135-8.136,9.526,9.526,0,0,0-9.515-9.515,11.19,11.19,0,0,0-11.177,11.177,13.2,13.2,0,0,0,13.18,13.179,15.61,15.61,0,0,0,15.592-15.592Z" transform="translate(-173.495 213.349)" fill="#fff"/>
                          <path id="Tracé_825" data-name="Tracé 825" d="M546.989,290.492v-2.453c0-.04.029-.06.089-.06h2.483c.058,0,.089.02.089.06v2.453a.079.079,0,0,1-.089.089h-2.483a.1.1,0,0,1-.089-.089Z" transform="translate(-202.54 192.017)" fill="#fff"/>
                          <path id="Tracé_826" data-name="Tracé 826" d="M571.565,274.192a3.01,3.01,0,0,0,.665-1.847h.029l-.029-1.951a1.337,1.337,0,0,1-.71.473,6.491,6.491,0,0,1-1.419.207l-5.32.414a5.059,5.059,0,0,0-2.586.695,1.859,1.859,0,0,0-.7,1.552v.236a2.038,2.038,0,0,0,.961,1.937,6.963,6.963,0,0,0,3.236.546,9.2,9.2,0,0,0,3.7-.649A5.178,5.178,0,0,0,571.565,274.192Zm4.566,3.651c0,.1-.07.147-.207.147h-1.507a2.482,2.482,0,0,1-1.656-.443,1.8,1.8,0,0,1-.5-1.419v-.768h-.207a8.089,8.089,0,0,1-1.182,1.226,7.163,7.163,0,0,1-2.187,1.108,11.594,11.594,0,0,1-3.813.532,7.47,7.47,0,0,1-4.359-.991,3.647,3.647,0,0,1-1.345-3.148v-.354q0-3.4,4.552-3.754l5.764-.443a8.616,8.616,0,0,0,2.054-.369.987.987,0,0,0,.7-1.02v-.03a3.124,3.124,0,0,0-1.182-2.571,6.928,6.928,0,0,0-4.079-.888q-5.32,0-5.32,3.163v.325a.131.131,0,0,1-.149.148H559.52a.131.131,0,0,1-.148-.148v-.325a4.277,4.277,0,0,1,1.729-3.576q1.729-1.332,5.867-1.331t5.852,1.434a5.213,5.213,0,0,1,1.715,4.242l.029,6.827a.921.921,0,0,0,.177.65.972.972,0,0,0,.681.178h.5c.137,0,.207.05.207.147Z" transform="translate(-208.657 204.607)" fill="#fff"/>
                          <path id="Tracé_827" data-name="Tracé 827" d="M611.012,268.236v1.508a.131.131,0,0,1-.148.148h-1.98a.131.131,0,0,1-.148-.148V268.5a3.863,3.863,0,0,0-.975-2.911,4.531,4.531,0,0,0-3.192-.932,4.817,4.817,0,0,0-2.6.665,4.375,4.375,0,0,0-1.6,1.671,4.157,4.157,0,0,0-.532,1.951v8.9a.13.13,0,0,1-.147.147h-2.01a.131.131,0,0,1-.148-.147V263.27a.13.13,0,0,1,.148-.147h2.01a.13.13,0,0,1,.147.147v2.453h.207a5.342,5.342,0,0,1,1.8-1.906,6.377,6.377,0,0,1,3.665-.9,5.668,5.668,0,0,1,4.2,1.331A5.489,5.489,0,0,1,611.012,268.236Z" transform="translate(-227.931 204.607)" fill="#fff"/>
                          <path id="Tracé_828" data-name="Tracé 828" d="M632.677,269.677a3.292,3.292,0,0,0,.5,1.995,1.906,1.906,0,0,0,1.626.665h3.576a.13.13,0,0,1,.147.147v1.449a.13.13,0,0,1-.147.147H634.1a3.6,3.6,0,0,1-2.748-1,4.024,4.024,0,0,1-.946-2.868v-9.251h-3.695a.13.13,0,0,1-.147-.148v-1.448a.13.13,0,0,1,.147-.147H630.4v-3.931a.131.131,0,0,1,.148-.148h1.98a.131.131,0,0,1,.148.148v3.931h5.7a.13.13,0,0,1,.147.147v1.448a.13.13,0,0,1-.147.148h-5.7Z" transform="translate(-242.513 208.516)" fill="#fff"/>
                        </g>
                      </svg>                              
                </a>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col d-flex">
                        <a href="#" class="d-flex">
                            <svg xmlns="http://www.w3.org/2000/svg" width="19.201" height="15.361" viewBox="0 0 19.201 15.361">
                                <path id="Icon_material-mail" data-name="Icon material-mail" d="M20.281,6H4.92A1.918,1.918,0,0,0,3.01,7.92L3,19.441a1.926,1.926,0,0,0,1.92,1.92H20.281a1.926,1.926,0,0,0,1.92-1.92V7.92A1.926,1.926,0,0,0,20.281,6Zm0,3.84-7.681,4.8L4.92,9.84V7.92l7.681,4.8,7.681-4.8Z" transform="translate(-3 -6)" fill="#fff"/>
                            </svg>
                            <span class="font-barlow text-white">
                                bertrand@wave.art
                            </span>  
                        </a>
                    </div>
                    <div class="col d-flex">
                        <svg xmlns="http://www.w3.org/2000/svg" width="19.157" height="19.158" viewBox="0 0 19.157 19.158">
                            <path id="Icon_awesome-phone-alt" data-name="Icon awesome-phone-alt" d="M18.611,13.538l-4.191-1.8A.9.9,0,0,0,13.373,12l-1.856,2.267a13.869,13.869,0,0,1-6.63-6.63L7.154,5.782a.9.9,0,0,0,.258-1.048L5.616.543A.9.9,0,0,0,4.587.023L.7.921A.9.9,0,0,0,0,1.8,17.36,17.36,0,0,0,17.362,19.158a.9.9,0,0,0,.876-.7l.9-3.891A.909.909,0,0,0,18.611,13.538Z" transform="translate(0 0)" fill="#fff"/>
                        </svg>
                        <div>
                            <a href="tel:+33611945624" class="font-barlow text-white">+33 (0) 6 11 94 56 24</a> <br>
                            <a href="tel:+261324831579" class="font-barlow text-white">+261 (0) 32 48 315 79</a>    
                        </div>  
                    </div>
                    <div class="col d-flex">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13.263" height="19.158" viewBox="0 0 13.263 19.158">
                            <path id="Icon_ionic-ios-pin" data-name="Icon ionic-ios-pin" d="M14.507,3.375A6.41,6.41,0,0,0,7.875,9.532c0,4.79,6.632,13,6.632,13s6.632-8.211,6.632-13A6.41,6.41,0,0,0,14.507,3.375Zm0,8.792a2.16,2.16,0,1,1,2.16-2.16A2.16,2.16,0,0,1,14.507,12.167Z" transform="translate(-7.875 -3.375)" fill="#fff"/>
                        </svg>
                        <span class="font-barlow text-white">
                            120 rue Jean Jaures, 92300 Levallois Perret
                        </span>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</div>

@stop

@section('scripts')
<script>
(function() {
    var css = [
        
        {
            link: "https://fonts.googleapis.com/css2?family=Barlow:wght@500&display=swap",
            integrity: "",
            crossorigin: "",
            type: "font",
        },
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css",
            integrity: "sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3",
            crossorigin: "anonymous",
            type: "css"
        },
        
        {
            link: "css/app.css",
            integrity: "",
            crossorigin: "",
            type: "css"
        }
    ];

    var js = [
        
        {
            link: "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js",
            integrity: "sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p",
            crossorigin: "anonymous",
            type: "js"
        },

        {
            link: "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        
        {
            link: "https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },

        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/gsap.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },

        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollToPlugin.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        },
        {
            link: "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.8.0/ScrollTrigger.min.js",
            integrity: "",
            crossorigin: "",
            type: "js"
        }
    ];

    var img = getImgs();
    var done = 0;
    var srcs = [...css, ...img, ...js];

    addSCript(done, srcs);

    function getImgs() {
        var imgs = document.querySelectorAll('.background')
        var src = srcs
        var arr = []
        for (let i = 0; i < imgs.length; i++) {
            var url = imgs[i].getAttribute('data-src')
            if(url) {
                arr.push({
                    link: url,
                    id: imgs[i].getAttribute('id'),
                    type: "img"     
                })
            }
        }
        return arr
    }

    function insertAt(array, index, ...elementsArray) {
        array.splice(index, 0, ...elementsArray);
    }

    function updateDone(done) {
        var percent = (done / srcs.length) * 100;
        percent = Math.round((percent + Number.EPSILON) * 100) / 100
        document.getElementById('loading').innerHTML = Math.round(percent)
    }

    function insertAfter(referenceNode, newNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    function addSCript(index, srcs) {
        if (index < srcs.length) {
            if(srcs[index].type == "img") {
            var img = document.getElementById(srcs[index].id)
            img.src = srcs[index].link
                done++;
                updateDone(done)
                setTimeout(function() {
                    addSCript(index + 1, srcs)
                }, 100)
            } else {
                var src = srcs[index].type == "js" ? document.createElement('script') : document.createElement('link');                
                if (srcs[index].integrity != "") {
                    src.setAttribute('integrity', srcs[index].integrity)
                }
                if (srcs[index].crossorigin != "") {
                    src.setAttribute('crossorigin', srcs[index].crossorigin)
                }
                if (srcs[index].type == "js") {
                    src.src = srcs[index].link;
                    document.body.append(src);
                } else {
                    src.href = srcs[index].link;
                    src.setAttribute('rel', "stylesheet")
                    document.head.append(src);
                }
                src.onload = function() {
                    done++;
                    if(index === 2) {
                        document.getElementById('loader-text').classList.remove('hidden')
                    }
                    updateDone(done)
                    setTimeout(function() {
                        addSCript(index + 1, srcs)
                    }, 100)
                };
            }
        } else {
            setTimeout(function() {
                document.getElementById('loader').classList.add('hidden')
            }, 150);
        }

    }
})()
</script>
<script>
    (function() {
        document.addEventListener("DOMContentLoaded", function(){
            window.addEventListener('scroll', function() {
                if (window.scrollY > 50) {
                    document.getElementById('navbar_top').classList.add('bg-black');
                } else {
                    document.getElementById('navbar_top').classList.remove('bg-black');
                } 
            });
        }); 
        // DOMContentLoaded  end
        var pimp = document.querySelector('#pimp-prez .pop')
        var logia = document.querySelector('#logia-prez .pop')
        var isart = document.querySelector('#isart-prez .pop')
        var panorama = document.querySelector('#panorama-prez .pop')
        var pano = document.querySelector('#pano-prez .pop')

        let interval = null;

        pimp.addEventListener('click', function(e) {
            e.preventDefault();
            if(
                !document.getElementById('pimp').classList.contains('hover')
            ) {
                document.querySelector('.groupe').classList.add('bg_black')
                document.getElementById('prez').classList.add('haspop')
                document.getElementById('pimp-prez').classList.add('hide')
                document.getElementById('pimp').classList.add('hover')
                document.getElementById('panorama-prez').classList.add('hide')
                document.getElementById('logia-prez').classList.add('hide')
                document.getElementById('isart-prez').classList.add('hide')
                document.getElementById('pano-prez').classList.add('hide')
                document.querySelector('#pimp .slides_min').classList.add('open')
                interval = slides();
                listenClick()
            }
        })

        logia.addEventListener('click', function(e) {
            e.preventDefault();
            if(
                !document.getElementById('pimp').classList.contains('hover')
            ) {
                document.querySelector('.groupe').classList.add('bg_black')
                document.getElementById('prez').classList.add('haspop')
                document.getElementById('logia-prez').classList.add('hide')
                document.getElementById('logia').classList.add('hover')
                document.getElementById('panorama-prez').classList.add('hide')
                document.getElementById('pimp-prez').classList.add('hide')
                document.getElementById('isart-prez').classList.add('hide')
                document.getElementById('pano-prez').classList.add('hide')
                document.querySelector('#logia .slides_min').classList.add('open')
                interval = slides();
                listenClick()


            }
        })
        
        isart.addEventListener('click', function(e) {
            e.preventDefault();
            if(
                !document.getElementById('pimp').classList.contains('hover')
            ) {
                document.querySelector('.groupe').classList.add('bg_black')
                document.getElementById('prez').classList.add('haspop')
                document.getElementById('isart-prez').classList.add('hide')
                document.getElementById('isart').classList.add('hover')
                document.getElementById('panorama-prez').classList.add('hide')
                document.getElementById('pimp-prez').classList.add('hide')
                document.getElementById('logia-prez').classList.add('hide')
                document.getElementById('pano-prez').classList.add('hide')
                document.querySelector('#isart .slides_min').classList.add('open')
                interval = slides();
                listenClick()

            }
        })
        
        
        panorama.addEventListener('click', function(e) {
            e.preventDefault();
            if(
                !document.getElementById('pimp').classList.contains('hover')
            ) {
                document.querySelector('.groupe').classList.add('bg_black')
                document.getElementById('prez').classList.add('haspop')
                document.getElementById('panorama-prez').classList.add('hide')
                document.getElementById('panorama').classList.add('hover')
                document.getElementById('isart-prez').classList.add('hide')
                document.getElementById('pimp-prez').classList.add('hide')
                document.getElementById('logia-prez').classList.add('hide')
                document.getElementById('pano-prez').classList.add('hide')
                document.querySelector('#panorama .slides_min').classList.add('open')
                interval = slides();
                listenClick()

            }
        })

        
        pano.addEventListener('click', function(e) {
            e.preventDefault();
            if(
                !document.getElementById('pimp').classList.contains('hover')
            ) {
                document.querySelector('.groupe').classList.add('bg_black')
                document.getElementById('prez').classList.add('haspop')
                document.getElementById('pano-prez').classList.add('hide')
                document.getElementById('pano').classList.add('hover')
                document.getElementById('isart-prez').classList.add('hide')
                document.getElementById('pimp-prez').classList.add('hide')
                document.getElementById('logia-prez').classList.add('hide')
                document.getElementById('panorama-prez').classList.add('hide')
                document.querySelector('#pano .slides_min').classList.add('open')
                interval = slides();
                listenClick()

            }
        })
        document.querySelectorAll('.close').forEach(function(item) {
            item.addEventListener('click', function(e) {
                e.preventDefault();
                document.querySelector('.hover').classList.remove('hover')
                document.querySelector('.haspop').classList.remove('haspop')
                document.querySelector('.bg_black').classList.remove('bg_black')
                var hide = document.querySelectorAll('.hide')
                for (var i = 0; i< hide.length; i++) {
                    hide[i].classList.remove('hide')
                }
                document.querySelector('.open').classList.remove('open')
                var hide = document.querySelectorAll('.background')
                for (var i = 0; i< hide.length; i++) {
                    if(hide[i].classList.contains('appear')) {
                        hide[i].classList.remove('appear')
                        hide[i].classList.add('disapear')
                    }
                }
                /*
                var hide = document.querySelectorAll('.disapear')
                for (var i = 0; i< hide.length; i++) {
                }
                */
                deslides(interval)
                removeClick()
                document.getElementById('isart_-3').classList.add('appear')
                document.getElementById('isart_-3').classList.remove('disapear')

                document.getElementById('pano-1').classList.add('appear')
                document.getElementById('pano-1').classList.remove('disapear')

                document.getElementById('pano_-4').classList.add('appear')
                document.getElementById('pano_-4').classList.remove('disapear')

                document.getElementById('pimp-4').classList.add('appear')
                document.getElementById('pimp-4').classList.remove('disapear')

                
            })
        })

        function slides () {
            var current_slide = 0;
            var list_slides = document.querySelectorAll('.slides_min.open img')
        
            interval = setInterval(() => {  
                list_slides.forEach(function(item) {
                    if(!item.classList.contains('disapear')) {
                        item.classList.add('disapear')
                        item.classList.remove('appear')
                    }
                })
                
                list_slides[current_slide].classList.add('appear')
                list_slides[current_slide].classList.remove('disapear')
                if(!list_slides[current_slide].classList.contains('appear')) {
                    list_slides[current_slide].classList.add('appear')
                }
                if( list_slides.length > 2)  {
                    current_slide = ((current_slide + 1) == list_slides.length) ? 0 : current_slide + 1
                } else {
                    console.log(current_slide + 1)
                    current_slide = ((current_slide + 1) == list_slides.length) ? 0 : current_slide + 1
                }

            }, 2000);

            return interval
        }

        function deslides(interval) {
            clearInterval(interval)
        }

        function listenClick(elem) {

        }
        
        function removeClick(elem) {
            window.removeEventListener('click', function(e) {})
        }
    })()
</script>
@stop