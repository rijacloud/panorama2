<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--meta http-equiv="X-UA-Compatible" content="IE=edge"-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="author" content="@yield('author')">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('images/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('images/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <!-- CSS only -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <style type="text/css">
    @keyframes rotation {
        from {
            transform: rotate(0deg);
        }

        to {
            transform: rotate(359deg);
        }
    }

    @keyframes invisible {
        from {
            transform: translateX(-50%);
            opacity: 0.8;
        }

        to {
            transform: translateX(-150%);
            opacity: 0.2;

        }
    }


    .loadingio-spinner-gear-s1isj1j2e1p {
        width: 100%;
        height: 100%;
        display: inline-block;
        overflow: hidden;
        background: #ffffff;
        position: fixed;
        z-index: 11000;
    }

    .loadingio-spinner-gear-s1isj1j2e1p p {
        text-align: center;
    }

    .ldio-2naj4a27i9c {
        width: 100%;
        height: 100%;
        position: absolute;
        transform: translateZ(0) scale(1);
        backface-visibility: hidden;
        transform-origin: 0 0;
        margin: auto;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .ldio-2naj4a27i9c div {
        box-sizing: content-box;
        text-align: center;
    }

    .ldio-2naj4a27i9c svg {
        animation: rotation 2s infinite linear;

        transform: scale(2);
        margin-bottom: 20px;
    }

    /* generated by https://loading.io/ */

    .hidden {
        animation: invisible 2s infinite linear;
        visibility: hidden;
    }
    </style>
</head>

<body class="">
    <div class="loadingio-spinner-gear-s1isj1j2e1p" id="loader">
        <div class="ldio-2naj4a27i9c">
            <div>

                <!--svg xmlns="http://www.w3.org/2000/svg" width="28.771" height="24.357" viewBox="0 0 28.771 24.357">
                    <path id="Tracé_8299" data-name="Tracé 8299"
                        d="M515.142,254.28a12.8,12.8,0,0,1-12.789,12.789,10.389,10.389,0,0,1-10.377-10.376,8.384,8.384,0,0,1,8.375-8.375,6.72,6.72,0,0,1,6.712,6.712,5.34,5.34,0,0,1-5.333,5.334v2.8a8.145,8.145,0,0,0,8.135-8.136,9.526,9.526,0,0,0-9.515-9.515,11.19,11.19,0,0,0-11.177,11.177,13.2,13.2,0,0,0,13.18,13.179,15.61,15.61,0,0,0,15.592-15.592Z"
                        transform="translate(-489.173 -245.516)" fill="#e2051e" />
                </svg-->

                <p class="font-medium red hidden" id="loader-text" style="font-size: 30px;">
                    <span id="loading">0</span> %
                </p>
            </div>

        </div>
    </div>
    <!-- Menu -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-transparent" id="navbar_top">
        <div class="container">
            <a class="navbar-brand ml-logo" href="{{ route('home') }}">
                <img src="/images/panorama/image10.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse mr-menu" id="navbarNav">
                <ul class="navbar-nav  me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link {{ 'home' == Route::currentRouteName() ? 'active' : '' }} font-regular font-menu text-white text-uppercase" aria-current="page"
                            href="{{ route('home') }}">INICIO</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link {{ 'about' == Route::currentRouteName() ? 'active' : '' }} font-regular font-menu text-white text-uppercase"
                            href="#home-" id="sobree">SOBRE NOSOTROS</a>
                    </li>
                    
                    {{-- <li class="nav-item">
                        <a class="nav-link {{ 'about' == Route::currentRouteName() ? 'active' : '' }} font-regular font-menu text-white text-uppercase"
                            href="#home-" id="servicios">SERVICIOS</a>
                    </li>
                     --}}
                    <li class="nav-item">
                        <a class="nav-link {{ 'expertises' == Route::currentRouteName() ? 'active' : '' }} font-regular font-menu text-white text-uppercase" id="tocontac"
                            href="#contac">CONTACTO</a>
                    </li>
                    

                </ul>
                <!--ul class="navbar-nav  ms-auto mb-2 mb-lg-0" id="social_menu">
                    <li class="nav-item">
                        <a class="nav-link font-regular font-menu text-white" aria-current="page"
                            href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15.035" height="15.035"
                                viewBox="0 0 15.035 15.035">
                                <path id="instagram-svgrepo-com"
                                    d="M10.849.048a5.356,5.356,0,0,1,1.778.337,3.358,3.358,0,0,1,1.225.8,3.358,3.358,0,0,1,.8,1.225,5.357,5.357,0,0,1,.336,1.778c.043.946.048,1.279.048,3.332s-.005,2.386-.048,3.332a5.357,5.357,0,0,1-.337,1.778,3.535,3.535,0,0,1-2.023,2.023,5.356,5.356,0,0,1-1.778.336c-.946.043-1.279.048-3.332.048s-2.386-.005-3.332-.048a5.356,5.356,0,0,1-1.778-.337,3.357,3.357,0,0,1-1.225-.8,3.358,3.358,0,0,1-.8-1.225,5.357,5.357,0,0,1-.336-1.778C.005,9.9,0,9.571,0,7.518S.005,5.131.048,4.186A5.357,5.357,0,0,1,.385,2.408a3.357,3.357,0,0,1,.8-1.225,3.357,3.357,0,0,1,1.225-.8A5.356,5.356,0,0,1,4.186.048C5.131.005,5.464,0,7.518,0S9.9.005,10.849.048ZM7.518,1.671c-2.03,0-2.34,0-3.256.047a3.722,3.722,0,0,0-1.249.225,1.7,1.7,0,0,0-.649.422,1.7,1.7,0,0,0-.422.649,3.723,3.723,0,0,0-.225,1.249c-.042.915-.047,1.226-.047,3.256s0,2.341.047,3.256a3.723,3.723,0,0,0,.225,1.249,1.7,1.7,0,0,0,.422.649,1.7,1.7,0,0,0,.649.422,3.722,3.722,0,0,0,1.249.225c.916.042,1.226.047,3.256.047s2.34,0,3.256-.047a3.722,3.722,0,0,0,1.249-.225,1.864,1.864,0,0,0,1.071-1.071,3.723,3.723,0,0,0,.225-1.249c.042-.916.047-1.226.047-3.256s0-2.34-.047-3.256a3.723,3.723,0,0,0-.225-1.249,1.7,1.7,0,0,0-.422-.649,1.7,1.7,0,0,0-.649-.422,3.722,3.722,0,0,0-1.249-.225C9.858,1.675,9.548,1.671,7.518,1.671Zm0,9.745a3.9,3.9,0,1,1,3.9-3.9A3.9,3.9,0,0,1,7.518,11.416Zm0-1.671A2.227,2.227,0,1,0,5.29,7.518,2.227,2.227,0,0,0,7.518,9.745Zm3.9-5.29a.835.835,0,1,1,.835-.835A.835.835,0,0,1,11.416,4.455Z"
                                    fill="#fff" />
                            </svg>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link font-regularfont-menu text-white">
                            <svg id="XMLID_801_" xmlns="http://www.w3.org/2000/svg" width="15.068" height="15.035"
                                viewBox="0 0 15.068 15.035">
                                <path id="XMLID_802_"
                                    d="M8.2,99.73H5.17a.243.243,0,0,0-.243.243v9.718a.243.243,0,0,0,.243.243H8.2a.243.243,0,0,0,.243-.243V99.973A.243.243,0,0,0,8.2,99.73Z"
                                    transform="translate(-4.688 -94.899)" fill="#fff" />
                                <path id="XMLID_803_" d="M2,.341A1.994,1.994,0,1,0,3.991,2.335,2,2,0,0,0,2,.341Z"
                                    transform="translate(0 -0.341)" fill="#fff" />
                                <path id="XMLID_804_"
                                    d="M112.217,94.761a3.553,3.553,0,0,0-2.658,1.116v-.631a.243.243,0,0,0-.243-.243h-2.9a.243.243,0,0,0-.243.243v9.718a.243.243,0,0,0,.243.243h3.018a.243.243,0,0,0,.243-.243v-4.808c0-1.62.44-2.251,1.57-2.251,1.23,0,1.328,1.012,1.328,2.335v4.725a.243.243,0,0,0,.243.243h3.02a.243.243,0,0,0,.243-.243V99.633C116.083,97.224,115.624,94.761,112.217,94.761Z"
                                    transform="translate(-101.015 -90.171)" fill="#fff" />
                            </svg>

                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link font-regularfont-menu text-white"><svg id="XMLID_834_"
                                xmlns="http://www.w3.org/2000/svg" width="7.595" height="15.035"
                                viewBox="0 0 7.595 15.035">
                                <path id="XMLID_835_"
                                    d="M76.946,8.008h1.648v6.785a.242.242,0,0,0,.243.243h2.794a.242.242,0,0,0,.243-.243V8.04h1.895a.243.243,0,0,0,.241-.215l.288-2.5a.242.242,0,0,0-.241-.27H81.873V3.491c0-.472.254-.711.755-.711h1.427a.242.242,0,0,0,.243-.243V.244A.242.242,0,0,0,84.056,0H82.089L82,0a3.768,3.768,0,0,0-2.464.929,2.584,2.584,0,0,0-.859,2.3V5.057h-1.73A.242.242,0,0,0,76.7,5.3V7.765A.243.243,0,0,0,76.946,8.008Z"
                                    transform="translate(-76.703)" fill="#fff" />
                            </svg>
                        </a>
                    </li>
                </ul-->

                <ul class="navbar-nav  ms-auto mb-2 mb-lg-0" id="lang_menu">
                    <li class="nav-item">
                        <a href="/lang/en" class="nav-link font-regular font-menu text-white {{ 'en' == Lang::locale() ? 'active' : '' }}">
                            EN
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/lang/esp" class="nav-link font-regular font-menu text-white {{ 'esp' == Lang::locale() ? 'active' : '' }}">
                            ESP
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- end Menu -->
    @yield('content')


    <!-- footer -->
    <!-- end footer -->
    <!-- JavaScript Bundle with Popper -->

    <!--script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script-->

    @yield('scripts')

</body>

</html>