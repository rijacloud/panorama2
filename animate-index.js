var end = false;
var doScroll = false;
window.scrollTo(0, 0);
function initTextEnd() {
    gsap.set("#text-slide-10", { translateX: -50, opacity: 0 });
    gsap.set("#text-slide-11", { translateX: 50, opacity: 0 });
    gsap.set("#text-slide-12", { translateX: -50, opacity: 0 });
    gsap.set("#text-slide-13", { translateX: 50, opacity: 0 });
    gsap.set("#text-slide-14", { translateX: -50, opacity: 0 });
}
async function autoScroll(target, timeout, previous, step) {
    var i = window.scrollY;
    if (target > 0) {
        const container = document.querySelector("#mainContainer");
        target = container.clientHeight + 500;
    }
    if ((step > 0 && doScroll) || (i > target && step < 0 && doScroll)) {
        window.scrollBy(0, step);
        setTimeout(() => {
            autoScroll(target, timeout, i, step);
        }, timeout);
    } else {
        return false;
    }
}
function staticAnimation() {
    //console.log("ST");
    gsap.to(
        "#text-slide-10",
        { translateX: 0, opacity: 1, duration: 0.1, overwrite: true },
        "+=0.01"
    );
    gsap.to(
        "#text-slide-11",
        { translateX: 0, opacity: 1, duration: 0.12, overwrite: true },
        "+=0.01"
    );
    gsap.to(
        "#text-slide-12",
        { translateX: 0, opacity: 1, duration: 0.14, overwrite: true },
        "+=0.01"
    );
    gsap.to(
        "#text-slide-13",
        { translateX: 0, opacity: 1, duration: 0.16, overwrite: true },
        "+=0.01"
    );
    gsap.to(
        "#text-slide-14",
        { translateX: 0, opacity: 1, duration: 0.18, overwrite: true },
        "+=0.01"
    );
    animateImg();
}

function animateImg() {
    /* gsap.to("#image-10", {
        translateX: 0,
        opacity: 1,
        duration: 0.2,
        overwrite: "auto",
    }); */
    gsap.set("#image-11", { translateX: 0, opacity: 1 });
    gsap.set("#image-12", { translateX: 0, opacity: 1 });
    gsap.set("#image-13", { translateX: 0, opacity: 1 });
    gsap.to(
        "#image-10",
        {
            opacity: 0,
            duration: 0.7,
            ease: "power3.out",
        },
        "+=0.4"
    );
    gsap.to(
        "#image-11",
        {
            opacity: 0,
            duration: 0.7,
            ease: "power3.out",
        },
        "+=0.4"
    );
    gsap.to(
        "#image-12",
        {
            opacity: 0,
            duration: 0.7,
            ease: "power3.out",
        },
        "+=0.4"
    );

    /* gsap.to(
        "#image-10",
        {
            onComplete: () => {
                if (end) {
                    animateImg();
                       const mainContainer =
                        document.querySelector("#mainContainer");
                    const scroll = mainContainer.clientHeight;
                    autoScroll(0, 100, scroll, -125); 
                }
            },
            translateX: "0%",
            opacity: 1,
            duration: 1,
            ease: "power3.out",
        },
        "+=1"
    ); */
    gsap.to(
        ".image0, #text-slide-0",
        {
            onComplete: () => {
                /* const mainContainer = document.querySelector("#mainContainer");
                const scroll = mainContainer.clientHeight;
                autoScroll(0, 10, 1, -1); */
                /* gsap.to(window, {
                    scrollTo: "max",
                    duration: 10,
                    ease: "power2.out",
                    overwrite: "auto",
                }); */
                doScroll = false;
                window.scrollTo(0, 0);
            },
            opacity: 1,
            duration: 0.7,
            ease: "power4.in",
            overwrite: true,
        },
        "+=0.1"
    );
}
function trigger() {
    doScroll = false;
    initTextEnd();
    //text 1 : ART
    gsap.set(
        "#text-slide-1, #text-slide-0",
        {
            //height: "100vh",
            width: "100vw",
            textAlign: "center",
            translateY: "25vh",
            opacity: 1,
            duration: 0.1,
            overwrite: true,
        },
        0
    );
    gsap.to(
        "#text-slide-3",
        {
            width: "100vw",
            textAlign: "center",
            translateY: "40vh",
            opacity: 0,
            duration: 0.1,
            overwrite: "auto",
        },
        0
    );
    /* gsap.set("#text-slide-4, #text-slide-5", {
        opacity: 0,
        duration: 0.1,
        overwrite: true,
    }); */
    gsap.to(
        "#image-6",
        {
            translateX: "0",
            translateY: "0",
            duration: 0.1,
            ease: "power4.out",
            overwrite: "auto",
        },
        0
    );
    gsap.to(
        "#text-slide-4, #text-slide-5, #text-slide-6",
        {
            opacity: 0,
            duration: 0.1,
            overwrite: true,
        },
        0
    );

    gsap.to(
        "#text-slide-7",
        {
            translateX: "0",
            translateY: "50vh",
            scale: 1,
            opacity: 0,
            duration: 0.1,
            ease: "power4.in",
            overwrite: true,
        },
        0
    );
    gsap.to(
        "#text-slide-9",
        {
            translateX: "100%",
            opacity: 0,
            duration: 0.1,
            ease: "power4.in",
            overwrite: true,
        },
        0
    );

    /* gsap.to("#text-end", {
        ease: "power4.in",
        overwrite: true,
    }); */

    gsap.to(
        ".image0, #text-slide-0",
        {
            onStart: () => {
                gsap.set(
                    "#text-slide-2",
                    {
                        opacity: 0,
                        translateX: "-100%",
                        overwrite: true,
                    },
                    0
                );
            },
            onComplete: () => {
                if (window.scrollY == 0) {
                    gsap.to(
                        "#loader",
                        {
                            onComplete: () => {
                                setTimeout(() => {
                                    const loader =
                                        document.querySelector("#loader");
                                    loader.toggleAttribute("hidden", true);
                                    const mainContainer =
                                        document.querySelector(
                                            "#mainContainer"
                                        );
                                    const scroll = mainContainer.clientHeight;
                                    doScroll = true;
                                    autoScroll(scroll, 100, 0, 150);
                                }, 500);
                            },
                            opacity: 0,
                            ease: "power4.out",
                            duration: 0.5,
                            overwrite: true,
                        },
                        ">"
                    );
                }

                /* gsap.to(window, {
                    scrollTo: "max",
                    duration: 10,
                    ease: "power2.out",
                    overwrite: "auto",
                }); */
            },
            opacity: 0,
            duration: 0.5,
            ease: "power4.in",
            overwrite: true,
        },
        ">"
    );

    //img1
    gsap.timeline({
        onReverseComplete: () => {
            initTextEnd();
            gsap.to(
                ".image0, #text-slide-0",
                {
                    onComplete: () => {
                        const mainContainer =
                            document.querySelector("#mainContainer");
                        doScroll = true;
                        const scroll = mainContainer.clientHeight;
                        autoScroll(scroll, 100, 0, 150);

                        gsap.to(
                            "#loader",
                            {
                                onComplete: () => {
                                    const loader =
                                        document.querySelector("#loader");
                                    loader.toggleAttribute("hidden", true);
                                },
                                opacity: 0,
                                duration: 0.5,
                                overwrite: true,
                            },
                            ">"
                        );
                    },
                    opacity: 0,
                    duration: 0.1,
                    ease: "power4.in",
                    overwrite: true,
                },
                0
            );
        },
        scrollTrigger: {
            trigger: ".h-animated",
            start: "2% 2%",
            end: "8% 8%",
            scrub: 1,
        },
    }).fromTo(
        "#image-1",
        {
            translateY: 0,
            opacity: 1,
            ease: "power4.in",
            overwrite: true,
        },
        {
            translateY: "-100vh",
            opacity: 0,
            ease: "power4.in",
            overwrite: "auto",
        },
        0
    );

    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "0% 0%",
            end: "5% 5%",
            scrub: 1,
        },
    }).fromTo(
        "#text-slide-1",
        {
            opacity: 1,
            ease: "power4.in",
            overwrite: true,
        },
        {
            opacity: 0,
            ease: "power4.in",
            overwrite: "auto",
        },
        0
    );

    //text 2 : Creative
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "7% 7%",
            end: "23% 23%",
            scrub: 1,
        },
    }).fromTo(
        "#text-slide-2",
        {
            translateX: "-100%",
            ease: "linear",
            overwrite: "auto",
        },
        {
            translateX: "150%",
            ease: "linear",
            overwrite: "auto",
        },
        0
    );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "8% 8%",
            end: "9% 9%",
            scrub: 1,
        },
    }).fromTo(
        "#text-slide-2",
        {
            opacity: 0,
            ease: "power4.out",
            overwrite: "auto",
        },
        {
            opacity: 1,
            ease: "power4.out",
            overwrite: "auto",
        },
        0
    );
    //img2
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "20% 20%",
            end: "23% 23%",
            scrub: 1,
        },
    }).fromTo(
        "#image-2",
        {
            opacity: 1,
            ease: "linear",
            overwrite: true,
        },
        {
            opacity: 0,
            ease: "linear",
            overwrite: "auto",
        }
    );
    //img3
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "18% 18%",
            end: "20% 20%",
            scrub: 1,
        },
    })
        .fromTo(
            "#mCircle",
            {
                attr: { r: "0%" },
                ease: "linear",
                overwrite: true,
            },
            {
                attr: { r: "200%" },
                ease: "linear",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#svg-1",
            {
                scale: 1,
                ease: "linear",
                overwrite: true,
            },
            {
                scale: 1.1,
                ease: "linear",
                overwrite: "auto",
            },
            0
        );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "10% 10%",
            end: "20% 20%",
            scrub: 1,
        },
    }).fromTo(
        "#image-2",
        {
            scale: 1,
            ease: "power1.in",
            overwrite: true,
        },
        {
            scale: 1.2,
            ease: "power1.in",
            overwrite: "auto",
        },
        0
    );
    //text 3 : Digital
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "18% 18%",
            end: "26% 26%",
            scrub: 1,
        },
    }).fromTo(
        "#text-slide-3",
        {
            scale: 2.0,
            opacity: 0,
            ease: "power4.inOut",
            overwrite: "auto",
        },
        {
            scale: 1,
            opacity: 1,
            ease: "power4.inOut",
            overwrite: "auto",
        }
    );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "26% 26%",
            end: "32% 32%",
            scrub: 1,
        },
    }).fromTo(
        "#text-slide-3",
        {
            scale: 1.0,
            opacity: 1,
            ease: "power4.inOut",
            overwrite: "auto",
        },
        {
            scale: 0,
            opacity: 0,
            ease: "power4.inOut",
            overwrite: "auto",
        }
    );
    // image 4
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "30% 30%",
            end: "37% 37%",
            scrub: 1,
        },
    })
        .fromTo(
            "#image-4",
            {
                scale: 0,
                opacity: 0,
                ease: "power4.out",
                overwrite: true,
            },
            {
                scale: 1,
                opacity: 1,
                ease: "power4.out",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#text-slide-2",
            {
                translateX: "150%",
                opacity: 1,
                ease: "power4.out",
                overwrite: "auto",
            },
            {
                translateX: "-100%",
                opacity: 0,
                ease: "power4.out",
                overwrite: true,
            },
            0
        );

    // text 4 & 5 : technology
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "29% 29%",
            end: "32% 32%",
            scrub: 1,
        },
    })
        .fromTo(
            "#text-slide-4",
            {
                translateX: "-100%",
                translateY: "30%",
                opacity: 0,
                ease: "linear",
                overwrite: true,
            },
            {
                translateX: "0%",
                translateY: "30%",
                opacity: 1,
                ease: "linear",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#text-slide-5",
            {
                translateX: "300%",
                translateY: "30%",
                opacity: 0,
                ease: "linear",
                overwrite: true,
            },
            {
                translateX: "0%",
                translateY: "30%",
                opacity: 1,
                ease: "linear",
                overwrite: "auto",
            },

            0
        )
        .fromTo(
            "#text-slide-6",
            {
                opacity: 0,
                ease: "linear",
                overwrite: true,
            },
            {
                opacity: 1,
                ease: "linear",
                overwrite: "auto",
            },

            0
        );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "38% 38%",
            end: "42% 42%",
            scrub: 1,
        },
    })
        .fromTo(
            "#text-slide-4",
            {
                translateX: "0%",
                translateY: "30%",
                opacity: 1,
                ease: "linear",
                overwrite: "auto",
            },
            {
                translateX: "300%",
                translateY: "30%",
                opacity: 0,
                ease: "linear",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#text-slide-5",
            {
                translateX: "0%",
                translateY: "30%",
                opacity: 1,
                ease: "linear",
                overwrite: "auto",
            },
            {
                translateX: "-100%",
                translateY: "30%",
                opacity: 0,
                ease: "linear",
                overwrite: "auto",
            },
            0
        );
    //img 4 out
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "40% 40%",
            end: "44% 44%",
            scrub: 1,
        },
    })
        .fromTo(
            "#mRect",
            {
                attr: { x: "50%", y: "50%", width: "0%", height: "0%" },
                ease: "linear",
                overwrite: true,
            },
            {
                attr: { x: "0%", y: "0%", width: "100%", height: "100%" },
                ease: "linear",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#svg-1",
            {
                scale: 1.1,
                ease: "linear",
                overwrite: true,
            },
            {
                scale: 1,
                ease: "linear",
                overwrite: "auto",
            },
            0
        );
    //text - 6
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "40% 40%",
            end: "58% 58%",
            scrub: 1,
        },
    }).fromTo(
        "#text-slide-6",
        {
            translateX: "100%",
            translateY: "60vh",
            ease: "linear",
            overwrite: true,
        },
        {
            translateX: "-100%",
            translateY: "60vh",
            ease: "linear",
            overwrite: "auto",
        }
    );
    //image - 6 out
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "50% 50%",
            end: "52% 52%",
            scrub: 1,
        },
    }).fromTo(
        "#image-6",
        {
            translateX: "0",
            translateY: "0",
            ease: "power4.out",
            overwrite: "auto",
        },
        {
            translateX: "-5vh",
            translateY: "5vh",
            ease: "power4.out",
            overwrite: "auto",
        }
    );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "54% 54%",
            end: "56% 56%",
            scrub: 1,
        },
    }).fromTo(
        "#image-6",
        {
            translateX: "-5vh",
            scale: 1,
            opacity: 1,
            ease: "power4.in",
            overwrite: "auto",
        },
        {
            translateX: "-100%",
            scale: 1.5,
            opacity: 0,
            ease: "power4.in",
            overwrite: "auto",
        }
    );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "58% 58%",
            end: "62% 62%",
            scrub: 1,
        },
    }).fromTo(
        "#text-slide-7",
        {
            translateY: "45vh",
            width: "100vw",
            textAlign: "center",
            scale: 1,
            opacity: 0,
            ease: "power4.inOut",
            overwrite: true,
        },
        {
            translateY: "45vh",
            width: "100vw",
            textAlign: "center",
            scale: 1,
            opacity: 1,
            ease: "power4.inOut",
            overwrite: "auto",
        }
    );
    // image - 7
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "50% 50%",
            end: "67% 67%",
            scrub: 1,
        },
    }).fromTo(
        "#image-7",
        {
            scale: 1.1,

            ease: "power4.in",
            overwrite: "auto",
        },
        {
            scale: 1,

            ease: "power4.in",
            overwrite: "auto",
        }
    );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "63% 63%",
            end: "70% 70%",
            scrub: 1,
        },
    })
        .fromTo(
            "#mTriangle",
            {
                attr: { transform: "translate(0 150)" },
                ease: "linear",
                overwrite: true,
            },
            {
                attr: { transform: "translate(0 -100)" },

                ease: "linear",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#text-slide-7",
            {
                translateY: "45vh",
                ease: "power4.inOut",
                overwrite: true,
            },
            {
                translateY: "35vh",
                ease: "power4.inOut",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#text-slide-8",
            {
                attr: { y: "44vh" },
                ease: "power4.inOut",
                overwrite: true,
            },
            {
                attr: { y: "37vh" },
                ease: "power4.inOut",
                overwrite: "auto",
            },
            0
        );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "69% 69%",
            end: "70% 70%",
            scrub: 1,
        },
    })
        .fromTo(
            "#image-7",
            {
                opacity: 1,
                ease: "power4.in",
                overwrite: "auto",
            },
            {
                opacity: 0,
                ease: "power4.in",
                overwrite: "auto",
            }
        )
        .fromTo(
            "#text-slide-7",
            {
                opacity: 1,
                ease: "power4.in",
                overwrite: true,
            },
            {
                opacity: 0,
                ease: "power4.in",
                overwrite: "auto",
            },
            0
        );

    //image 8 & texte 8 out & image 9 enter
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "70% 70%",
            end: "78% 78%",
            scrub: 1,
        },
    }).fromTo(
        "#svg-2",
        {
            translateX: "%0",
            ease: "power4.in",
            overwrite: "auto",
        },
        {
            translateX: "-100%",
            ease: "power4.in",
            overwrite: "auto",
        },
        0
    );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "70% 70%",
            end: "77% 77%",
            scrub: 1,
        },
    })
        .fromTo(
            "#text-slide-8",
            {
                attr: { x: "0vh" },
                opacity: 1,
                ease: "power4.in",
                overwrite: true,
            },
            {
                attr: { x: "100vh" },
                opacity: 0,
                ease: "power4.in",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#image-9",
            {
                translateX: "50vh",
                ease: "power4.in",
                overwrite: true,
            },
            {
                translateX: "0vh",
                ease: "power4.in",
                overwrite: "auto",
            },
            0
        );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "78% 78%",
            end: "80% 80%",
            scrub: 1,
        },
    }).fromTo(
        "#svg-2",
        {
            opacity: 1,
            ease: "power4.in",
            overwrite: true,
        },
        {
            opacity: 0,
            ease: "power4.in",
            overwrite: "auto",
        },
        0
    );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "70% 70%",
            end: "78% 78%",
            scrub: 1,
        },
    })
        .fromTo(
            "#text-slide-9",
            {
                translateX: "90%",
                ease: "power4.in",
                overwrite: "auto",
            },
            {
                translateX: "0%",
                ease: "power4.in",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#text-slide-9",
            {
                opacity: 0,
                ease: "power4.inOut",
                overwrite: "auto",
            },
            {
                opacity: 1,
                ease: "power4.inOut",
                overwrite: "auto",
            },
            0
        );
    gsap.timeline({
        scrollTrigger: {
            trigger: ".h-animated",
            start: "85% 85%",
            end: "92% 92%",
            scrub: 1,
        },
    })
        .fromTo(
            "#text-slide-9",
            {
                translateX: "0%",
                ease: "linear",
                overwrite: "auto",
            },
            {
                translateX: "-100%",
                ease: "linear",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#image-10",
            {
                ease: "linear",
                overwrite: "auto",
            },
            {
                opacity: 1,
                translateX: 0,
                ease: "linear",
                overwrite: true,
            },
            0
        );
    gsap.timeline({
        onStart: () => {
            end = true;
            staticAnimation();
        },
        onReverseComplete: () => {
            end = false;
        },
        scrollTrigger: {
            trigger: ".h-animated",
            start: "92% 92%",
            end: "97% 97%",
            scrub: 1,
        },
    })
        .fromTo(
            "#image-9",
            {
                translateY: "0%",
                ease: "linear",
                overwrite: "auto",
            },
            {
                translateY: "-100%",
                ease: "linear",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#image-10",
            {
                translateY: "100%",
                ease: "linear",
                overwrite: "auto",
            },
            {
                translateY: "0%",
                ease: "linear",
                overwrite: "auto",
            },
            0
        )
        .fromTo(
            "#text-end",
            {
                opacity: 0,
                ease: "linear",
                overwrite: "auto",
            },
            {
                opacity: 1,
                ease: "linear",
                overwrite: true,
            },
            0
        );
}

function main() {
    const userAgent = navigator.userAgent;
    if (userAgent.match(/firefox|fxios/i)) {
        console.log("User agent : firefox");
        document.documentElement.style.setProperty("--sHeight", "220vh");
    } else if (userAgent.match(/edg/i)) {
        document.documentElement.style.setProperty("--sHeight", "165vh");
        console.log("User agent : edge");
    } else {
        console.log("User agent : chromium/others");
        document.documentElement.style.setProperty("--sHeight", "125vh");
    }
    trigger();
}
main();
