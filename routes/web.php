<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/{locale?}', function ($locale = null) {
    if (isset($locale) && in_array($locale, config('app.available_locales'))) {
        app()->setLocale($locale);
    }

    return view('pages.home');
});
*/

Route::get('lang/{locale}', function ($locale) {
    app()->setLocale($locale);
    session()->put('locale', $locale);
    return redirect()->back();
});

Route::get('/', function () {
    return view('pages.home');
})->name('home');

// Route::get('/about', function () {
//     return view('pages.about');
// })->name('about');

// Route::get('/expertises', function () {
//     return view('pages.expertises');
// })->name('expertises');

// Route::get('/realisations', function () {
//     return view('pages.realisations');
// })->name('realisations');

// Route::get('/contact', function () {
//     return view('pages.contact');
// })->name('contact');

// Route::get('/groupe', function () {
//     return view('pages.groupe');
// })->name('groupe');

Route::post('/message', [App\Http\Controllers\Controller::class, 'message'])->name('message');